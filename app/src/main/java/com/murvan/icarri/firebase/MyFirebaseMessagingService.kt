package com.murvan.icarri.firebase

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.os.Bundle
import android.util.Log

import androidx.core.app.NotificationCompat

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.murvan.icarri.R
import com.murvan.icarri.activity.*
import com.murvan.icarri.activity.OfferListActivity.Companion.offerType
import com.murvan.icarri.activity.OfferListActivity.Companion.postType
import com.murvan.icarri.activity.PaymentListActivity.Companion.paymentType
import com.murvan.icarri.utils.Constant
import com.murvan.icarri.utils.Preference

import java.io.IOException
import java.net.URL

class MyFirebaseMessagingService : FirebaseMessagingService() {
    private var numMessages = 0


    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
    }


    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        val notification = remoteMessage.notification
        val data = remoteMessage.data
        Log.d("FROM", remoteMessage.from!!)
        sendNotification(data)
    }

    private fun sendNotification(
        // notification: RemoteMessage.Notification?,
        data: Map<String, String>
    ) {

        val bundle = Bundle()
        bundle.putString(FCM_PARAM, data[FCM_PARAM])
        bundle.putString(FCM_ID, data[FCM_ID])
        bundle.putString(FCM_TYPE, data[FCM_TYPE])
        bundle.putString(FCM_TITLE, data[FCM_TITLE])
        bundle.putString(FCM_BODY, data[FCM_BODY])
        bundle.putString(FCM_POST_ID, data[FCM_POST_ID])
        bundle.putString(FCM_PAYMENT_ID, data[FCM_PAYMENT_ID])
        var type = data[FCM_TYPE]
        var intent: Intent? = null

        if (!Preference.getInstance(this).getString(Constant.USER_ID).equals("")) {
            if (type.equals("new_message")) {
                intent = Intent(this, InboxActivity::class.java)
            } else if (type.equals("comment")) {
                intent = Intent(this, CommentListActivity::class.java)
                intent.putExtra("ID", data[FCM_POST_ID])
            } else if (type.equals("receive_travel_offer")) {
                intent = Intent(this, OfferListActivity::class.java)
                postType = "Travel Details"
                offerType = "Received"
            } else if (type.equals("receive_package_offer")) {
                intent = Intent(this, OfferListActivity::class.java)
                offerType = "Received"
                postType = "Package Details"
            } else if (type.equals("accept_travel_offer")) {
                intent = Intent(this, OfferListActivity::class.java)
                postType = "Travel Details"
                offerType = "Received"
            } else if (type.equals("accept_package_offer")) {
                intent = Intent(this, OfferListActivity::class.java)
                offerType = "Received"
                postType = "Package Details"
            } else if (type.equals("reject_travel_offer")) {
                intent = Intent(this, OfferListActivity::class.java)
                postType = "Travel Details"
                offerType = "Given"
            } else if (type.equals("reject_package_offer")) {
                intent = Intent(this, OfferListActivity::class.java)
                offerType = "Received"
                postType = "Package Details"
            } else if (type.equals("payment_received")) {
                intent = Intent(this, PaymentListActivity::class.java)
                paymentType = "Received"
            } else if (type.equals("payment_paid")) {
                intent = Intent(this, PaymentListActivity::class.java)
                paymentType = "Given"
            } else if (type.equals("transfer")) {
                intent = Intent(this, PaymentDetailActivity::class.java)
                intent.putExtra("ID", data[FCM_PAYMENT_ID])
            } else if (type.equals("reviews")) {
                intent = Intent(this, ReviewsListActivity::class.java)
            } else {
                intent = Intent(this, HomeActivity::class.java)
            }
        } else {
            intent = Intent(this, LoginActivity::class.java)
        }


        //val intent
        intent?.putExtras(bundle)

        Log.d("TYPE", type)

        val pendingIntent =
            PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        var notificationBuilder: NotificationCompat.Builder? = null
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            notificationBuilder =
                NotificationCompat.Builder(this, getString(R.string.notification_channel_id))
                    .setContentTitle(data[FCM_TITLE])
                    .setContentText(data[FCM_BODY])
                    .setAutoCancel(true)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    //.setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.win))
                    .setContentIntent(pendingIntent)
                    .setContentInfo("Hello")
                    .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
                    .setColor(getColor(R.color.colorAccent))
                    .setLights(Color.RED, 1000, 300)
                    .setDefaults(Notification.DEFAULT_VIBRATE)
                    .setNumber(++numMessages)
                    .setSmallIcon(R.drawable.logo)
        }

        /*  try {
              val picture = data[FCM_PARAM]
              if (picture != null && "" != picture) {
                  val url = URL(picture)
                  val bigPicture = BitmapFactory.decodeStream(url.openConnection().getInputStream())
                  notificationBuilder!!.setStyle(
                      NotificationCompat.BigPictureStyle().bigPicture(bigPicture).setSummaryText(
                          notification!!.body
                      )
                  )
              }
          } catch (e: IOException) {
              e.printStackTrace()
          }*/

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                getString(R.string.notification_channel_id),
                CHANNEL_NAME,
                NotificationManager.IMPORTANCE_DEFAULT
            )
            channel.description = CHANNEL_DESC
            channel.setShowBadge(true)
            channel.canShowBadge()
            channel.enableLights(true)
            channel.lightColor = Color.RED
            channel.enableVibration(true)
            channel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500)

            assert(notificationManager != null)
            notificationManager.createNotificationChannel(channel)
        }

        assert(notificationManager != null)
        notificationManager.notify(0, notificationBuilder!!.build())
    }

    companion object {
        val FCM_PARAM = "picture"
        val FCM_TYPE = "type"
        val FCM_BODY = "body"
        val FCM_TITLE = "title"
        val FCM_POST_ID = "post_id"
        val FCM_PAYMENT_ID = "payment_id"
        val FCM_ID = "id"
        private val CHANNEL_NAME = "FCM"
        private val CHANNEL_DESC = "Firebase Cloud Messaging"
    }
}