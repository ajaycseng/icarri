package com.murvan.icarri.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.android.volley.VolleyError
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.murvan.icarri.R
import com.murvan.icarri.models.UserData
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.Constant
import com.murvan.icarri.utils.Preference
import com.murvan.icarri.utils.snackBar
import com.murvan.icarri.utils.toast
import kotlinx.android.synthetic.main.activity_message.*
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap

class SendMsgActivity : AppCompatActivity() {

    var msg: String = ""
    var userdata: UserData? = null
    private val gson = Gson()
    var announceID: String = ""
    var location: String = ""

    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_message)

        announceID = intent.getStringExtra("ID")
        location = intent.getStringExtra("LOCATION").replace(" to ", " : ")

        tvSourceDestination.setText(location)

        userdata = gson.fromJson(
            Preference.getInstance(this).getString(Constant.USER_DETAIL),
            UserData::class.java
        )

        btnSend.setOnClickListener(View.OnClickListener {

            msg = etMsg.text.toString().trim()

            if (msg.isNullOrEmpty()) {
                toast("Write your message in message box.")
            } else {
                callWS()
            }
        })

        ivBacks.setOnClickListener { finish() }
    }


    private fun callWS() {
        initCallback()
        mVolleyService = VolleyService(resultCallback, this@SendMsgActivity)
        mVolleyService!!.postStringRequest(
            this@SendMsgActivity,
            "POST",
            URLS.ADD_MSG,
            getParamsLogin(),
            "",true
        )
    }


    private fun getParamsLogin(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("user_id", userdata?.user_id)
            jsonStr.put("announcement_id", announceID)
            jsonStr.put("message_text", msg)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token); //FirebaseInstanceId.getInstance().getToken());
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }

    private fun initCallback() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) {
                try {
                    val jsonObject = JSONObject(response)
                    val status = jsonObject.getString("response_status")
                    val msg = jsonObject.getString("response_msg")
                    if (status.equals("success", ignoreCase = true)) {

                        // val responseData = jsonObject.getJSONObject("response_data")

                        toast(msg)
                        finish()


                    } else {
                        btnSend.snackBar(btnSend, msg)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                btnSend.snackBar(btnSend, error.toString())
            }
        }
    }
}
