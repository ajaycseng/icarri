package com.murvan.icarri.activity

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.WindowManager
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.PopupWindow
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.app.ActivityCompat
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.murvan.icarri.R
import com.murvan.icarri.models.DropListData
import com.murvan.icarri.models.UserData
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.Constant
import com.murvan.icarri.utils.Preference
import com.murvan.icarri.utils.snackBar
import com.murvan.icarri.utils.toast
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.activity_add_offer.*
import kotlinx.android.synthetic.main.activity_add_travel.btnChooseImg
import kotlinx.android.synthetic.main.activity_add_travel.btnSubmit
import kotlinx.android.synthetic.main.activity_add_travel.etDescription
import kotlinx.android.synthetic.main.activity_pkt_travel.etDimensionUnit
import kotlinx.android.synthetic.main.activity_pkt_travel.etHeight
import kotlinx.android.synthetic.main.activity_pkt_travel.etLength
import kotlinx.android.synthetic.main.activity_pkt_travel.etNumPkg
import kotlinx.android.synthetic.main.activity_pkt_travel.etWeight
import kotlinx.android.synthetic.main.activity_pkt_travel.etWeightUnit
import kotlinx.android.synthetic.main.activity_pkt_travel.etWidth
import kotlinx.android.synthetic.main.activity_sign_up.ivBack
import kotlinx.android.synthetic.main.activity_sign_up.ivImage
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.net.MalformedURLException
import java.util.*
import kotlin.collections.ArrayList


class AddPkgOfferActivity : BaseActivity() {

    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null

    var weight: String? = null
    var weightUnit: String? = null

    var description: String? = null
    var offerAmount: String? = null
    var pkgNo: String? = null
    var dLength: String? = null
    var dWidth: String? = null
    var dHeight: String? = null

    var dimentionsUnit: String? = null

    var bitmap: Bitmap? = null
    var resultUri: Uri? = null
    var isImgAttached: Boolean = false
    val gson = Gson()
    val postType: String = "Package Details"
    var postID: String = ""

    var userdata: UserData? = null
    var dropListData = ArrayList<DropListData>()

    var values = ArrayList<String>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_offer)


        postID = intent.extras?.getString("ID").toString()
        userdata =
            gson.fromJson(
                Preference.getInstance(this).getString(Constant.USER_DETAIL),
                UserData::class.java
            )
        // callWSDrop()

        ivBack.setOnClickListener {
            finish()
        }

        etWeightUnit.setOnClickListener {

            values.clear()
            values.add("Kg")
            values.add("Pounds")
            val popUp = popupWindowsort(etWeightUnit);
            popUp.showAsDropDown(etWeightUnit, 0, 0);
        }
        etDimensionUnit.setOnClickListener {
            values.clear()
            values.add("CM")
            values.add("Inches")
            dropListData
            val popUp = popupWindowsort(etDimensionUnit);
            popUp.showAsDropDown(etDimensionUnit, 0, 0);
        }


        btnSubmit.setOnClickListener {

            validation()
        }
        btnChooseImg.setOnClickListener {
            storagePermission()
        }

/*
        try {


            if (intent.hasExtra("DATA")) {
                var travelPkgData = TravelPkgData()
                travelPkgData = intent.getSerializableExtra("DATA") as TravelPkgData
                setData(travelPkgData)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }*/

    }

    /*   private fun setData(travelPkgData: TravelPkgData) {


           tvtitle.setText("Add Offer")
           postID = travelPkgData.id.toString()
           //etPickUpFrom.setText(travelPkgData.depart_from)
           // etDeliverTo.setText(travelPkgData.travel_destination)
           etDescription.setText(travelPkgData.post_description)
           etWeight.setText(travelPkgData.max_weight)
           etWeightUnit.setText(travelPkgData.max_weight_unit)

           etNumPkg.setText(travelPkgData.max_packets)
           etLength.setText(travelPkgData.dimension_packet_length)
           etWidth.setText(travelPkgData.dimension_packet_width)
           etHeight.setText(travelPkgData.dimension_packet_height)


           etDimensionUnit.setText(travelPkgData.dimension_packet_unit)


           cbNotification.isChecked = travelPkgData.notification!!


           if (!travelPkgData.main_photo.isNullOrEmpty()) {


               isImgAttached = true
               Glide.with(this@AddPkgOfferActivity)
                   .load(travelPkgData.main_photo)
                   .apply(RequestOptions.fitCenterTransform())
                   .into(ivImage);

           }

       }*/


    private fun validation() {

        weight = etWeight.text.toString().trim()
        weightUnit = etWeightUnit.text.toString().trim()
        pkgNo = etNumPkg.text.toString().trim()
        dLength = etLength.text.toString().trim()
        dWidth = etWidth.text.toString().trim()
        dHeight = etHeight.text.toString().trim()

        dimentionsUnit = etDimensionUnit.text.toString().trim()


        offerAmount = etOfferAmount.text.toString().trim()
        description = etDescription.text.toString().trim()
        if (weight.isNullOrEmpty()) {
            btnSubmit.snackBar(btnSubmit, "Weight!!")
        } else if (weightUnit.isNullOrEmpty()) {
            btnSubmit.snackBar(btnSubmit, "Weight Unit!!")
        } else if (pkgNo.isNullOrEmpty()) {
            btnSubmit.snackBar(btnSubmit, "No of Package!!")
        } else if (dLength.isNullOrEmpty()) {
            btnSubmit.snackBar(btnSubmit, "Dimension Length!!")
        } else if (dWidth.isNullOrEmpty()) {
            btnSubmit.snackBar(btnSubmit, "Dimension Width!!")
        } else if (dHeight.isNullOrEmpty()) {
            btnSubmit.snackBar(btnSubmit, "Dimension Height!!")
        } else if (dimentionsUnit.isNullOrEmpty()) {
            btnSubmit.snackBar(btnSubmit, "Dimension Unit!!")
        } else if (offerAmount.isNullOrEmpty()) {
            btnSubmit.snackBar(btnSubmit, "Offer Amount!!")
        } else if (description.isNullOrEmpty()) {
            btnSubmit.snackBar(btnSubmit, "Description!!")
        }
        // else if (!isImgAttached) {
        //     btnSubmit.snackBar(btnSubmit, "Click on user to attach image!!")
        // }
        else {
            callWS();
        }

    }


    private fun callWS() {
        var url: String
        url = URLS.ADD_OFFER
        initCallback()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.sendImgRequest(
            this,
            "POST",
            "main_photo",
            bitmap,
            url,
            getParamsLogin(),
            ""
        )
    }


    private fun getParamsLogin(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            // if (!postID.isNullOrEmpty()) {
            // }

            jsonStr.put("post_id", postID)
            // jsonStr.put("post_type", postType)
            jsonStr.put("user_id", userdata?.user_id)
            jsonStr.put("max_weight", weight)
            jsonStr.put("max_weight_unit", weightUnit)
            jsonStr.put("max_packets", pkgNo)
            // jsonStr.put("dimension_packet", Dheigth)
            jsonStr.put("dimension_packet_length", dLength)
            jsonStr.put("dimension_packet_width", dWidth)
            jsonStr.put("dimension_packet_height", dHeight)
            jsonStr.put("offer_description", description)
            jsonStr.put("offer_amount", offerAmount)

            jsonStr.put("dimension_packet_unit", dimentionsUnit)
           // jsonStr.put("post_description", description)

            jsonStr.put("device_type", "Android")
            jsonStr.put(
                "device_id",
                HomeActivity.token
            ); //FirebaseInstanceId.getInstance().getToken());
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallback() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) {

                try {
                    val jsonObject = JSONObject(response)
                    val status = jsonObject.getString("response_status")
                    val msg = jsonObject.getString("response_msg")
                    if (status.equals("success", ignoreCase = true)) {

                        toast(msg)


                        finish()

                    } else {
                        btnSubmit.snackBar(btnSubmit, msg)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                btnSubmit.snackBar(btnSubmit, error.toString())
            }
        }
    }


    private fun pickImage() {
        CropImage.activity()
            .setMinCropWindowSize(200, 400)
            .setFixAspectRatio(true)
            .start(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode === CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode === Activity.RESULT_OK) {
                resultUri = result.uri


                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                isImgAttached = true
                try {


                    Glide.with(this)
                        .load(resultUri)
                        .apply(RequestOptions.fitCenterTransform()).into(ivImage);


                } catch (e: MalformedURLException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                //appCache.getUser()?.showImage(userPic, File(resultUri.path).absolutePath)
            } else if (resultCode === CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
                toast(error.localizedMessage)
            }
        }
    }


    private fun storagePermission() {

        if (ActivityCompat.checkSelfPermission(
                this!!,
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this!!,
                arrayOf(
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), 100
            )
        } else {
            pickImage()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {

        if (requestCode == 100 && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            pickImage()
        } else {
            toast("Permission not granted!!")
        }
    }


    fun popupWindowsort(editText: AppCompatEditText): PopupWindow {
        var popupWindow = PopupWindow(this);

/*  var values = ArrayList<String>()

for (str in dropListData) {
  values.add(str.type.toString())
}*/


        var adapter = ArrayAdapter<String>(this, R.layout.dropdown_item, values)
        var listViewSort = ListView(this);
        listViewSort.setAdapter(adapter);
        listViewSort.setOnItemClickListener { parent, view, position, id ->
            editText.setText(values[position])
            popupWindow.dismiss()
        }
        popupWindow.setFocusable(true);
        popupWindow.setWidth(etWeight.getWidth())
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setContentView(listViewSort)

        return popupWindow;
    }


//

    private fun callWSDrop() {
        initCallbackDrop()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.DROP_LIST_OPTION,
            getParamsDrop(),
            "",true
        )
    }


    private fun getParamsDrop(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("dropdown_type", "weightUnits")
            jsonStr.put("device_type", "Android")
            jsonStr.put(
                "device_id",
                HomeActivity.token
            ); //FirebaseInstanceId.getInstance().getToken());
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackDrop() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                if (status.equals("success", ignoreCase = true)) {

                    val responseData = jsonObject.getJSONArray("response_data")

                    for (i in 0 until responseData.length()) {
                        val data: DropListData
                        data =
                            gson.fromJson(responseData.get(i).toString(), DropListData::class.java)
                        dropListData.add(data)
                    }


                } else {
                    btnSubmit.snackBar(btnSubmit, msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                btnSubmit.snackBar(btnSubmit, error.toString())
            }
        }
    }


//
}

