package com.murvan.icarri.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.murvan.icarri.R
import com.murvan.icarri.models.UserData
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.Constant
import com.murvan.icarri.utils.Preference
import com.murvan.icarri.utils.snackBar
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.activity_user_profile.*
import org.json.JSONException
import org.json.JSONObject
import java.lang.Exception
import java.util.HashMap

class UserProfileActivity : AppCompatActivity() {
    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null
    var userID: String = ""
    private val gson = Gson()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile)
        userID = intent.extras?.getString("ID").toString()
        callWS()

        ivBacck.setOnClickListener(View.OnClickListener {
            finish()
        })
    }


    private fun callWS() {
        initCallback()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.GET_PROFILE,
            getParamsLogin(),
            "",true
        )
    }


    private fun getParamsLogin(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {

            jsonStr.put("user_id", userID)
            // jsonStr.put("email_address", email)


            jsonStr.put("device_type", "Android")
            jsonStr.put(
                "device_id",
                HomeActivity.token
            ); //FirebaseInstanceId.getInstance().getToken());
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallback() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) {


                try {
                    val jsonObject = JSONObject(response)
                    val status = jsonObject.getString("response_status")
                    val msg = jsonObject.getString("response_msg")
                    if (status.equals("success", ignoreCase = true)) {

                        val responseData = jsonObject.getJSONObject("response_data")

                        val user: UserData
                        user =
                            gson.fromJson(responseData.toString(), UserData::class.java)
                        tvName.setText(user.first_name + " " + user.last_name)
                        tvLocation.setText(user.city_town + ", " + user.country)
                        tvPostCount.setText(user.total_posts)
                        tvComment.setText(user.total_comments)
                        tvReview.setText(user.total_reviews)

                        if (!user?.profile_image.isNullOrBlank()) {
                            Glide.with(this@UserProfileActivity)
                                .load(user?.profile_image)
                                .apply(RequestOptions.circleCropTransform())
                                .into(ivImages);
                        }
                        rating.rating = (user.average_rating)!!.toFloat();

                    } else {
                        cardView2.snackBar(cardView2, msg)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                cardView2.snackBar(cardView2, error.toString())
            }
        }
    }
}
