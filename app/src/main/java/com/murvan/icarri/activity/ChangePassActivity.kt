package com.murvan.icarri.activity

import android.content.Intent
import android.os.Bundle
import com.android.volley.VolleyError
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.murvan.icarri.R
import com.murvan.icarri.models.UserData
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.*
import kotlinx.android.synthetic.main.activity_change_pass.*
import kotlinx.android.synthetic.main.activity_forgot_pass.btnSubmit
import kotlinx.android.synthetic.main.activity_forgot_pass.ivBack
import kotlinx.android.synthetic.main.activity_gen_pass.*
import kotlinx.android.synthetic.main.activity_gen_pass.etConPass
import kotlinx.android.synthetic.main.activity_gen_pass.etPass
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap

class ChangePassActivity : BaseActivity() {

    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null


    var oldPass: String? = null
    var pass: String? = null
    var conPass: String? = null
    private val gson = Gson()

    var userdata: UserData? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_pass)

        userdata =
            gson.fromJson(
                Preference.getInstance(this).getString(Constant.USER_DETAIL),
                UserData::class.java
            )


        btnSubmit.setOnClickListener {
            hideKeyboard(btnSubmit)
            oldPass = etOldPass.text.toString().trim()
            pass = etPass.text.toString().trim()
            conPass = etConPass.text.toString().trim()
            if (oldPass.isNullOrEmpty()) {
                btnSubmit.snackBar(btnSubmit, "Write Old Password!!")
            } else if (pass.isNullOrEmpty()) {
                btnSubmit.snackBar(btnSubmit, "Password!!")
            } else if (conPass.isNullOrEmpty()) {
                btnSubmit.snackBar(btnSubmit, "Confirm Password!!")
            } else if (!pass.equals(conPass)) {
                btnSubmit.snackBar(btnSubmit, "Password not match!!")
            } else {
                callWS();
            }

        }

        ivBack.setOnClickListener {

            finish()
        }
    }

    private fun callWS() {
        initCallback()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(this, "POST", URLS.CHANGE_PASS, getParam(), "",true)
    }


    private fun getParam(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("email_address", userdata?.email_address)
            jsonStr.put("user_id", userdata?.user_id)
            jsonStr.put("current_password", oldPass)
            jsonStr.put("new_password", pass)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token); //FirebaseInstanceId.getInstance().getToken());
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }

    private fun initCallback() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) {
                try {
                    val jsonObject = JSONObject(response)
                    val status = jsonObject.getString("response_status")
                    val msg = jsonObject.getString("response_msg")
                    if (status.equals("success", ignoreCase = true)) {
                        toast(msg)
                        finish()

                    } else {
                        btnSubmit.snackBar(btnSubmit, msg)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                btnSubmit.snackBar(btnSubmit, error.toString())
            }
        }
    }


}
