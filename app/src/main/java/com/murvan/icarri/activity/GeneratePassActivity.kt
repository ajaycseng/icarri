package com.murvan.icarri.activity

import android.content.Intent
import android.os.Bundle
import com.android.volley.VolleyError
import com.google.firebase.iid.FirebaseInstanceId
import com.murvan.icarri.R
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.hideKeyboard
import com.murvan.icarri.utils.snackBar
import kotlinx.android.synthetic.main.activity_forgot_pass.btnSubmit
import kotlinx.android.synthetic.main.activity_forgot_pass.ivBack
import kotlinx.android.synthetic.main.activity_gen_pass.*
import kotlinx.android.synthetic.main.activity_gen_pass.etConPass
import kotlinx.android.synthetic.main.activity_gen_pass.etPass
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap

class GeneratePassActivity : BaseActivity() {

    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null


    var email: String? = null
    var otp: String? = null
    var pass: String? = null
    var conPass: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gen_pass)
        email = intent.extras?.getString("EMAIL")


        btnSubmit.setOnClickListener {
            hideKeyboard(btnSubmit)
            otp = etOTP.text.toString().trim()
            pass = etPass.text.toString().trim()
            conPass = etConPass.text.toString().trim()
            if (otp.isNullOrEmpty()) {
                btnSubmit.snackBar(btnSubmit, "Write correct OTP!!")
            } else if (pass.isNullOrEmpty()) {
                btnSubmit.snackBar(btnSubmit, "Password!!")
            } else if (conPass.isNullOrEmpty()) {
                btnSubmit.snackBar(btnSubmit, "Confirm Password!!")
            } else if (!pass.equals(conPass)) {
                btnSubmit.snackBar(btnSubmit, "Password not match!!")
            } else {
                callWS();
            }

        }

        ivBack.setOnClickListener {

            finish()
        }
    }

    private fun callWS() {
        initCallback()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(this, "POST", URLS.GENERATE_PASS, getParam(), "",true)
    }


    private fun getParam(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("email_address", email)
            jsonStr.put("six_digit_code", otp)
            jsonStr.put("new_password", pass)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token); //FirebaseInstanceId.getInstance().getToken());
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }

    private fun initCallback() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) {
                try {
                    val jsonObject = JSONObject(response)
                    val status = jsonObject.getString("response_status")
                    val msg = jsonObject.getString("response_msg")
                    if (status.equals("success", ignoreCase = true)) {
                        var inten = Intent(this@GeneratePassActivity, LoginActivity::class.java)
                        startActivity(inten)
                        finishAffinity()

                    } else {
                        btnSubmit.snackBar(btnSubmit, msg)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                btnSubmit.snackBar(btnSubmit, error.toString())
            }
        }
    }


}
