package com.murvan.icarri.activity

import android.graphics.Matrix
import android.os.Bundle
import android.view.ScaleGestureDetector
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.murvan.icarri.R
import kotlinx.android.synthetic.main.activity_image_view.*


class ImageViewActivity : AppCompatActivity() {

    var url: String = ""
    // var scaleGestureDetector: ScaleGestureDetector? = null
    // var matrix = Matrix()
    //var ivImage: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_view)
        url = intent.extras?.getString("URL").toString()
        //ivImage = findViewById(R.id.ivImage)

        ivClose.setOnClickListener {
            finish()
        }
        ivImage?.let {
            Glide.with(this@ImageViewActivity)
                .load(url)
                .apply(RequestOptions.fitCenterTransform())
                .into(ivImage)
        };

        // scaleGestureDetector = ScaleGestureDetector(this, ScaleListener())

    }


    /* internal inner class ScaleListener : SimpleOnScaleGestureListener() {
         override fun onScale(detector: ScaleGestureDetector): Boolean {
             var scaleFactor = detector.scaleFactor
             scaleFactor = Math.max(0.1f, Math.min(scaleFactor, 5.0f))
             matrix.setScale(scaleFactor, scaleFactor)
             ivImage?.setImageMatrix(matrix)
             return true
         }
     }
 */

}
