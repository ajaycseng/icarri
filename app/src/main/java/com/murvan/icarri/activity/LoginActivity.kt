package com.murvan.icarri.activity

import android.content.Intent
import android.os.Bundle
import com.android.volley.VolleyError
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.murvan.icarri.R
import com.murvan.icarri.models.UserData
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.Constant
import com.murvan.icarri.utils.Preference
import com.murvan.icarri.utils.hideKeyboard
import com.murvan.icarri.utils.snackBar
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class LoginActivity : BaseActivity() {

    var email: String? = null
    var pass: String? = null

    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null
    private val gson = Gson()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        // val authViewModel = ViewModelProviders.of(this).get(AuthViewModel::class.java)


        btnLogin.setOnClickListener {
            hideKeyboard(btnLogin)
            email = etEmail.text.toString().trim()
            pass = etPass.text.toString().trim()

            if (email.isNullOrEmpty() || pass.isNullOrEmpty()) {
                btnLogin.snackBar(btnLogin, "Write correct email or password!!")
            } else {
                callWS()

            }


        }

        tvForgotPass.setOnClickListener {

            val mainIntent = Intent(this, ForgotPassActivity::class.java)
            startActivity(mainIntent)

        }

        tvSignup.setOnClickListener {

            val mainIntent = Intent(this, SignUpActivity::class.java)
            startActivity(mainIntent)

        }

    }

    private fun callWS() {
        initCallback()
        mVolleyService = VolleyService(resultCallback, this@LoginActivity)
        mVolleyService!!.postStringRequest(
            this@LoginActivity,
            "POST",
            URLS.LOGIN,
            getParamsLogin(),
            "",true
        )
    }


    private fun getParamsLogin(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("email_address", email)
            jsonStr.put("password", pass)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }

    private fun initCallback() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) {
                try {
                    val jsonObject = JSONObject(response)
                    val status = jsonObject.getString("response_status")
                    val msg = jsonObject.getString("response_msg")
                    if (status.equals("success", ignoreCase = true)) {

                        val responseData = jsonObject.getJSONObject("response_data")


                        val mainIntent = Intent(this@LoginActivity, HomeActivity::class.java)
                        startActivity(mainIntent)

                        val user: UserData
                        user =
                            gson.fromJson(responseData.toString(), UserData::class.java)
                        Preference.getInstance(this@LoginActivity)
                            .putString(Constant.USER_DETAIL, responseData.toString())
                        Preference.getInstance(this@LoginActivity)
                            .putString(Constant.USER_ID, user.user_id)

                        finishAffinity()
                    } else {
                        btnLogin.snackBar(btnLogin, msg)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                btnLogin.snackBar(btnLogin, error.toString())
            }
        }
    }

}


