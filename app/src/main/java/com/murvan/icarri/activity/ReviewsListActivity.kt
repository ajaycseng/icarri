package com.murvan.icarri.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.murvan.icarri.R
import com.murvan.icarri.models.ReviewsData
import com.murvan.icarri.models.UserData
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.ChangeDateFormat
import com.murvan.icarri.utils.Constant
import com.murvan.icarri.utils.Preference
import com.murvan.icarri.utils.toast
import kotlinx.android.synthetic.main.activity_my_travel_list.ivBacks
import kotlinx.android.synthetic.main.activity_my_travel_list.tvNoDatas
import kotlinx.android.synthetic.main.activity_offer_list.*
import kotlinx.android.synthetic.main.row_review_list.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class ReviewsListActivity : AppCompatActivity() {

    var recycleView: RecyclerView? = null
    var arrayList = ArrayList<ReviewsData>()
    internal lateinit var mAdapter: AdapterClass
    val gson = Gson()
    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null
    var userdata: UserData? = null
    var postType: String = "Travel Details"
    var reviewType: String = "Received"
    var optionType: String = ""
    var offerID: String = ""
    var transactionID: String = ""
    var paymentJson: String = ""
    var selectedPosition: Int = -1
    var finalPrice: Double = 0.0
    var perAmount: Double = 0.0
    var amount: Double = 0.0
    var isPaymentProcess: Boolean = false

    //
    var pastVisiblesItems = 0
    var visibleItemCount = 0
    var totalItemCount = 0
    private var loading = true
    var pageNumber: Int = 1
    var totalRecords: Int = 0
    var totalPages: Int = 0
    //
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_review_list)
        isPaymentProcess = false
        tvTitles.setText("Reviews")
        tvNoDatas.setText(resources.getString(R.string.nooffer))

        tvNoDatas.setOnClickListener {
            callWSList()
        }

        ivBacks.setOnClickListener {

            finish()
        }
        ivType.setOnClickListener {
            showPopupMenu(ivType)

        }

        userdata =
            gson.fromJson(
                Preference.getInstance(this).getString(Constant.USER_DETAIL),
                UserData::class.java
            )


        recycleView = findViewById(R.id.recycleView)

        var layoutManager = LinearLayoutManager(this)
        val layoutManagerPost = LinearLayoutManager(this)
        recycleView?.layoutManager = layoutManager
        recycleView?.itemAnimator = DefaultItemAnimator()
        mAdapter = AdapterClass(arrayList)
        recycleView?.adapter = mAdapter
        mAdapter.notifyDataSetChanged()
        recycleView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(
                recyclerView: RecyclerView,
                dx: Int,
                dy: Int
            ) {

                var visibleItemCount = layoutManager.getChildCount();
                var totalItemCount = layoutManager.getItemCount();
                var firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (totalPages > pageNumber) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                    ) {
                        pageNumber++
                        callWSList()
                    }
                }

            }
        })

        btnReceive.setOnClickListener(View.OnClickListener {


            btnGiven.setBackgroundDrawable(resources.getDrawable(R.drawable.corner_greybtn))
            btnGiven.setTextColor(resources.getColor(R.color.black))
            btnReceive.setBackgroundDrawable(resources.getDrawable(R.drawable.corner_orange))
            btnReceive.setTextColor(resources.getColor(R.color.white))
            reviewType = "Received"
            arrayList.clear()
            pageNumber=1
            callWSList();

        })

        btnGiven.setOnClickListener(View.OnClickListener {
            btnReceive.setBackgroundDrawable(resources.getDrawable(R.drawable.corner_greybtn))
            btnReceive.setTextColor(resources.getColor(R.color.black))
            btnGiven.setBackgroundDrawable(resources.getDrawable(R.drawable.corner_orange))
            btnGiven.setTextColor(resources.getColor(R.color.white))
            reviewType = "Given"
            arrayList.clear()
            pageNumber=1
            callWSList();
        })
    }


    fun showPopupMenu(view: View) {

        var popup = PopupMenu(this, view);
        popup.getMenuInflater().inflate(R.menu.type_menu, popup.getMenu())
        popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { it ->
            val item = it!!.itemId
            when (item) {
                R.id.menuTravel -> {
                    tvTitles.setText("Travel Offers")
                    postType = "Travel Details"
                    arrayList.clear()
                    pageNumber=1
                    callWSList()

                }
                R.id.menuPkg -> {
                    tvTitles.setText("Package Offers");
                    postType = "Package Details"
                    arrayList.clear()
                    pageNumber=1
                    callWSList()

                }

            }
            true

        })
        popup.show();
    }


    override fun onResume() {
        super.onResume()
        if (pageNumber == 1) {
            arrayList.clear()
            callWSList();
        }
    }
    //

    private fun callWSList() {
        initCallbackDrop()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.REVIEWS_LIST,
            getParamsDrop(),
            "", true
        )
    }


    private fun getParamsDrop(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("review_type", reviewType)
            jsonStr.put("user_id", userdata?.user_id)
            jsonStr.put("page_number", pageNumber)

            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackDrop() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                if (status.equals("success", ignoreCase = true)) {
                    totalRecords = jsonObject.getInt("total_records")
                    totalPages = jsonObject.getInt("total_pages")

                    val data = jsonObject.getJSONObject("response_data")
                    val responseData = data.getJSONArray("reviews_list")


                    if (!data.getString("total_received").equals("0")) {

                        btnReceive.setText("Received (" + data.getString("total_received") + ")")
                    } else if (!data.getString("total_given").equals("0")) {

                        btnGiven.setText("Given (" + data.getString("total_given") + ")")
                    }


                    //  arrayList.clear()
                    if (responseData.length() == 0) {
                        tvNoDatas.visibility = View.VISIBLE
                        recycleView?.visibility = View.GONE
                    } else {
                        tvNoDatas.visibility = View.GONE
                        recycleView?.visibility = View.VISIBLE


                        for (i in 0 until responseData.length()) {
                            val data: ReviewsData
                            data =
                                gson.fromJson(
                                    responseData.get(i).toString(),
                                    ReviewsData::class.java
                                )
                            arrayList.add(data)
                        }

                    }

                    // mAdapter = AdapterClass(arrayList)
                    // recycleView?.adapter = mAdapter
                    mAdapter.notifyDataSetChanged()

                } else {
                    //  arrayList.clear()

                    //mAdapter = AdapterClass(arrayList)
                    // recycleView?.adapter = mAdapter
                    mAdapter.notifyDataSetChanged()
                    toast(msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                toast(error.toString())
            }
        }
    }


    //


    internal inner class AdapterClass(val arrayList1: ArrayList<ReviewsData>) :
        RecyclerView.Adapter<AdapterClass.MyViewHolder>() {
        override fun getItemCount(): Int {

            return arrayList1.size
        }


        internal inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tvName: TextView
            var tvTravelling: TextView
            var tvDate: TextView
            var iv: ImageView
            var tvDescription: TextView
            var ll: LinearLayout
            var ratinBar: RatingBar
            var tvDetails: TextView

            init {
                tvName = view.findViewById(R.id.tvName) as TextView
                tvDetails = view.findViewById(R.id.tvDetails) as TextView
                ratinBar = view.findViewById(R.id.ratinBar) as RatingBar
                tvTravelling = view.findViewById(R.id.tvTravelling) as TextView
                tvDescription = view.findViewById(R.id.tvDescription) as TextView
                tvDate = view.findViewById(R.id.tvDate) as TextView
                iv = view.findViewById(R.id.iv) as ImageView
                ll = view.findViewById(R.id.ll) as LinearLayout
            }

        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_review_list, parent, false)
            return MyViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.tvName.text = arrayList1[position].user_name
            holder.tvDescription.text = arrayList1[position].review_description
            holder.tvTravelling.text =
                arrayList1[position].depart_from + "-" + arrayList1[position].travel_destination
            holder.tvDate.text = ChangeDateFormat.getDateMonthDate(arrayList1[position].created)

            if (arrayList1[position].review_score != null) {
                holder.ratinBar.rating = arrayList1[position].review_score?.toFloat()!!
            }

            Glide.with(this@ReviewsListActivity)
                .load(arrayList1[position].profile_image)
                .apply(RequestOptions.circleCropTransform())
                .into(holder.iv);

            holder.tvDetails.setOnClickListener {
                val mainIntent = Intent(
                    this@ReviewsListActivity,
                    ReviewsDetailActivity::class.java
                ).putExtra("DATA", arrayList1[position])
                startActivity(mainIntent)

            }

            /*    holder.iv.setOnClickListener {
                    if (reviewType.equals("Given")) {
                        val mainIntent = Intent(
                            this@ReviewsListActivity,
                            UserProfileActivity::class.java
                        ).putExtra("ID", arrayList1[position].user_id)
                        startActivity(mainIntent)

                    } else {
                        val mainIntent = Intent(
                            this@ReviewsListActivity,
                            UserProfileActivity::class.java
                        ).putExtra("ID", arrayList1[position].offer_posted_user_id)
                        startActivity(mainIntent)

                    }
                }*/
            /*  holder.tvName.setOnClickListener {
                  if (reviewType.equals("Given")) {
                      val mainIntent = Intent(
                          this@ReviewsListActivity,
                          UserProfileActivity::class.java
                      ).putExtra("ID", arrayList1[position].user_id)
                      startActivity(mainIntent)

                  } else {
                      val mainIntent = Intent(
                          this@ReviewsListActivity,
                          UserProfileActivity::class.java
                      ).putExtra("ID", arrayList1[position].offer_posted_user_id)
                      startActivity(mainIntent)

                  }

              }*/


        }


    }
}