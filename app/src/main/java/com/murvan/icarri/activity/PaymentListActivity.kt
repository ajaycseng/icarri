package com.murvan.icarri.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.murvan.icarri.R
import com.murvan.icarri.models.PaymentData
import com.murvan.icarri.models.UserData
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.ChangeDateFormat
import com.murvan.icarri.utils.Constant
import com.murvan.icarri.utils.Preference
import com.murvan.icarri.utils.toast
import kotlinx.android.synthetic.main.activity_my_travel_list.ivAdd
import kotlinx.android.synthetic.main.activity_my_travel_list.ivBacks
import kotlinx.android.synthetic.main.activity_my_travel_list.tvNoDatas
import kotlinx.android.synthetic.main.activity_offer_list.*
import org.json.JSONException
import org.json.JSONObject
import java.lang.Exception
import java.util.ArrayList
import java.util.HashMap

class PaymentListActivity : AppCompatActivity() {

    var recycleView: RecyclerView? = null
    var arrayList = ArrayList<PaymentData>()
    internal lateinit var mAdapter: AdapterClass
    val gson = Gson()
    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null
    var userdata: UserData? = null
    var postType: String = "Package Details"
    var status: String = "All"
    var selectedPosition: Int = -1

    //
    var pastVisiblesItems = 0
    var visibleItemCount = 0
    var totalItemCount = 0
    private var loading = true
    var pageNumber: Int = 1
    var totalRecords: Int = 0
    var totalPages: Int = 0
    //

    companion object {
        var paymentType: String = "Received"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_list)

        tvNoDatas.setText(resources.getString(R.string.norecord))

        tvNoDatas.setOnClickListener {
            callWSList()
        }

        ivBacks.setOnClickListener {
            finish()
        }

        ivAdd.setOnClickListener {
            startActivity(Intent(this@PaymentListActivity, AddPkgActivity::class.java))

        }

        userdata =
            gson.fromJson(
                Preference.getInstance(this).getString(Constant.USER_DETAIL),
                UserData::class.java
            )


        recycleView = findViewById(R.id.recycleView)

        var layoutManager = LinearLayoutManager(this)
        val layoutManagerPost = LinearLayoutManager(this)
        recycleView?.layoutManager = layoutManager
        recycleView?.itemAnimator = DefaultItemAnimator()
        mAdapter = AdapterClass(arrayList)

        recycleView?.adapter = mAdapter
        mAdapter.notifyDataSetChanged()
        recycleView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(
                recyclerView: RecyclerView,
                dx: Int,
                dy: Int
            ) {

                var visibleItemCount = layoutManager.getChildCount();
                var totalItemCount = layoutManager.getItemCount();
                var firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (totalPages > pageNumber) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                    ) {
                        pageNumber++
                        callWSList()
                    }
                }

            }
        })


        if (paymentType.equals("Received")) {
            btnGiven.setBackgroundDrawable(resources.getDrawable(R.drawable.corner_greybtn))
            btnGiven.setTextColor(resources.getColor(R.color.black))
            btnReceive.setBackgroundDrawable(resources.getDrawable(R.drawable.corner_orange))
            btnReceive.setTextColor(resources.getColor(R.color.white))

        } else {
            btnReceive.setBackgroundDrawable(resources.getDrawable(R.drawable.corner_greybtn))
            btnReceive.setTextColor(resources.getColor(R.color.black))
            btnGiven.setBackgroundDrawable(resources.getDrawable(R.drawable.corner_orange))
            btnGiven.setTextColor(resources.getColor(R.color.white))
        }

        btnReceive.setOnClickListener(View.OnClickListener {


            btnGiven.setBackgroundDrawable(resources.getDrawable(R.drawable.corner_greybtn))
            btnGiven.setTextColor(resources.getColor(R.color.black))
            btnReceive.setBackgroundDrawable(resources.getDrawable(R.drawable.corner_orange))
            btnReceive.setTextColor(resources.getColor(R.color.white))
            paymentType = "Received"
            pageNumber=1

            arrayList.clear()

            callWSList();

        })

        btnGiven.setOnClickListener(View.OnClickListener {
            btnReceive.setBackgroundDrawable(resources.getDrawable(R.drawable.corner_greybtn))
            btnReceive.setTextColor(resources.getColor(R.color.black))
            btnGiven.setBackgroundDrawable(resources.getDrawable(R.drawable.corner_orange))
            btnGiven.setTextColor(resources.getColor(R.color.white))
            paymentType = "Given"
            arrayList.clear()
            pageNumber=1

            callWSList();
        })
    }


    override fun onResume() {
        super.onResume()
        if (pageNumber == 1) {
            arrayList.clear()
            callWSList();
        }
    }
    //

    private fun callWSList() {
        initCallbackDrop()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.PAYMENT_LIST,
            getParamsDrop(),
            "", true
        )
    }


    private fun getParamsDrop(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("payment_type", paymentType)
            jsonStr.put("user_id", userdata?.user_id)
            jsonStr.put("page_number", pageNumber)

            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackDrop() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                if (status.equals("success", ignoreCase = true)) {
                    totalRecords = jsonObject.getInt("total_records")
                    totalPages = jsonObject.getInt("total_pages")


                    val responseData = jsonObject.getJSONObject("response_data")
                    val payementData = responseData.getJSONArray("payment_transactions_list")
                    if (payementData.length() == 0) {
                        tvNoDatas.visibility = View.VISIBLE
                        recycleView?.visibility = View.GONE
                    } else {
                        tvNoDatas.visibility = View.GONE
                        recycleView?.visibility = View.VISIBLE

                        //  arrayList.clear()
                        for (i in 0 until payementData.length()) {
                            val data: PaymentData
                            data =
                                gson.fromJson(
                                    payementData.get(i).toString(),
                                    PaymentData::class.java
                                )
                            arrayList.add(data)
                        }

                        //   mAdapter = AdapterClass(arrayList)
                        //  recycleView?.adapter = mAdapter
                        mAdapter.notifyDataSetChanged()

                    }


                } else {
                    toast(msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                toast(error.toString())
            }
        }
    }


    //


    internal inner class AdapterClass(val arrayList1: ArrayList<PaymentData>) :
        RecyclerView.Adapter<AdapterClass.MyViewHolder>() {
        override fun getItemCount(): Int {

            return arrayList1.size
        }


        internal inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tvName: TextView
            var tvReview: TextView
            var tvChangeStatus: TextView
            var tvStatusText: TextView
            var tvTo: TextView
            var tvCreated: TextView
            var tvPaymentReleastStatus: TextView
            var tvDetails: TextView
            var tvFrom: TextView
            var tvOfferAmount: TextView
            var tvRelease: TextView
            var tvTax: TextView
            var ivOption: ImageView

            var tvDescription: TextView

            var iv: ImageView

            var ll: LinearLayout

            init {
                tvRelease = view.findViewById(R.id.tvRelease) as TextView
                tvPaymentReleastStatus = view.findViewById(R.id.tvPaymentReleastStatus) as TextView
                tvReview = view.findViewById(R.id.tvReview) as TextView
                tvDetails = view.findViewById(R.id.tvDetails) as TextView
                tvName = view.findViewById(R.id.tvName) as TextView
                tvChangeStatus = view.findViewById(R.id.tvChangeStatus) as TextView
                tvCreated = view.findViewById(R.id.tvCreated) as TextView
                tvTo = view.findViewById(R.id.tvTo) as TextView
                tvStatusText = view.findViewById(R.id.tvStatusText) as TextView
                tvFrom = view.findViewById(R.id.tvFrom) as TextView
                tvOfferAmount = view.findViewById(R.id.tvDate) as TextView
                tvTax = view.findViewById(R.id.tvTime) as TextView
                tvDescription = view.findViewById(R.id.tvDescription) as TextView
                ivOption = view.findViewById(R.id.ivOption) as ImageView
                iv = view.findViewById(R.id.iv) as ImageView

                ll = view.findViewById(R.id.ll) as LinearLayout
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_payment_list, parent, false)

            return MyViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.tvName.text = arrayList1[position].user_name
            holder.tvTo.text = arrayList1[position].travel_destination

            holder.tvCreated.text = ChangeDateFormat.getDateMonthDate(arrayList1[position].created)
            holder.tvFrom.text = arrayList1[position].depart_from
            holder.tvDescription.text = "Total: " + arrayList1[position].total_paid_amount

            if (paymentType.equals("Given")) {
                holder.tvOfferAmount.text =
                    "Offer Amount: " + arrayList1[position].offer_amount
                //holder.tvTax.text = "Commission: " + arrayList1[position].commission_amount
//                arrayList1[position].show_payment_release_button="Yes"
//                arrayList1[position].delivery_status="3"
                if (arrayList1[position].delivery_status.equals("3")&& arrayList1[position].show_payment_release_button!=null&&arrayList1[position].show_payment_release_button.equals("Yes")){
                    holder.tvRelease.visibility=View.VISIBLE
                }else{
                    holder.tvRelease.visibility=View.GONE
                }
            } else {
                holder.tvRelease.visibility=View.GONE
                holder.tvOfferAmount.text =
                    "Final Earning: " + arrayList1[position].final_earnings
               // holder.tvTax.text = "Commission: " + arrayList1[position].commission_amount


            }



            if (arrayList1[position].delivery_next_status_code == null) {
                holder.tvChangeStatus.visibility = View.GONE;
                //   holder.tvChangeStatus.text =
                //    Html.fromHtml("<u>" + "Change to:" + arrayList1[position].delivery_next_status + "?" + "</u>")
            }
            if (arrayList1[position].delivery_current_status != null) {
                holder.tvStatusText.text = "Status: " + arrayList1[position].delivery_current_status
            }
            if (arrayList1[position].review_posted != null && arrayList1[position].review_posted.equals(
                    "No"
                )
            ) {
                holder.tvChangeStatus.visibility = View.GONE;
                holder.tvStatusText.visibility = View.GONE;
                holder.tvReview.visibility = View.VISIBLE
            } else {
                holder.tvReview.visibility = View.GONE

            }



            holder.tvChangeStatus.setOnClickListener {
                val popUp = popupWindowStatus(
                    arrayList1[position].delivery_next_status,
                    position,
                    holder.tvChangeStatus
                );
                popUp.showAsDropDown(holder.tvChangeStatus, 0, 0);
                // statusDialog(position);

            }
            if (arrayList1.get(position).payment_released_button_pressed!=null&&arrayList1.get(position).payment_released_button_pressed.equals("1")){
                holder.tvPaymentReleastStatus.text=arrayList1.get(position).payment_released_status;
                holder.tvPaymentReleastStatus.visibility = View.VISIBLE;
            }else{
                holder.tvPaymentReleastStatus.visibility = View.GONE;
            }

            holder.tvRelease.setOnClickListener {
                callPaymentRelease(arrayList1[position]);
            }
            holder.ivOption.setOnClickListener {

                showPopupMenu(holder.ivOption, position)
            }
            holder.tvReview.setOnClickListener {
                val mainIntent = Intent(
                    this@PaymentListActivity,
                    AddReviewsActivity::class.java
                ).putExtra("ID", arrayList1[position].id)
                    .putExtra("USERNAME", arrayList1[position].user_name)
                    .putExtra("IMG_URL", arrayList1[position].profile_image)
                startActivity(mainIntent)
            }

            holder.tvDetails.setOnClickListener {

                val mainIntent = Intent(
                    this@PaymentListActivity,
                    PaymentDetailActivity::class.java
                ).putExtra("ID", arrayList1[position].id)


                startActivity(mainIntent)
            }
            Glide.with(this@PaymentListActivity)
                .load(arrayList1[position].profile_image)
                .apply(RequestOptions.circleCropTransform())
                .into(holder.iv);

        }
    }

    fun popupWindowStatus(
        deliveryNextStatus: String?,
        position: Int,
        tvChangeStatus: TextView
    ): PopupWindow {

        var popupWindow = PopupWindow(this);

        var values = ArrayList<String>()
        if (deliveryNextStatus != null) {
            values.add(deliveryNextStatus)
        }


        var adapter = ArrayAdapter<String>(this, R.layout.dropdown_item, values)
        var listViewSort = ListView(this);
        listViewSort.setAdapter(adapter);
        listViewSort.setOnItemClickListener { parent, view, position, id ->

            popupWindow.dismiss()
            statusDialog(position)
        }
        popupWindow.setFocusable(true);
        popupWindow.setWidth(tvChangeStatus.getWidth())
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setContentView(listViewSort)

        return popupWindow;
    }


    fun showPopupMenu(view: View, position: Int) {

        var popup = PopupMenu(this, view);
        popup.getMenuInflater().inflate(R.menu.list_menu, popup.getMenu())
        popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { it ->
            val item = it!!.itemId
            when (item) {
                R.id.menuEdit -> {
                    startActivity(
                        Intent(
                            this@PaymentListActivity,
                            AddPkgActivity::class.java
                        ).putExtra("DATA", arrayList.get(position))
                    )
                }
                R.id.menuDelete -> {
                    statusDialog(position)
                }

                R.id.menuAdditional -> {
                    val mainIntent = Intent(
                        this@PaymentListActivity,
                        AdditionalImgActivity::class.java
                    ).putExtra("ID", arrayList[position].id).putExtra(
                        "TITLES",
                        arrayList[position].depart_from + " To " + arrayList[position].travel_destination
                    )
                    startActivity(mainIntent)
                }
            }
            true

        })


        popup.show();


    }


    fun statusDialog(position: Int) {
        val builder = AlertDialog.Builder(this)
        //set message for alert dialog

        builder.setMessage("Do you want to Change  " + arrayList[position].delivery_current_status + " to " + arrayList[position].delivery_next_status + "?")
        builder.setIcon(android.R.drawable.ic_dialog_alert)

        //performing positive action
        builder.setPositiveButton("Yes") { dialogInterface, which ->
            selectedPosition = position
            callWSChangeStatus()

        }
        builder.setNegativeButton("No") { dialogInterface, which ->

        }
        //performing cancel action
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }


    private fun callWSChangeStatus() {
        initCallbackDelete()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.PAYMENT_STATUS,
            getParamsDelete(),
            "", true
        )
    }

    private fun getParamsDelete(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("user_id", userdata?.user_id)
            jsonStr.put("delivery_status", arrayList[selectedPosition].delivery_next_status_code)
            jsonStr.put("payment_id", arrayList.get(selectedPosition).id)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackDelete() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                if (status.equals("success", ignoreCase = true)) {
                    toast(msg)
                    arrayList.clear()
                    callWSList()
                } else {
                    toast(msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                toast(error.toString())
            }
        }
    }


    //
//......................Payment Release WS

    private fun callPaymentRelease(paymentData: PaymentData) {
        initCallbackPR()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.PAYMENT_RELEASE+"/requestrelease",
            getParamsPR(paymentData),
            "", true
        )
    }

    private fun getParamsPR(paymentData: PaymentData): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("user_id", userdata?.user_id)
            jsonStr.put("payment_id", paymentData.id)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackPR() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                if (status.equals("success", ignoreCase = true)) {
                    toast(msg)
                    arrayList.clear()
                    callWSList()
                } else {
                    toast(msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                toast(error.toString())
            }
        }
    }


    //...............

}
