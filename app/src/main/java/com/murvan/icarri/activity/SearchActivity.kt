package com.murvan.icarri.activity

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.WindowManager
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import android.widget.PopupWindow
import androidx.appcompat.widget.AppCompatEditText
import com.android.volley.VolleyError
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.murvan.icarri.R
import com.murvan.icarri.models.DropListData
import com.murvan.icarri.models.TravelPkgData
import com.murvan.icarri.models.UserData
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.ChangeDateFormat
import com.murvan.icarri.utils.snackBar
import kotlinx.android.synthetic.main.activity_add_travel.*
import kotlinx.android.synthetic.main.activity_pkt_travel.*
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.activity_search.etDepartFrom
import kotlinx.android.synthetic.main.activity_search.etDestination
import kotlinx.android.synthetic.main.activity_search.etDimensionUnit
import kotlinx.android.synthetic.main.activity_search.etTravelDate
import kotlinx.android.synthetic.main.activity_search.etTravelMode
import kotlinx.android.synthetic.main.activity_search.etWeightUnit
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class SearchActivity : AppCompatActivity() {

    var departFrom: String = ""
    var destination: String = ""
    var date: String = ""
    var mode: String = ""
    var isPkt: Boolean = false

    var weight: String? = null
    var weightUnit: String? = null

    var pkgNo: String? = null
    var dLength: String? = null
    var dWidth: String? = null
    var dHeight: String? = null
    var type: String? = "Travel"
    var dimentionsUnit: String? = null

    val gson = Gson()
    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null

    var dropListData = ArrayList<DropListData>()


    var values = ArrayList<String>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        callWSDrop()
        onClickes()
    }

    private fun onClickes() {

        etDepartFrom.setOnClickListener {
            startActivityForResult(Intent(this, TownListActivity::class.java), 1)
        }
        etDestination.setOnClickListener {
            startActivityForResult(Intent(this, TownListActivity::class.java), 2)
        }


        ivBacks.setOnClickListener(View.OnClickListener {
            finish()
        })
        btnReset.setOnClickListener(View.OnClickListener {

            departFrom = etDepartFrom.setText("").toString().toString()
            destination = etDestination.setText("").toString()
            date = etTravelDate.setText("").toString()
            mode = etTravelMode.setText("").toString()
            weight = etWeights.setText("").toString()
            weightUnit = etWeightUnit.setText("").toString()
            pkgNo = etNumPkgs.setText("").toString()
            dLength = etLengths.setText("").toString()
            dWidth = etWidths.setText("").toString()
            dHeight = etHeights.setText("").toString()
            dimentionsUnit = etDimensionUnit.setText("").toString()

        })

        btnTravel.setOnClickListener(View.OnClickListener {
            btnPkg.setBackgroundColor(resources.getColor(R.color.greylight))
            btnPkg.setTextColor(resources.getColor(R.color.grey))
            btnTravel.setBackgroundColor(resources.getColor(R.color.colorAccent))
            btnTravel.setTextColor(resources.getColor(R.color.white))
            //llPkt.visibility = View.GONE
            llTravel.visibility = View.VISIBLE
            etDepartFrom.setHint("Depart from")
            etDestination.setHint("Destination")
            etTravelDate.setHint("Travel Date")
            type = "Travel"

            isPkt = false

        })

        btnPkg.setOnClickListener(View.OnClickListener {
            btnTravel.setBackgroundColor(resources.getColor(R.color.greylight))
            btnTravel.setTextColor(resources.getColor(R.color.grey))
            btnPkg.setBackgroundColor(resources.getColor(R.color.colorAccent))
            btnPkg.setTextColor(resources.getColor(R.color.white))
            // llPkt.visibility = View.VISIBLE
            llTravel.visibility = View.GONE

            etDepartFrom.setHint("Pick Up From")
            etDestination.setHint("Deliver To")
            etTravelDate.setHint("Date to Move")

            type = "Pkg"

            isPkt = true
        })


        btnSearch.setOnClickListener(View.OnClickListener {
            validateData()

        })
        etTravelDate.setOnClickListener(View.OnClickListener {
            getDate(etTravelDate)

        })

        etTravelMode.setOnClickListener {
            val popUp = popupWindowsort();
            popUp.showAsDropDown(etTravelMode, 0, 0);
        }


        etWeightUnit.setOnClickListener {

            values.clear()
            values.add("Kg")
            values.add("Pounds")
            val popUp = popupWindowsort(etWeightUnit);
            popUp.showAsDropDown(etWeightUnit, 0, 0);
        }
        etDimensionUnit.setOnClickListener {
            values.clear()
            values.add("CM")
            values.add("Inches")
            dropListData
            val popUp = popupWindowsort(etDimensionUnit);
            popUp.showAsDropDown(etDimensionUnit, 0, 0);
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 1) {
                val value = data?.getStringExtra("TOWN")
                etDepartFrom.setText(value)
            }
            if (requestCode == 2) {
                val value = data?.getStringExtra("TOWN")
                etDestination.setText(value)
            }
            return
        }
    }

    fun popupWindowsort(editText: AppCompatEditText): PopupWindow {
        var popupWindow = PopupWindow(this);

/*  var values = ArrayList<String>()

for (str in dropListData) {
  values.add(str.type.toString())
}*/


        var adapter = ArrayAdapter<String>(this, R.layout.dropdown_item, values)
        var listViewSort = ListView(this);
        listViewSort.setAdapter(adapter);
        listViewSort.setOnItemClickListener { parent, view, position, id ->
            editText.setText(values[position])
            popupWindow.dismiss()
        }
        popupWindow.setFocusable(true);
        popupWindow.setWidth(etWeightUnit.getWidth())
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setContentView(listViewSort)

        return popupWindow;
    }


    private fun getDate(etTravelDate: AppCompatEditText?) {

        val c = Calendar.getInstance()

        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        val dpd = DatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->


                var month = monthOfYear + 1
                var date = ChangeDateFormat.getDateYYYYMMDD("$year-$month-$dayOfMonth")

                etTravelDate?.setText(date)
            },
            year,
            month,
            day
        )
        dpd.datePicker.minDate = System.currentTimeMillis()

        dpd.show()
    }


    private fun validateData() {

        departFrom = etDepartFrom.text.toString().trim()
        destination = etDestination.text.toString().trim()
        date = etTravelDate.text.toString().trim()
        mode = etTravelMode.text.toString().trim()
        weight = etWeights.text.toString().trim()
        weightUnit = etWeightUnit.text.toString().trim()
        pkgNo = etNumPkgs.text.toString().trim()
        dLength = etLengths.text.toString().trim()
        dWidth = etWidths.text.toString().trim()
        dHeight = etHeights.text.toString().trim()
        dimentionsUnit = etDimensionUnit.text.toString().trim()


        if (TextUtils.isEmpty(departFrom)) {
            btnSearch.snackBar(btnSearch, etDepartFrom.hint.toString() + "!!")
            return
        }
        if (TextUtils.isEmpty(destination)) {
            btnSearch.snackBar(btnSearch, etDestination.hint.toString() + "!!")
            return
        }
        if (destination.equals(departFrom)) {
            if (type.equals("Travel")) {
                btnSearch.snackBar(btnSearch, "Depart and Destination should not be same!!")
            } else {
                btnSearch.snackBar(btnSearch, "Pickup From and Deliver to should not be same!!")

            }
            return;
        }

        if (!weight.isNullOrEmpty() && weightUnit.isNullOrEmpty()) {
            btnSearch.snackBar(btnSearch, "Weight Unit!!")
            return
        }

        if (!dLength.isNullOrEmpty() && dWidth.isNullOrEmpty()) {
            btnSearch.snackBar(btnSearch, "Dimension Width!!")
            return
        }
        if (!dLength.isNullOrEmpty() && !dWidth.isNullOrEmpty() && dHeight.isNullOrEmpty()) {
            btnSearch.snackBar(btnSearch, "Dimension Height!!")
            return
        }
        if (!dLength.isNullOrEmpty() && !dWidth.isNullOrEmpty() && !dHeight.isNullOrEmpty() && dimentionsUnit.isNullOrEmpty()) {
            btnSearch.snackBar(btnSearch, "Dimension Unit!!")
            return
        }
        // btnSearch.snackBar(btnSearch, "Bingo!!")
        var travelPkgData = TravelPkgData()
        travelPkgData.depart_from = departFrom
        travelPkgData.travel_destination = destination

        if (type.equals("Pkg")) {
            travelPkgData.date_to_move = date
        } else {
            travelPkgData.travel_date = date
        }
        travelPkgData.travel_mode = mode
        travelPkgData.max_weight = weight
        travelPkgData.max_weight_unit = weightUnit
        travelPkgData.dimension_packet_length = dLength
        travelPkgData.dimension_packet_width = dWidth
        travelPkgData.dimension_packet_height = dHeight

        travelPkgData.dimension_packet_unit = dimentionsUnit
        travelPkgData.max_packets = pkgNo
        if (isPkt) {
            startActivity(
                Intent(this@SearchActivity, SearchPktListActivity::class.java).putExtra(
                    "DATA",
                    travelPkgData
                )
            )

        } else {
            startActivity(
                Intent(this@SearchActivity, SearchTravelListActivity::class.java).putExtra(
                    "DATA",
                    travelPkgData
                )
            )
        }


    }

    fun popupWindowsort(): PopupWindow {
        var popupWindow = PopupWindow(this);

        var values = ArrayList<String>()

        for (str in dropListData) {
            values.add(str.type.toString())
        }


        var adapter = ArrayAdapter<String>(this, R.layout.dropdown_item, values)
        var listViewSort = ListView(this);
        listViewSort.setAdapter(adapter);
        listViewSort.setOnItemClickListener { parent, view, position, id ->
            etTravelMode.setText(values[position])
            popupWindow.dismiss()
        }
        popupWindow.setFocusable(true);
        popupWindow.setWidth(etTravelMode.getWidth())
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setContentView(listViewSort)

        return popupWindow;
    }

    private fun callWSDrop() {
        initCallbackDrop()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.DROP_LIST_OPTION,
            getParamsDrop(),
            "", true
        )
    }


    private fun getParamsDrop(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("dropdown_type", "travelMode")
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackDrop() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                if (status.equals("success", ignoreCase = true)) {

                    val responseData = jsonObject.getJSONArray("response_data")

                    for (i in 0 until responseData.length()) {
                        val data: DropListData
                        data =
                            gson.fromJson(responseData.get(i).toString(), DropListData::class.java)
                        dropListData.add(data)
                    }


                } else {
                    btnSearch.snackBar(btnSearch, msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                btnSearch.snackBar(btnSearch, error.toString())
            }
        }
    }
}
