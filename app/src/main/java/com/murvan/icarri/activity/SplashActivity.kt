package com.murvan.icarri.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.murvan.icarri.R
import com.murvan.icarri.utils.Constant
import com.murvan.icarri.utils.Preference

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        goNext()
    }


    override fun onResume() {
        super.onResume()
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("HomeActivity", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                HomeActivity.token = task.result?.token.toString()

            })
    }


    private fun goNext() {
        Handler().postDelayed({


            if (Preference.getInstance(this@SplashActivity).getString(Constant.USER_ID)
                    .equals("")
            ) {
                startActivity(Intent(this@SplashActivity, OptionActivity::class.java))
            } else {
                startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
            }

            finish()
        }, 3000)

    }


}
