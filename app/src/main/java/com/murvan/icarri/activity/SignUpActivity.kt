package com.murvan.icarri.activity

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.view.Window
import android.widget.TextView
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.app.ActivityCompat
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.murvan.icarri.R
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.snackBar
import com.murvan.icarri.utils.toast
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.activity_my_travel_list.*
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.activity_term.*
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.lang.Exception
import java.net.MalformedURLException
import java.util.*


class SignUpActivity : BaseActivity() {

    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null

    var firstName: String? = null
    var lastName: String? = null
    var email: String? = null
    var password: String? = null
    var confPass: String? = null
    var username: String? = null
    var city: String? = null
    var country: String? = null
    var phone: String? = null
    var msgTerms: String? = ""
    var dob: String? = null
    var bitmap: Bitmap? = null
    var resultUri: Uri? = null
    var isImgAttached: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)


        ivBack.setOnClickListener {
            finish()
        }



        etDob.setOnClickListener {
            getDate(etDob)
        }
        btnSignUP.setOnClickListener {

            validation()
        }
        ivImage.setOnClickListener {
            storagePermission()
        }

        cbTermCondition.setOnClickListener {
            cbTermCondition.isChecked = false
            showDialogTerms(msgTerms)
        }
        callWSListTerms()
    }

    private fun showDialogTerms(msgTerms: String?) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.activity_term)
        val text: TextView = dialog.findViewById(R.id.tvText) as TextView
        text.setText(msgTerms)
        val tvAgree: TextView = dialog.findViewById(R.id.tvAgree) as TextView
        tvAgree.setOnClickListener {
            cbTermCondition.isChecked = true

            dialog.dismiss()
        }
        dialog.show()
    }


    private fun getDate(etDob: AppCompatEditText?) {

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        val dpd = DatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                etDob?.setText("$year-$monthOfYear-$dayOfMonth")
            },
            year,
            month,
            day
        )
        dpd.datePicker.maxDate = System.currentTimeMillis() - 568025136000L

        dpd.show()
    }

    private fun validation() {

        firstName = etFName.text.toString().trim()
        lastName = etLName.text.toString().trim()
        email = etEmail.text.toString().trim()
        city = etCity.text.toString().trim()
        country = etCountry.text.toString().trim()
        phone = etPhone.text.toString().trim()
        username = etUserName.text.toString().trim()
        password = etPass.text.toString().trim()
        confPass = etConPass.text.toString().trim()
        dob = etDob.text.toString().trim()

        if (firstName.isNullOrEmpty()) {
            btnSignUP.snackBar(btnSignUP, "First Name!!")
        } else if (lastName.isNullOrEmpty()) {
            btnSignUP.snackBar(btnSignUP, "Last Name!!")
        } else if (email.isNullOrEmpty()) {
            btnSignUP.snackBar(btnSignUP, "Email Name!!")
        } else if (city.isNullOrEmpty()) {
            btnSignUP.snackBar(btnSignUP, "City!!")
        } else if (country.isNullOrEmpty()) {
            btnSignUP.snackBar(btnSignUP, "Country Name!!")
        } else if (phone.isNullOrEmpty()) {
            btnSignUP.snackBar(btnSignUP, "Phone number!!")
        } else if (dob.isNullOrEmpty()) {
            btnSignUP.snackBar(btnSignUP, "Date of Birth!!")
        } else if (username.isNullOrEmpty()) {
            btnSignUP.snackBar(btnSignUP, "Username!!")
        } else if (password.isNullOrEmpty()) {
            btnSignUP.snackBar(btnSignUP, "Password!!")
        } else if (confPass.isNullOrEmpty()) {
            btnSignUP.snackBar(btnSignUP, "Confirm Password!!")
        } else if (!password.equals(confPass)) {
            btnSignUP.snackBar(btnSignUP, "Password not match!!")
        } else if (!isImgAttached) {
            btnSignUP.snackBar(btnSignUP, "Click on user to attach image!!")
        } else if (!cbTermCondition.isChecked) {
            btnSignUP.snackBar(btnSignUP, "Please select Terms and Condition!!")
        } else {

            callWS();

        }


    }


    private fun callWS() {
        initCallback()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.sendImgRequest(
            this,
            "POST",
            "profile_image",
            bitmap,
            URLS.SIGN_UP,
            getParamsLogin(),
            ""
        )
    }


    private fun getParamsLogin(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {


            jsonStr.put("first_name", firstName)
            jsonStr.put("last_name", lastName)
            jsonStr.put("email_address", email)
            jsonStr.put("password", password)
            jsonStr.put("username", username)
            jsonStr.put("city_town", city)
            jsonStr.put("country", country)
            jsonStr.put("mobile_number", phone)
            jsonStr.put("dob", dob)


            jsonStr.put("device_type", "Android")
            jsonStr.put(
                "device_id",
                HomeActivity.token
            ); //FirebaseInstanceId.getInstance().getToken());
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallback() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) {


                try {
                    val jsonObject = JSONObject(response)
                    val status = jsonObject.getString("response_status")
                    val msg = jsonObject.getString("response_msg")
                    if (status.equals("success", ignoreCase = true)) {

                        val mainIntent =
                            Intent(this@SignUpActivity, SignUpVerifyActivity::class.java)
                        mainIntent.putExtra("EMAIL", email)
                        startActivity(mainIntent)
                        // btnSignUP.snackBar(btnSignUP, msg)
                        // btnSignUP.snackBar(btnSignUP, "Request is sent to your email address.")
                        // finish()
                    } else {
                        btnSignUP.snackBar(btnSignUP, msg)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                btnSignUP.snackBar(btnSignUP, error.toString())
            }
        }
    }


    private fun pickImage() {
        CropImage.activity()
            .setMinCropWindowSize(200, 200)
            .setFixAspectRatio(true)
            .start(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode === CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode === Activity.RESULT_OK) {
                resultUri = result.uri

                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                isImgAttached = true
                try {


                    Glide.with(this)
                        .load(resultUri)
                        .apply(RequestOptions.circleCropTransform())
                        .into(ivImage);


                } catch (e: MalformedURLException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                //appCache.getUser()?.showImage(userPic, File(resultUri.path).absolutePath)
            } else if (resultCode === CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
                toast(error.localizedMessage)
            }
        }
    }


    private fun storagePermission() {

        if (ActivityCompat.checkSelfPermission(
                this!!,
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this!!,
                arrayOf(
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), 100
            )
        } else {
            pickImage()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {

        if (requestCode == 100 && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            pickImage()
        } else {
            toast("Permission not granted!!")
        }
    }

    private fun callWSListTerms() {
        initCallbackTerms()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.TERM_CONDITION,
            getParamsTerms(),
            "", true
        )
    }


    private fun getParamsTerms(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("page_slug", "terms")
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackTerms() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                if (status.equals("success", ignoreCase = true)) {
                    val responseData = jsonObject.getJSONObject("response_data")
                    msgTerms = responseData.getString("static_page_description")

                } else {
                    toast(msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) {
                toast(error.toString())
            }
        }


    }
}

