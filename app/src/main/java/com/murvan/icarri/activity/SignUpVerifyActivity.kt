package com.murvan.icarri.activity

import android.content.Intent
import android.os.Bundle
import com.android.volley.VolleyError
import com.google.firebase.iid.FirebaseInstanceId
import com.murvan.icarri.R
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.hideKeyboard
import com.murvan.icarri.utils.snackBar
import kotlinx.android.synthetic.main.activity_forgot_pass.btnSubmit
import kotlinx.android.synthetic.main.activity_forgot_pass.ivBack
import kotlinx.android.synthetic.main.activity_signup_verify.*
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap

class SignUpVerifyActivity : BaseActivity() {

    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null

    var otp: String? = null
    var email: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup_verify)

        email = intent.extras?.getString("EMAIL")

        btnSubmit.setOnClickListener {
            hideKeyboard(btnSubmit)
            otp = etOTP.text.toString().trim()

            if (otp.isNullOrEmpty()) {
                btnSubmit.snackBar(btnSubmit, "Write enter verification code.")
            } else {
                callWS();
            }


        }

        ivBack.setOnClickListener {

            finish()
        }
    }


    private fun callWS() {
        initCallback()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(this, "POST", URLS.SIGN_UP_VERIFY, getParamsLogin(), "",true)
    }


    private fun getParamsLogin(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {


            jsonStr.put("email_address", email)
            jsonStr.put("six_digit_code", otp)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token); //FirebaseInstanceId.getInstance().getToken());
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }

    private fun initCallback() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) {
                try {
                    val jsonObject = JSONObject(response)
                    val status = jsonObject.getString("response_status")
                    val msg = jsonObject.getString("response_msg")
                    if (status.equals("success", ignoreCase = true)) {

                     var inten=   Intent(this@SignUpVerifyActivity, LoginActivity::class.java)
                        startActivity(inten)
                        finishAffinity()

                    } else {
                        btnSubmit.snackBar(btnSubmit, msg)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                btnSubmit.snackBar(btnSubmit, error.toString())
            }
        }
    }






}
