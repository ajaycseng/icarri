package com.murvan.icarri.activity

import android.annotation.TargetApi
import android.app.ProgressDialog
import android.content.Context
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.murvan.icarri.R
import com.murvan.icarri.activity.OfferListActivity.Companion.isPaymentProcess
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.utils.PaymentStatus
import com.murvan.icarri.utils.Utilities


class PaymentWVActivity : AppCompatActivity(), PaymentStatus {
    private var webView: WebView? = null

//    private val CLICK_ON_WEBVIEW = 1
//    private val CLICK_ON_URL = 2
  //  private val handler: Handler = Handler(this)
    var userID=""
    var offerID=""
    var URL=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_w_v)

        offerID = intent.getStringExtra("OFFER_ID")
        userID= intent.getStringExtra("USERID")


        URL=URLS.PAYMENT_WV_DETAIL+offerID+"/"+userID
        val webview = findViewById<View>(R.id.webView) as WebView
        val settings = webview.settings
        settings.javaScriptEnabled = true
        webview.scrollBarStyle = WebView.SCROLLBARS_OUTSIDE_OVERLAY

        val pdfWebViewClient = PdfWebViewClient(this, webview,this)
        pdfWebViewClient.loadPdfUrl(
            URL
        )

    }


     class PdfWebViewClient(context: Context, webView: WebView, paymentStatus: PaymentStatus) :
        WebViewClient() {
        private val mContext: Context
        private val mWebView: WebView
        private var mProgressDialog: ProgressDialog? = null
         val paymentStatus :PaymentStatus?=paymentStatus;
       // private var isLoadingPdfUrl = false
        fun loadPdfUrl(url: String) {
            mWebView.stopLoading()
            if (!TextUtils.isEmpty(url)) {
                //isLoadingPdfUrl = isPdfUrl(url)
               // if (isLoadingPdfUrl) {
                 //   mWebView.clearHistory()
                //}
                showProgressDialog()
            }
            mWebView.loadUrl(url)
        }

        override fun shouldOverrideUrlLoading(
            webView: WebView,
            url: String
        ): Boolean {
            return shouldOverrideUrlLoading(url)
        }

        override fun onReceivedError(
            webView: WebView,
            errorCode: Int,
            description: String,
            failingUrl: String
        ) {
            handleError(errorCode, description, failingUrl)
        }

        @TargetApi(Build.VERSION_CODES.N)
        override fun shouldOverrideUrlLoading(
            webView: WebView,
            request: WebResourceRequest
        ): Boolean {
            val uri: Uri = request.url
            return shouldOverrideUrlLoading(webView, uri.toString())
        }

        @TargetApi(Build.VERSION_CODES.N)
        override fun onReceivedError(
            webView: WebView,
            request: WebResourceRequest,
            error: WebResourceError
        ) {
            val uri: Uri = request.url
            handleError(error.errorCode, error.description.toString(), uri.toString())
        }

        override fun onPageFinished(view: WebView, url: String) {

            dismissProgressDialog()
            if (url.equals(URLS.PAYMENT_WV_SUCCESS)){
                paymentStatus?.setStatus(1)

            }else if (url.equals(URLS.PAYMENT_WV_CANCEL)){
                paymentStatus?.setStatus(2)

            }else if (url.equals(URLS.PAYMENT_WV_DECLINE)){
                paymentStatus?.setStatus(3)
            }
            Log.i(TAG, "Finished loading. URL : $url")

        }

        private fun shouldOverrideUrlLoading(url: String): Boolean {
            Log.i(
                TAG,
                "shouldOverrideUrlLoading() URL : $url"
            )
            showProgressDialog()
           /* if (!isLoadingPdfUrl && isPdfUrl(url)) {
                mWebView.stopLoading()
                val pdfUrl = PDF_VIEWER_URL + url
                Handler().postDelayed(Runnable { loadPdfUrl(pdfUrl) }, 300)
                return true
            }*/
            return false // Load url in the webView itself
        }

        private fun handleError(
            errorCode: Int,
            description: String,
            failingUrl: String
        ) {
            Log.e(
                TAG,
                "Error : $errorCode, $description URL : $failingUrl"
            )
        }

        private fun showProgressDialog() {
            dismissProgressDialog()
            mProgressDialog = ProgressDialog.show(mContext, "", "Loading...")
        }

        private fun dismissProgressDialog() {
            if (mProgressDialog != null && mProgressDialog!!.isShowing) {
                mProgressDialog!!.dismiss()
                mProgressDialog = null
            }
        }

        /*private fun isPdfUrl(url: String): Boolean {
            var url = url
            if (!TextUtils.isEmpty(url)) {
                url = url.trim { it <= ' ' }
                val lastIndex =
                    url.toLowerCase().lastIndexOf(PDF_EXTENSION)
                if (lastIndex != -1) {
                    return url.substring(lastIndex)
                        .equals(PDF_EXTENSION, ignoreCase = true)
                }
            }
            return false
        }*/

        companion object {
            private const val TAG = "PAYMENT"
        }

        init {
            mContext = context
            mWebView = webView
            mWebView.webViewClient = this
        }
    }


    override fun setStatus(status: Int) {
        if (status==1){
            isPaymentProcess=true
            finish()
            Utilities.showDialog(this,"Payment is successfully Done")
        }else if (status==2){
            isPaymentProcess=true
            finish()
            Utilities.showDialog(this,"Payment is Declined")
        }else{
            isPaymentProcess=true
            finish()
            Utilities.showDialog(this,"Payment is Cancelled")
        }
    }


}