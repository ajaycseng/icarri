package com.murvan.icarri.activity

import android.app.Activity
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.app.ActivityCompat
import com.android.volley.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.murvan.icarri.R
import com.murvan.icarri.models.UserData
import com.murvan.icarri.networkcall.*
import com.murvan.icarri.utils.*
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.activity_sign_up.btnSignUP
import kotlinx.android.synthetic.main.activity_sign_up.etCity
import kotlinx.android.synthetic.main.activity_sign_up.etCountry
import kotlinx.android.synthetic.main.activity_sign_up.etDob
import kotlinx.android.synthetic.main.activity_sign_up.etEmail
import kotlinx.android.synthetic.main.activity_sign_up.etFName
import kotlinx.android.synthetic.main.activity_sign_up.etLName
import kotlinx.android.synthetic.main.activity_sign_up.etPhone
import kotlinx.android.synthetic.main.activity_sign_up.etUserName
import kotlinx.android.synthetic.main.activity_sign_up.ivBack
import kotlinx.android.synthetic.main.activity_sign_up.ivImage

import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.net.MalformedURLException
import java.time.Year
import java.util.*


class EditProfileActivity : BaseActivity() {

    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null

    var firstName: String? = null
    var lastName: String? = null
    var email: String? = null
    var username: String? = null
    var city: String? = null
    var country: String? = null
    var phone: String? = null
    var dob: String? = null
    var bitmap: Bitmap? = null
    var resultUri: Uri? = null
    var isImgAttached: Boolean = false

    private val gson = Gson()

    var userdata: UserData? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)


        userdata =
            gson.fromJson(
                Preference.getInstance(this).getString(Constant.USER_DETAIL),
                UserData::class.java
            )




        ivBack.setOnClickListener {
            finish()
        }
        btnRemoveImg.setOnClickListener {
            if (isImgAttached) {
                bitmap = null
                ivImage.setImageResource(R.drawable.avtar)
                btnRemoveImg.visibility = View.GONE
                isImgAttached = false
            } else {
                callWSRemoveImg()
            }


        }

        etDob.setOnClickListener {
            getDate(etDob)
        }
        btnSignUP.setOnClickListener {

            validation()
        }
        ivImage.setOnClickListener {
            storagePermission()
        }
        setData()
    }

    private fun setData() {


        userdata?.let {

            etFName.setText(userdata?.first_name)
            etLName.setText(userdata?.last_name)
            etEmail.setText(userdata?.email_address)
            etCity.setText(userdata?.city_town)
            etCountry.setText(userdata?.country)
            etPhone.setText(userdata?.mobile_number)
            etUserName.setText(userdata?.username)
            etDob.setText(userdata?.dob)


            if (!userdata?.profile_image.isNullOrBlank()) {
                Glide.with(this)
                    .load(userdata?.profile_image)
                    .apply(RequestOptions.circleCropTransform())
                    .into(ivImage);
                btnRemoveImg.visibility = View.VISIBLE
            }


        }

    }

    private fun getDate(etDob: AppCompatEditText?) {

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        val dpd = DatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                var month = monthOfYear + 1
                var date = ChangeDateFormat.getDateYYYYMMDD("$year-$month-$dayOfMonth")

                etDob?.setText(date)

                //etDob?.setText("$year-$monthOfYear-$dayOfMonth")
            },
            year,
            month,
            day
        )
        dpd.datePicker.maxDate = System.currentTimeMillis() - 568025136000L

        dpd.show()
    }

    private fun validation() {

        firstName = etFName.text.toString().trim()
        lastName = etLName.text.toString().trim()
        email = etEmail.text.toString().trim()
        city = etCity.text.toString().trim()
        country = etCountry.text.toString().trim()
        phone = etPhone.text.toString().trim()
        username = etUserName.text.toString().trim()
        dob = etDob.text.toString().trim()

        if (firstName.isNullOrEmpty()) {
            btnSignUP.snackBar(btnSignUP, "First Name!!")
        } else if (lastName.isNullOrEmpty()) {
            btnSignUP.snackBar(btnSignUP, "Last Name!!")
        } else if (email.isNullOrEmpty()) {
            btnSignUP.snackBar(btnSignUP, "Email Name!!")
        } else if (city.isNullOrEmpty()) {
            btnSignUP.snackBar(btnSignUP, "City!!")
        } else if (country.isNullOrEmpty()) {
            btnSignUP.snackBar(btnSignUP, "Country Name!!")
        } else if (phone.isNullOrEmpty()) {
            btnSignUP.snackBar(btnSignUP, "Phone number!!")
        } else if (dob.isNullOrEmpty()) {
            btnSignUP.snackBar(btnSignUP, "Date of Birth!!")
        } else if (username.isNullOrEmpty()) {
            btnSignUP.snackBar(btnSignUP, "Username!!")
        }
        /*   else if (!isImgAttached) {

               btnSignUP.snackBar(btnSignUP, "Click on user to attach image!!")

           } */
        else {

            callWS();

        }


    }


    private fun callWS() {
        initCallback()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.sendImgRequest(
            this,
            "POST",
            "profile_image",
            bitmap,
            URLS.EDIT_PROFILE,
            getParamsLogin(),
            ""
        )
    }


    private fun getParamsLogin(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {

            jsonStr.put("user_id", userdata?.user_id)
            jsonStr.put("first_name", firstName)
            jsonStr.put("last_name", lastName)
            jsonStr.put("email_address", email)
            jsonStr.put("username", username)
            jsonStr.put("city_town", city)
            jsonStr.put("country", country)
            jsonStr.put("mobile_number", phone)
            jsonStr.put("dob", dob)


            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token); //FirebaseInstanceId.getInstance().getToken());
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallback() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) {


                try {
                    val jsonObject = JSONObject(response)
                    val status = jsonObject.getString("response_status")
                    val msg = jsonObject.getString("response_msg")
                    if (status.equals("success", ignoreCase = true)) {

                        val responseData = jsonObject.getJSONObject("response_data")


                        Preference.getInstance(this@EditProfileActivity)
                            .putString(Constant.USER_DETAIL, responseData.toString())

                        btnSignUP.snackBar(btnSignUP, msg)

                        /*   val mainIntent =
                               Intent(this@EditProfileActivity, SignUpVerifyActivity::class.java)
                           mainIntent.putExtra("EMAIL", email)
                           startActivity(mainIntent)*/
                        // btnSignUP.snackBar(btnSignUP, msg)
                        // btnSignUP.snackBar(btnSignUP, "Request is sent to your email address.")
                        // finish()
                    } else {
                        btnSignUP.snackBar(btnSignUP, msg)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                btnSignUP.snackBar(btnSignUP, error.toString())
            }
        }
    }


    private fun pickImage() {
        CropImage.activity()
            .setMinCropWindowSize(200, 200)
            .setFixAspectRatio(true)
            .start(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode === CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode === Activity.RESULT_OK) {
                resultUri = result.uri

                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                isImgAttached = true
                btnRemoveImg.visibility = View.VISIBLE
                try {


                    Glide.with(this)
                        .load(resultUri)
                        .apply(RequestOptions.circleCropTransform())
                        .into(ivImage);


                } catch (e: MalformedURLException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                //appCache.getUser()?.showImage(userPic, File(resultUri.path).absolutePath)
            } else if (resultCode === CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
                toast(error.localizedMessage)
            }
        }
    }


    private fun storagePermission() {

        if (ActivityCompat.checkSelfPermission(
                this!!,
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this!!,
                arrayOf(
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), 100
            )
        } else {
            pickImage()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {

        if (requestCode == 100 && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            pickImage()
        } else {
            toast("Permission not granted!!")
        }
    }


    // Remove image

    private fun callWSRemoveImg() {
        initCallbackRemoveImg()
        mVolleyService = VolleyService(resultCallback, this@EditProfileActivity)
        mVolleyService!!.postStringRequest(
            this@EditProfileActivity,
            "POST",
            URLS.REMOVE_IMG,
            getParamsRemoveImg(),
            "",true
        )
    }


    private fun getParamsRemoveImg(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("email_address", userdata?.email_address)
            jsonStr.put("user_id", userdata?.user_id)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token); //FirebaseInstanceId.getInstance().getToken());
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }

    private fun initCallbackRemoveImg() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) {
                try {
                    val jsonObject = JSONObject(response)
                    val status = jsonObject.getString("response_status")
                    val msg = jsonObject.getString("response_msg")
                    if (status.equals("success", ignoreCase = true)) {
                        val responseData = jsonObject.getJSONObject("response_data")
                        btnRemoveImg.visibility = View.GONE
                        ivImage.setImageResource(R.drawable.avtar)
                        Preference.getInstance(this@EditProfileActivity)
                            .putString(Constant.USER_DETAIL, responseData.toString())
                        btnRemoveImg.snackBar(btnRemoveImg, msg)

                    } else {
                        btnRemoveImg.snackBar(btnRemoveImg, msg)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                btnRemoveImg.snackBar(btnRemoveImg, error.toString())
            }
        }
    }

    //


}

