package com.murvan.icarri.activity

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.android.volley.VolleyError
import com.google.firebase.iid.FirebaseInstanceId
import com.murvan.icarri.R
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.hideKeyboard
import com.murvan.icarri.utils.snackBar
import com.murvan.icarri.utils.toast
import com.murvan.icarri.view.MyOnClickListner
import com.murvan.icarri.viewmodels.AuthViewModel
import kotlinx.android.synthetic.main.activity_forgot_pass.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.etEmail
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap

class ForgotPassActivity : BaseActivity() {

    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null

    var email: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_pass)


        btnSubmit.setOnClickListener {
            hideKeyboard(btnSubmit)
            email = etEmail.text.toString().trim()

            if (email.isNullOrEmpty()) {
                btnSubmit.snackBar(btnSubmit, "Write correct email address!!")
            } else {

                callWS();


            }


        }

        ivBack.setOnClickListener {

            finish()
        }
    }


    private fun callWS() {
        initCallback()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(this, "POST", URLS.FORGOT_PASS, getParamsLogin(), "",true)
    }


    private fun getParamsLogin(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("email_address", email)

            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token); //FirebaseInstanceId.getInstance().getToken());
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }

    private fun initCallback() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) {
                try {
                    val jsonObject = JSONObject(response)
                    val status = jsonObject.getString("response_status")
                    val msg = jsonObject.getString("response_msg")
                    if (status.equals("success", ignoreCase = true)) {

                        val mainIntent =
                            Intent(this@ForgotPassActivity, GeneratePassActivity::class.java)
                        mainIntent.putExtra("EMAIL", email)
                        startActivity(mainIntent)
                    } else {
                        btnSubmit.snackBar(btnSubmit, msg)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                btnSubmit.snackBar(btnSubmit, error.toString())
            }
        }
    }


}
