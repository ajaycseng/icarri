package com.murvan.icarri.activity

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.WindowManager
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.PopupWindow
import androidx.core.app.ActivityCompat
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.murvan.icarri.R
import com.murvan.icarri.models.DropListData
import com.murvan.icarri.models.UserData
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.*
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.activity_add_reviews.*
import kotlinx.android.synthetic.main.activity_add_travel.btnSubmit
import kotlinx.android.synthetic.main.activity_add_travel.etDescription
import kotlinx.android.synthetic.main.activity_add_travel.etTravelDate
import kotlinx.android.synthetic.main.activity_add_travel.etTravelMode
import kotlinx.android.synthetic.main.activity_add_travel_offer.*
import kotlinx.android.synthetic.main.activity_sign_up.ivBack
import kotlinx.android.synthetic.main.activity_sign_up.ivImage
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.net.MalformedURLException
import java.util.*
import kotlin.collections.ArrayList


class AddReviewsActivity : BaseActivity() {

    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null

    var description: String? = null
    var bitmap: Bitmap? = null
    var resultUri: Uri? = null
    var isImgAttached: Boolean = false
    val gson = Gson()
    val postType: String = "Travel Details"
    var paymentID: String = ""
    var imgURL: String = ""
    var userName: String = ""
    var rating: Float = 0.0F

    var userdata: UserData? = null
    var dropListData = ArrayList<DropListData>()

    companion object {

        var isFromHome: Boolean = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_reviews)

        paymentID = intent.extras?.getString("ID").toString()
        userName = intent.extras?.getString("USERNAME").toString()
        imgURL = intent.extras?.getString("IMG_URL").toString()
        tvUserName.text = userName
        userdata =
            gson.fromJson(
                Preference.getInstance(this).getString(Constant.USER_DETAIL),
                UserData::class.java
            )


        if (imgURL != null) {
            Glide.with(this@AddReviewsActivity)
                .load(imgURL)
                .apply(RequestOptions.circleCropTransform())
                .into(iv);
        }
        ivBack.setOnClickListener {
            finish()
        }
        btnSubmit.setOnClickListener {
            validation()
        }


    }


    private fun validation() {

        rating = ratinBar.rating
        description = etDescription.text.toString().trim()

        if (rating == 0.0F) {
            btnSubmit.snackBar(btnSubmit, "Please give your rating to " + userName)
        } else if (description.isNullOrEmpty()) {
            btnSubmit.snackBar(btnSubmit, "Please share your experience….")
        } else {
            callWS();
        }

    }


    private fun callWS() {
        var url: String

        url = URLS.ADD_REVIEWS

        initCallback()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.sendImgRequest(
            this,
            "POST",
            "main_photo",
            bitmap,
            url,
            getParamsLogin(),
            ""
        )
    }


    private fun getParamsLogin(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            // if (!postID.isNullOrEmpty()) {
            // }

            jsonStr.put("user_id", userdata?.user_id)
            jsonStr.put("review_score", rating)
            jsonStr.put("payment_id", paymentID)
            jsonStr.put("review_description", description)
            jsonStr.put("device_type", "Android")
            jsonStr.put(
                "device_id",
                HomeActivity.token
            );//FirebaseInstanceId.getInstance().getToken());
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallback() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) {

                try {
                    val jsonObject = JSONObject(response)
                    val status = jsonObject.getString("response_status")
                    val msg = jsonObject.getString("response_msg")
                    if (status.equals("success", ignoreCase = true)) {

                        toast(msg)

                        if (isFromHome) {
                            isFromHome = false
                            startActivity(
                                Intent(
                                    this@AddReviewsActivity,
                                    MyTravelListActivity::class.java
                                )
                            )

                        }

                        finish()

                    } else {
                        btnSubmit.snackBar(btnSubmit, msg)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                btnSubmit.snackBar(btnSubmit, error.toString())
            }
        }
    }


    private fun pickImage() {
        CropImage.activity()
            .setMinCropWindowSize(200, 400)
            .setFixAspectRatio(true)
            .start(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode === CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode === Activity.RESULT_OK) {
                resultUri = result.uri


                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                isImgAttached = true
                try {


                    Glide.with(this)
                        .load(resultUri)
                        .apply(RequestOptions.fitCenterTransform()).into(ivImage);


                } catch (e: MalformedURLException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                //appCache.getUser()?.showImage(userPic, File(resultUri.path).absolutePath)
            } else if (resultCode === CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
                toast(error.localizedMessage)
            }
        }
    }


    private fun storagePermission() {

        if (ActivityCompat.checkSelfPermission(
                this!!,
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this!!,
                arrayOf(
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), 100
            )
        } else {
            pickImage()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {

        if (requestCode == 100 && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            pickImage()
        } else {
            toast("Permission not granted!!")
        }
    }


    fun popupWindowsort(): PopupWindow {
        var popupWindow = PopupWindow(this);

        var values = ArrayList<String>()

        for (str in dropListData) {
            values.add(str.type.toString())
        }


        var adapter = ArrayAdapter<String>(this, R.layout.dropdown_item, values)
        var listViewSort = ListView(this);
        listViewSort.setAdapter(adapter);
        listViewSort.setOnItemClickListener { parent, view, position, id ->
            etTravelMode.setText(values[position])
            popupWindow.dismiss()
        }
        popupWindow.setFocusable(true);
        popupWindow.setWidth(etTravelMode.getWidth())
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setContentView(listViewSort)

        return popupWindow;
    }


    //

    private fun callWSDrop() {
        initCallbackDrop()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.DROP_LIST_OPTION,
            getParamsDrop(),
            "",true
        )
    }


    private fun getParamsDrop(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("dropdown_type", "travelMode")
            jsonStr.put("device_type", "Android")
            jsonStr.put(
                "device_id",
                HomeActivity.token
            ); //FirebaseInstanceId.getInstance().getToken());
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackDrop() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                if (status.equals("success", ignoreCase = true)) {

                    val responseData = jsonObject.getJSONArray("response_data")

                    for (i in 0 until responseData.length()) {
                        val data: DropListData
                        data =
                            gson.fromJson(responseData.get(i).toString(), DropListData::class.java)
                        dropListData.add(data)
                    }


                } else {
                    btnSubmit.snackBar(btnSubmit, msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                btnSubmit.snackBar(btnSubmit, error.toString())
            }
        }
    }


    //
}

