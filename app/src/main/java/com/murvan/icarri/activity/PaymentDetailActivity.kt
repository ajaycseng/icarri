package com.murvan.icarri.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.murvan.icarri.R
import com.murvan.icarri.models.PaymentData
import com.murvan.icarri.models.UserData
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.ChangeDateFormat
import com.murvan.icarri.utils.Constant
import com.murvan.icarri.utils.Preference
import com.murvan.icarri.utils.toast
import kotlinx.android.synthetic.main.activity_payment_detail.*
import kotlinx.android.synthetic.main.activity_payment_detail.view.*
import kotlinx.android.synthetic.main.activity_travel_detail.ivBack
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.collections.set

class PaymentDetailActivity : AppCompatActivity() {

    val gson = Gson()
    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null
    var paymentID: String? = null;
    var userID: String = ""
    var offerStatus: String = ""
    var postedUserID: String = ""
    var userdata: UserData? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_detail)
        userdata =
            gson.fromJson(
                Preference.getInstance(this).getString(Constant.USER_DETAIL),
                UserData::class.java
            )

        paymentID = intent.getStringExtra("ID")

        ivBack.setOnClickListener {
            finish()
        }


    }

    override fun onResume() {
        super.onResume()
        callWSList()
    }

    private fun callWSList() {
        initCallbackDrop()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.PAYMENT_DETAIL,
            getParamsDrop(),
            "",true
        )
    }


    private fun getParamsDrop(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("user_id", userdata?.user_id)
            jsonStr.put("payment_id", paymentID)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackDrop() {
        resultCallback = object : IResult {
            @SuppressLint("SetTextI18n")
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                if (status.equals("success", ignoreCase = true)) {

                    val responseData = jsonObject.getJSONObject("response_data")

                    val arrayList1: PaymentData
                    arrayList1 =
                        gson.fromJson(
                            responseData.toString(),
                            PaymentData::class.java
                        )
                    Glide.with(this@PaymentDetailActivity)
                        .load(arrayList1.profile_image)
                        .apply(RequestOptions.circleCropTransform())
                        .into(ivPayUser);
                    tvName.text = arrayList1.user_name
                    tvTo.text = arrayList1.travel_destination
                    tvStatusText.text = "Status: " + arrayList1.delivery_current_status
                    tvFrom.text = arrayList1.depart_from

                    if (arrayList1.sender_received_type.equals("Received")) {
                        tvTransactionID.text = arrayList1.transaction_id

                        tvOfferAmountTitle.text =
                            "Total Paid Amount"
                        tvOfferAmount.text =
                            arrayList1.total_paid_amount

                        tvTaxTitle.text =
                            "Commission: " + "(" + arrayList1.commission_percent + "%)"
                        tvTax.text =
                            "Amount: " + arrayList1.commission_amount

                        tvTotalPaidTitle.text = "Final Earning"
                        tvTotalPaid.text = arrayList1.final_earnings

                        tvCreated.text = ChangeDateFormat.getDateMonthDate(arrayList1.created)

                    } else {
                        tvTransactionID.text = arrayList1.transaction_id
                        tvTotalPaid.text = arrayList1.total_paid_amount
                        tvCreated.text = ChangeDateFormat.getDateMonthDate(arrayList1.created)
                        tvOfferAmount.text =
                            arrayList1.offer_amount

                        tvTaxTitle.text =
                            "Commission: " + "(" + arrayList1.commission_percent + "%)"
                        tvTax.text =
                            "Amount: " + arrayList1.commission_amount

                       /* tvTax.text =
                            "(" + arrayList1.tax_percent + "%) " + arrayList1.tax_amount*/
                    }


                    if (arrayList1.payment_transfer_status != null && arrayList1.payment_transfer_status.equals(
                            "Completed"
                        )
                    ) {
                        cvTransfer.visibility = View.VISIBLE
                        tvTransferID.text = arrayList1.payment_transfer_transaction_id
                        tvTransferStatus.text = arrayList1.payment_transfer_status
                        tvTransferDate.text =
                            ChangeDateFormat.getDateMonthDate(arrayList1.transfer_date)
                        tvComment.text = arrayList1.transfer_comments
                    } else {
                        cvTransfer.visibility = View.GONE
                    }


                    //    userID = arrayList1.user_id.toString()


                } else {
                    toast(msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                toast(error.toString())
            }
        }
    }

}
