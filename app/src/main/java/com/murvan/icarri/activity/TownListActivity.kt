package com.murvan.icarri.activity

import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.murvan.icarri.R
import com.murvan.icarri.models.InboxData
import com.murvan.icarri.models.TownData
import com.murvan.icarri.models.UserData
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.ChangeDateFormat
import com.murvan.icarri.utils.Constant
import com.murvan.icarri.utils.Preference
import com.murvan.icarri.utils.toast
import kotlinx.android.synthetic.main.activity_my_travel_list.*
import kotlinx.android.synthetic.main.activity_my_travel_list.ivAdd
import kotlinx.android.synthetic.main.activity_my_travel_list.ivBacks
import kotlinx.android.synthetic.main.activity_my_travel_list.tvNoDatas
import kotlinx.android.synthetic.main.activity_my_travel_list.tvTitles
import kotlinx.android.synthetic.main.activity_town_list.*
import org.json.JSONException
import org.json.JSONObject
import java.lang.Exception
import java.util.ArrayList
import java.util.HashMap

class TownListActivity : AppCompatActivity() {

    var recycleView: RecyclerView? = null
    var arrayList = ArrayList<TownData>()
    internal lateinit var mAdapter: AdapterClass
    val gson = Gson()
    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null
    var status: String = "All"
    var searchKeyword: String = ""
    var selectedPosition: Int = -1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_town_list)
        tvTitles.setText("Select Town")
        tvNoDatas.setText(resources.getString(R.string.norecord))
        tvNoDatas.setOnClickListener {
            callWSList()
        }

        ivBacks.setOnClickListener {

            finish()
        }

        ivAdd.setOnClickListener {
            startActivity(Intent(this@TownListActivity, AddPkgActivity::class.java))
        }

        recycleView = findViewById(R.id.recycleView)
        val layoutManager = LinearLayoutManager(this)
        val layoutManagerPost = LinearLayoutManager(this)
        recycleView?.layoutManager = layoutManager
        recycleView?.itemAnimator = DefaultItemAnimator()
        mAdapter = AdapterClass(arrayList)
        recycleView?.adapter = mAdapter
        mAdapter.notifyDataSetChanged()
        callWSList()
        ivSend.setOnClickListener(View.OnClickListener {

            searchKeyword = etKeyword.text.toString().trim()
            callWSList()
        })

    }


    override fun onResume() {
        super.onResume()
    }
    //

    private fun callWSList() {
        initCallbackDrop()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.TOWN_LIST,
            getParamsDrop(),
            "", true
        )
    }


    private fun getParamsDrop(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("town_string", searchKeyword)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackDrop() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                if (status.equals("success", ignoreCase = true)) {

                    val msgList = jsonObject.getJSONArray("response_data")
                    if (msgList.length() == 0) {
                        tvNoDatas.visibility = View.VISIBLE
                        recycleView?.visibility = View.GONE
                    } else {
                        tvNoDatas.visibility = View.GONE
                        recycleView?.visibility = View.VISIBLE

                        arrayList.clear()
                        for (i in 0 until msgList.length()) {
                            val data: TownData
                            data =
                                gson.fromJson(
                                    msgList.get(i).toString(),
                                    TownData::class.java
                                )
                            arrayList.add(data)
                        }

                        mAdapter.notifyDataSetChanged()

                    }


                } else {
                    toast(msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) {
                toast(error.toString())
            }
        }
    }


    //


    internal inner class AdapterClass(val arrayList1: ArrayList<TownData>) :
        RecyclerView.Adapter<AdapterClass.MyViewHolder>() {
        override fun getItemCount(): Int {

            return arrayList1.size
        }


        internal inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tvName: TextView
            var ll: LinearLayout

            init {
                tvName = view.findViewById(R.id.tvName) as TextView
                ll = view.findViewById(R.id.ll) as LinearLayout
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_town_list, parent, false)
            return MyViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


            holder.tvName.setTypeface(Typeface.DEFAULT_BOLD)

            holder.tvName.text = arrayList1[position].name



            holder.ll.setOnClickListener {
                /*   val mainIntent = Intent(
                       this@TownListActivity,
                       MsgListActivity::class.java
                   ).putExtra("DATA", arrayList1[position])
                   startActivity(mainIntent)*/
                val resultIntent = Intent()
                resultIntent.putExtra("TOWN", arrayList1[position].name)

                setResult(Activity.RESULT_OK, resultIntent)
                finish()
            }

        }
    }


}
