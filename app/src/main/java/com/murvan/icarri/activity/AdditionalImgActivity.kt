package com.murvan.icarri.activity

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.net.toFile
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.murvan.icarri.R
import com.murvan.icarri.models.AdditionalImgData
import com.murvan.icarri.models.UserData
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.Constant
import com.murvan.icarri.utils.ImageUtils
import com.murvan.icarri.utils.Preference
import com.murvan.icarri.utils.toast
import com.theartofdev.edmodo.cropper.CropImage
import id.zelory.compressor.Compressor
import kotlinx.android.synthetic.main.activity_my_travel_list.*
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.net.MalformedURLException
import java.util.*

class AdditionalImgActivity : AppCompatActivity() {

    var recycleView: RecyclerView? = null
    var arrayList = ArrayList<AdditionalImgData>()
    internal lateinit var mAdapter: AdapterClass
    val gson = Gson()
    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null
    var userdata: UserData? = null
    var postType: String = "Travel Details"
    var status: String = "All"
    var selectedPosition: Int = -1
    var postID: String = ""

    var bitmap: Bitmap? = null
    var resultUri: Uri? = null
    var isImgAttached: Boolean = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_travel_list)

        tvTitles.setText("Additional Images")
        postID = intent.getStringExtra("ID")



        tvTitles.setText(intent.getStringExtra("TITLES"))

        tvNoDatas.setOnClickListener {
            callWSList()
        }

        ivBacks.setOnClickListener {

            finish()
        }

        ivAdd.setOnClickListener {

            pickImage()
        }

        userdata =
            gson.fromJson(
                Preference.getInstance(this).getString(Constant.USER_DETAIL),
                UserData::class.java
            )


        recycleView = findViewById(R.id.recycleView)

        recycleView?.layoutManager = GridLayoutManager(this, 2)

        recycleView?.itemAnimator = DefaultItemAnimator()
        callWSList();
    }


    override fun onResume() {
        super.onResume()

    }
    //

    private fun callWSList() {
        initCallImgList()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.IMAGE_LIST,
            getParamsList(),
            "",true
        )
    }


    private fun getParamsList(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("post_id", postID)
            jsonStr.put("user_id", userdata?.user_id)
            jsonStr.put("status", status)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallImgList() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                if (status.equals("success", ignoreCase = true)) {

                    val responseData = jsonObject.getJSONArray("response_data")
                    if (responseData.length() == 0) {
                        tvNoDatas.visibility = View.VISIBLE
                        tvNoDatas.setText("There is no additional images found for this record.\nPlease upload image.")
                        recycleView?.visibility = View.GONE
                    } else {
                        tvNoDatas.visibility = View.GONE
                        recycleView?.visibility = View.VISIBLE

                        arrayList.clear()
                        for (i in 0 until responseData.length()) {
                            val data: AdditionalImgData
                            data =
                                gson.fromJson(
                                    responseData.get(i).toString(),
                                    AdditionalImgData::class.java
                                )
                            arrayList.add(data)
                        }

                        mAdapter = AdapterClass(arrayList)
                        recycleView?.adapter = mAdapter
                        mAdapter.notifyDataSetChanged()

                    }


                } else {
                    tvNoDatas.visibility = View.VISIBLE
                    tvNoDatas.setText("There is no additional images found for this record.\nPlease upload image.")
                    recycleView?.visibility = View.GONE
                    toast(msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) {
                toast(error.toString())
            }
        }
    }


    //


    internal inner class AdapterClass(val arrayList1: ArrayList<AdditionalImgData>) :
        RecyclerView.Adapter<AdapterClass.MyViewHolder>() {
        override fun getItemCount(): Int {

            return arrayList1.size
        }


        internal inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tvDelete: TextView

            var ivImg: ImageView

            var ll: RelativeLayout

            init {
                tvDelete = view.findViewById(R.id.tvDelete) as TextView
                ivImg = view.findViewById(R.id.ivImg) as ImageView

                ll = view.findViewById(R.id.ll) as RelativeLayout
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_additional_img, parent, false)

            return MyViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


            holder.tvDelete.setOnClickListener {

                deleteDialog(position)

            }

            holder.ll.setOnClickListener {

                startActivity(
                    Intent(
                        this@AdditionalImgActivity,
                        ImageViewActivity::class.java
                    ).putExtra("URL", arrayList1.get(position).mediafile_file)
                )

            }

            Glide.with(this@AdditionalImgActivity)
                .load(arrayList1[position].mediafile_file)
                .apply(RequestOptions.fitCenterTransform())
                .into(holder.ivImg);

        }
    }


    fun deleteDialog(position: Int) {
        val builder = AlertDialog.Builder(this)
        //set message for alert dialog
        builder.setMessage(R.string.confirm_delete)
        builder.setIcon(android.R.drawable.ic_dialog_alert)

        //performing positive action
        builder.setPositiveButton("Yes") { dialogInterface, which ->
            selectedPosition = position
            callWSDelete()

        }
        builder.setNegativeButton("No") { dialogInterface, which ->

        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }


    private fun callWSDelete() {
        initCallbackDelete()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.IMAGE_DELETE,
            getParamsDelete(),
            "",true
        )
    }

    private fun getParamsDelete(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("mediafile_id", arrayList.get(selectedPosition).id)
            jsonStr.put("post_id", postID)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackDelete() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                if (status.equals("success", ignoreCase = true)) {
                    toast(msg)
                    callWSList()
                } else {
                    toast(msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                toast(error.toString())
            }
        }
    }


    //


    private fun callAddImg() {
        initCallbackAddImg()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.sendImgRequest(
            this,
            "POST",
            "mediafile_file",
            bitmap,
            URLS.IMAGE_ADD,
            getParamsAddImg(),
            ""
        )
    }


    private fun getParamsAddImg(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("post_id", postID)

            jsonStr.put("post_type", postType)
            jsonStr.put("user_id", userdata?.user_id)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token); //FirebaseInstanceId.getInstance().getToken());
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackAddImg() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) {

                try {
                    val jsonObject = JSONObject(response)
                    val status = jsonObject.getString("response_status")
                    val msg = jsonObject.getString("response_msg")
                    if (status.equals("success", ignoreCase = true)) {

                        toast(msg)
                        callWSList()

                    } else {
                        toast(msg)

                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                toast(error.toString())
            }
        }
    }


    private fun pickImage() {
        CropImage.activity()
            .setMinCropWindowSize(200, 200)
            .setFixAspectRatio(true)
            .start(this)
    }

    private fun compressBitmap(bitmap: Bitmap, quality: Int): Bitmap {
        // Initialize a new ByteArrayStream
        val stream = ByteArrayOutputStream()


        // Compress the bitmap with JPEG format and quality 50%
        bitmap.compress(Bitmap.CompressFormat.JPEG, quality, stream)

        val byteArray = stream.toByteArray()

        // Finally, return the compressed bitmap
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode === CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode === Activity.RESULT_OK) {

                resultUri = result.uri
                //var file = Uri.fromFile(File(resultUri.toString()))
                // bitmap = Compressor(this).compressToBitmap(data.)
                // bitmap = ImageUtils.getInstant().getCompressedBitmap(resultUri.toString());
                bitmap = (MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri))
                // bitmap= Compressor(this).compressToBitmap(MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri));


                isImgAttached = true
                try {
                    /* Glide.with(this)
                         .load(resultUri)
                         .apply(RequestOptions.fitCenterTransform()).into(ivImage);
 */
                    callAddImg()

                } catch (e: MalformedURLException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                //appCache.getUser()?.showImage(userPic, File(resultUri.path).absolutePath)
            } else if (resultCode === CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
                toast(error.localizedMessage)
            }
        }
    }


    private fun storagePermission() {

        if (ActivityCompat.checkSelfPermission(
                this!!,
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this!!,
                arrayOf(
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), 100
            )
        } else {
            pickImage()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {

        if (requestCode == 100 && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            pickImage()
        } else {
            toast("Permission not granted!!")
        }
    }


}
