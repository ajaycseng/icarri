package com.murvan.icarri.activity

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.WindowManager
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.PopupWindow
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.app.ActivityCompat
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.murvan.icarri.R
import com.murvan.icarri.models.DropListData
import com.murvan.icarri.models.TravelPkgData
import com.murvan.icarri.models.UserData
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.*
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.activity_add_travel.*
import kotlinx.android.synthetic.main.activity_sign_up.ivBack
import kotlinx.android.synthetic.main.activity_sign_up.ivImage
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.net.MalformedURLException
import java.util.*
import kotlin.collections.ArrayList


class AddTravelActivity : BaseActivity() {

    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null

    var departFrom: String? = null
    var destination: String? = null
    var travelMode: String? = null
    var description: String? = null
    var travelDate: String? = null
    var travelTime: String? = null
    var bitmap: Bitmap? = null
    var resultUri: Uri? = null
    var isImgAttached: Boolean = false
    val gson = Gson()
    val postType: String = "Travel Details"
    var postID: String = ""

    var userdata: UserData? = null
    var dropListData = ArrayList<DropListData>()

    companion object {

        var isFromHome: Boolean = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_travel)
        userdata =
            gson.fromJson(
                Preference.getInstance(this).getString(Constant.USER_DETAIL),
                UserData::class.java
            )
        callWSDrop()

        ivBack.setOnClickListener {
            finish()
        }

        etTravelDate.setOnClickListener {
            getDate(etTravelDate)
        }
        etTravelMode.setOnClickListener {
            val popUp = popupWindowsort();
            popUp.showAsDropDown(etTravelMode, 0, 0);
        }

        etTravelTime.setOnClickListener {

            clickTimePicker()

        }
        etDepartFrom.setOnClickListener {
            startActivityForResult(Intent(this, TownListActivity::class.java), 1)
        }
        etDestination.setOnClickListener {
            startActivityForResult(Intent(this, TownListActivity::class.java), 2)
        }
        btnSubmit.setOnClickListener {

            validation()
        }
        btnChooseImg.setOnClickListener {
            storagePermission()
        }


        try {


            if (intent.hasExtra("DATA")) {
                var travelPkgData = TravelPkgData()
                travelPkgData = intent.getSerializableExtra("DATA") as TravelPkgData
                setData(travelPkgData)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    private fun setData(travelPkgData: TravelPkgData) {


        tvtitle.setText("Edit Detail")
        postID = travelPkgData.id.toString()
        etDepartFrom.setText(travelPkgData.depart_from)
        etDestination.setText(travelPkgData.travel_destination)
        etDescription.setText(travelPkgData.post_description)
        etTravelDate.setText(travelPkgData.travel_date)
        travelTime = travelPkgData.travel_time
        etTravelTime.setText(ChangeDateFormat.getTimeFromDateTime(travelPkgData.travel_time))
        etTravelMode.setText(travelPkgData.travel_mode)

        if (!travelPkgData.main_photo.isNullOrEmpty()) {

            isImgAttached = true

            Glide.with(this@AddTravelActivity)
                .load(travelPkgData.main_photo)
                .apply(RequestOptions.fitCenterTransform())
                .into(ivImage);

        }

    }

    fun clickTimePicker() {
        val c = Calendar.getInstance()
        val hour = c.get(Calendar.HOUR)
        val minute = c.get(Calendar.MINUTE)
        val sec = c.get(Calendar.SECOND)


        val tpd =
            TimePickerDialog(this, TimePickerDialog.OnTimeSetListener(function = { view, h, m ->


                travelTime = "$h:$m"
                etTravelTime.setText(ChangeDateFormat.getTimeForamteHHMMA("$h:$m"))


            }), hour, minute, false)


        //datePicker.minDate = System.currentTimeMillis()
        tpd.show()
    }

/*
    fun showTime(h: Int, min: Int) {
        var format: String
        var hour: Int = h
        if (hour == 0) {
            hour += 12;
            format = "AM";
        } else if (hour == 12) {
            format = "PM";
        } else if (hour > 12) {
            hour -= 12;
            format = "PM";
        } else {
            format = "AM";
        }

        etTravelTime.setText(
            StringBuilder().append(hour).append(":").append(min)
                .append(" ").append(format)
        );
    }
*/

    private fun getDate(etTravelDate: AppCompatEditText?) {

        val c = Calendar.getInstance()

        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        val dpd = DatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->


                var month = monthOfYear + 1
                var date = ChangeDateFormat.getDateYYYYMMDD("$year-$month-$dayOfMonth")

                etTravelDate?.setText(date)
            },
            year,
            month,
            day
        )
        dpd.datePicker.minDate = System.currentTimeMillis()

        dpd.show()
    }

    private fun validation() {

        departFrom = etDepartFrom.text.toString().trim()
        destination = etDestination.text.toString().trim()
        description = etDescription.text.toString().trim()
        travelDate = etTravelDate.text.toString().trim()
        // travelTime = etTravelTime.text.toString().trim()
        travelMode = etTravelMode.text.toString().trim()

        if (departFrom.isNullOrEmpty()) {
            btnSubmit.snackBar(btnSubmit, "Depart From!!")
        } else if (destination.isNullOrEmpty()) {
            btnSubmit.snackBar(btnSubmit, "Destination!!")
        } else if (destination.equals(departFrom)) {
            btnSubmit.snackBar(btnSubmit, "Depart and Destination should not be same!!")
        } else if (description.isNullOrEmpty()) {
            btnSubmit.snackBar(btnSubmit, "Description!!")
        } else if (travelDate.isNullOrEmpty()) {
            btnSubmit.snackBar(btnSubmit, "Travel Date!!")
        } else if (travelTime.isNullOrEmpty()) {
            btnSubmit.snackBar(btnSubmit, "Travel Time!!")
        } else if (travelMode.isNullOrEmpty()) {
            btnSubmit.snackBar(btnSubmit, "Travel Mode!!")
        } else if (!isImgAttached) {
            btnSubmit.snackBar(btnSubmit, "Click on user to attach image!!")
        } else {
            callWS();
        }

    }


    private fun callWS() {
        var url: String
        if (!postID.isNullOrEmpty()) {
            url = URLS.EDIT_TRAVELER
        } else {
            url = URLS.ADD_TRAVELER
        }
        initCallback()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.sendImgRequest(
            this,
            "POST",
            "main_photo",
            bitmap,
            url,
            getParamsLogin(),
            ""
        )
    }


    private fun getParamsLogin(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            if (!postID.isNullOrEmpty()) {
                jsonStr.put("post_id", postID)
            }

            jsonStr.put("post_type", postType)
            jsonStr.put("user_id", userdata?.user_id)
            jsonStr.put("post_description", description)
            jsonStr.put("depart_from", departFrom)
            jsonStr.put("travel_destination", destination)
            jsonStr.put("travel_mode", travelMode)
            jsonStr.put("travel_date", travelDate)
            jsonStr.put("travel_time", ChangeDateFormat.getNewTimeTwlveHrFormat(travelTime))
            jsonStr.put("device_type", "Android")
            jsonStr.put(
                "device_id",
                HomeActivity.token
            );//FirebaseInstanceId.getInstance().getToken());
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallback() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) {

                try {
                    val jsonObject = JSONObject(response)
                    val status = jsonObject.getString("response_status")
                    val msg = jsonObject.getString("response_msg")
                    if (status.equals("success", ignoreCase = true)) {

                        toast(msg)

                        if (isFromHome) {
                            isFromHome = false
                            startActivity(
                                Intent(
                                    this@AddTravelActivity,
                                    MyTravelListActivity::class.java
                                )
                            )

                        }

                        finish()

                    } else {
                        btnSubmit.snackBar(btnSubmit, msg)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                btnSubmit.snackBar(btnSubmit, error.toString())
            }
        }
    }


    private fun pickImage() {
        CropImage.activity()
            .setMinCropWindowSize(200, 400)
            .setFixAspectRatio(true)
            .start(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                resultUri = result.uri


                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                isImgAttached = true
                try {


                    Glide.with(this)
                        .load(resultUri)
                        .apply(RequestOptions.fitCenterTransform()).into(ivImage);


                } catch (e: MalformedURLException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                //appCache.getUser()?.showImage(userPic, File(resultUri.path).absolutePath)
            } else if (resultCode === CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
                toast(error.localizedMessage)
            }
        }
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 1) {
                val value = data?.getStringExtra("TOWN")
                etDepartFrom.setText(value)
            }
            if (requestCode == 2) {
                val value = data?.getStringExtra("TOWN")
                etDestination.setText(value)
            }
            return
        }
    }


    private fun storagePermission() {

        if (ActivityCompat.checkSelfPermission(
                this!!,
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this!!,
                arrayOf(
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), 100
            )
        } else {
            pickImage()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {

        if (requestCode == 100 && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            pickImage()
        } else {
            toast("Permission not granted!!")
        }
    }


    fun popupWindowsort(): PopupWindow {
        var popupWindow = PopupWindow(this);

        var values = ArrayList<String>()

        for (str in dropListData) {
            values.add(str.type.toString())
        }


        var adapter = ArrayAdapter<String>(this, R.layout.dropdown_item, values)
        var listViewSort = ListView(this);
        listViewSort.setAdapter(adapter);
        listViewSort.setOnItemClickListener { parent, view, position, id ->
            etTravelMode.setText(values[position])
            popupWindow.dismiss()
        }
        popupWindow.setFocusable(true);
        popupWindow.setWidth(etTravelMode.getWidth())
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setContentView(listViewSort)

        return popupWindow;
    }


//

    private fun callWSDrop() {
        initCallbackDrop()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.DROP_LIST_OPTION,
            getParamsDrop(),
            "", true
        )
    }


    private fun getParamsDrop(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("dropdown_type", "travelMode")
            jsonStr.put("device_type", "Android")
            jsonStr.put(
                "device_id",
                HomeActivity.token
            ); //FirebaseInstanceId.getInstance().getToken());
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackDrop() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                if (status.equals("success", ignoreCase = true)) {

                    val responseData = jsonObject.getJSONArray("response_data")

                    for (i in 0 until responseData.length()) {
                        val data: DropListData
                        data =
                            gson.fromJson(responseData.get(i).toString(), DropListData::class.java)
                        dropListData.add(data)
                    }


                } else {
                    btnSubmit.snackBar(btnSubmit, msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                btnSubmit.snackBar(btnSubmit, error.toString())
            }
        }
    }


//
}

