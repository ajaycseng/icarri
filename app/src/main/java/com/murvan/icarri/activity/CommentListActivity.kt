package com.murvan.icarri.activity

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.murvan.icarri.R
import com.murvan.icarri.models.CommentData
import com.murvan.icarri.models.UserData
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.Constant
import com.murvan.icarri.utils.Preference
import com.murvan.icarri.utils.toast
import kotlinx.android.synthetic.main.activity_comment_list.*
import kotlinx.android.synthetic.main.activity_my_travel_list.ivAdd
import kotlinx.android.synthetic.main.activity_my_travel_list.ivBacks
import kotlinx.android.synthetic.main.activity_my_travel_list.tvNoDatas
import org.apache.commons.lang3.StringEscapeUtils
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class CommentListActivity : AppCompatActivity() {

    var recycleView: RecyclerView? = null
    var arrayList = ArrayList<CommentData>()
    internal lateinit var mAdapter: AdapterClass
    val gson = Gson()
    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null
    var userdata: UserData? = null
    var postID: String = ""

    var comment: String = ""
    var status: String = "All"

    //
    var pastVisiblesItems = 0
    var visibleItemCount = 0
    var totalItemCount = 0
    private var loading = true
    var pageNumber: Int = 1
    var totalRecords: Int = 0
    var totalPages: Int = 0
    //

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comment_list)



        postID = intent.extras?.getString("ID").toString()

        tvNoDatas.setOnClickListener {
            callWSList()
        }

        ivSend.setOnClickListener {
            comment = etComment.text.toString().trim()
            if (!comment.isNullOrEmpty()) {
                callWSComment()

            }
        }

        ivBacks.setOnClickListener {
            finish()
        }

        ivAdd.setOnClickListener {
            startActivity(Intent(this@CommentListActivity, AddTravelActivity::class.java))
        }

        userdata =
            gson.fromJson(
                Preference.getInstance(this).getString(Constant.USER_DETAIL),
                UserData::class.java
            )

        recycleView = findViewById(R.id.recycleView)

        var layoutManager = LinearLayoutManager(this)
        val layoutManagerPost = LinearLayoutManager(this)
        recycleView?.layoutManager = layoutManager
        recycleView?.itemAnimator = DefaultItemAnimator()
        mAdapter = AdapterClass(arrayList)

        recycleView?.adapter = mAdapter
        mAdapter.notifyDataSetChanged()
        recycleView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(
                recyclerView: RecyclerView,
                dx: Int,
                dy: Int
            ) {

                var visibleItemCount = layoutManager.getChildCount();
                var totalItemCount = layoutManager.getItemCount();
                var firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (totalPages > pageNumber) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                    ) {
                        pageNumber++
                        callWSList()
                    }
                }

            }
        })
    }


    override fun onResume() {
        super.onResume()
        if (pageNumber == 1) {
            arrayList.clear()
            callWSList();
        }
    }
    //

    private fun callWSList() {
        initCallbackDrop()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.COMMENT_LIST,
            getParamsDrop(),
            "", true
        )
    }


    private fun getParamsDrop(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("post_id", postID)
            jsonStr.put("page_number", pageNumber)

            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackDrop() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                if (status.equals("success", ignoreCase = true)) {
                    totalRecords = jsonObject.getInt("total_records")
                    totalPages = jsonObject.getInt("total_pages")

                    val responseData = jsonObject.getJSONObject("response_data")

                    val comments_list = responseData.getJSONArray("comments_list")

                    tvSourceDestination.setText(
                        responseData.getString("depart_from")
                                + " : " + responseData.getString("travel_destination")
                    )

                    if (comments_list.length() == 0) {
                        tvNoDatas.visibility = View.VISIBLE
                        recycleView?.visibility = View.GONE
                    } else {
                        tvNoDatas.visibility = View.GONE
                        recycleView?.visibility = View.VISIBLE

                        // arrayList.clear()
                        for (i in 0 until comments_list.length()) {
                            val data: CommentData
                            data =
                                gson.fromJson(
                                    comments_list.get(i).toString(),
                                    CommentData::class.java
                                )
                            arrayList.add(data)
                        }

                        // arrayList.reverse()

                        //    mAdapter = AdapterClass(arrayList)
                        //  recycleView?.adapter = mAdapter
                        mAdapter.notifyDataSetChanged()

                    }


                } else {
                    toast(msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                toast(error.toString())
            }
        }
    }


    //


    internal inner class AdapterClass(val arrayList1: ArrayList<CommentData>) :
        RecyclerView.Adapter<AdapterClass.MyViewHolder>() {
        override fun getItemCount(): Int {

            return arrayList1.size
        }

        internal inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tvSenderName: TextView
            var tvSenderDate: TextView
            var tvSenderComment: TextView
            var tvReceiverName: TextView
            var tvReceiverDate: TextView
            var tvReceiverComment: TextView
            var ivSender: ImageView
            var ivReceiver: ImageView
            var llReceiver: LinearLayout
            var llSender: LinearLayout

            init {
                tvSenderName = view.findViewById(R.id.tvSenderName) as TextView
                tvSenderDate = view.findViewById(R.id.tvSenderDate) as TextView
                tvSenderComment = view.findViewById(R.id.tvSenderComment) as TextView
                tvReceiverName = view.findViewById(R.id.tvReceiverName) as TextView
                tvReceiverDate = view.findViewById(R.id.tvReceiverDate) as TextView
                tvReceiverComment = view.findViewById(R.id.tvReceiverComment) as TextView
                ivSender = view.findViewById(R.id.ivSender) as ImageView
                ivReceiver = view.findViewById(R.id.ivReceiver) as ImageView
                llReceiver = view.findViewById(R.id.llReceiver) as LinearLayout
                llSender = view.findViewById(R.id.llSender) as LinearLayout

            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_comment_list, parent, false)

            return MyViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


            if (arrayList1[position].user_id.equals(userdata?.user_id)) {
                holder.llReceiver.visibility = View.VISIBLE
                holder.llSender.visibility = View.GONE

                holder.tvReceiverName.text = arrayList1[position].user_name
                holder.tvReceiverDate.text =
                    arrayList1[position].time_ago
                holder.tvReceiverComment.text =
                    StringEscapeUtils.unescapeJava(arrayList1[position].comment_text)

                Glide.with(this@CommentListActivity)
                    .load(arrayList1[position].profile_image)
                    .apply(RequestOptions.fitCenterTransform())
                    .into(holder.ivReceiver);

                holder.ivReceiver.setOnClickListener {

                    val mainIntent = Intent(
                        this@CommentListActivity,
                        UserProfileActivity::class.java
                    ).putExtra("ID", arrayList1[position].user_id)


                    startActivity(mainIntent)
                }
            } else {
                holder.llReceiver.visibility = View.GONE
                holder.llSender.visibility = View.VISIBLE


                holder.tvSenderName.text = arrayList1[position].user_name
                holder.tvSenderDate.text = arrayList1[position].time_ago
                holder.tvSenderComment.text =
                    StringEscapeUtils.unescapeJava(arrayList1[position].comment_text)

                Glide.with(this@CommentListActivity)
                    .load(arrayList1[position].profile_image)
                    .apply(RequestOptions.fitCenterTransform())
                    .into(holder.ivSender);
                holder.ivSender.setOnClickListener {

                    val mainIntent = Intent(
                        this@CommentListActivity,
                        UserProfileActivity::class.java
                    ).putExtra("ID", arrayList1[position].user_id)

                    startActivity(mainIntent)
                }


            }


        }
    }


    private fun callWSComment() {
        initCallbackComment()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.ADD_COMMENT,
            getParamsSendComment(),
            "", true
        )
    }


    private fun getParamsSendComment(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("comment_text", (StringEscapeUtils.escapeJava(comment)))
            jsonStr.put("post_id", postID)
            jsonStr.put("user_id", userdata?.user_id)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackComment() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                if (status.equals("success", ignoreCase = true)) {
                    etComment.setText("")
                    toast(msg)
                    callWSList()

                } else {
                    toast(msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                toast(error.toString())
            }
        }
    }


}
