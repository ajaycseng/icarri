package com.murvan.icarri.activity

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.murvan.icarri.R
import com.murvan.icarri.models.TravelPkgData
import com.murvan.icarri.models.UserData
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.URLS.PAYPAL_CLIENT_ID
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.*
import com.paypal.android.sdk.payments.*
import kotlinx.android.synthetic.main.activity_forgot_pass.*
import kotlinx.android.synthetic.main.activity_my_travel_list.ivBacks
import kotlinx.android.synthetic.main.activity_my_travel_list.tvNoDatas
import kotlinx.android.synthetic.main.activity_offer_list.*
import kotlinx.android.synthetic.main.row_offer_list.*
import org.json.JSONException
import org.json.JSONObject
import java.lang.Exception
import java.math.BigDecimal
import java.util.ArrayList
import java.util.HashMap
import kotlin.text.Typography.times

class OfferListActivity : AppCompatActivity() {
    var recycleView: RecyclerView? = null
    var arrayList = ArrayList<TravelPkgData>()
    internal lateinit var mAdapter: AdapterClass
    val gson = Gson()
    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null
    var userdata: UserData? = null
    var optionType: String = ""
    var offerID: String = ""
    var transactionID: String = ""
    var paymentJson: String = ""
    var selectedPosition: Int = -1
    var finalPrice: Double = 0.0
    var perAmount: Double = 0.0
    var amount: Double = 0.0

    //
    var pastVisiblesItems = 0
    var visibleItemCount = 0
    var totalItemCount = 0
    private var loading = true

    var recordsPerPage: Int = 10
    var pageNumber: Int = 1
    var totalRecords: Int = 0
    var totalPages: Int = 0
    val PAYMENT_STATUS_CODE: Int=9


    //
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_offer_list)
        isPaymentProcess = false
        tvTitles.setText("Travel Offers")
        paypalService()
        tvNoDatas.setText(resources.getString(R.string.nooffer))

        tvNoDatas.setOnClickListener {
            callWSList()
        }

        ivBacks.setOnClickListener {

            finish()
        }
        ivType.setOnClickListener {
            showPopupMenu(ivType)

        }

        userdata =
            gson.fromJson(
                Preference.getInstance(this).getString(Constant.USER_DETAIL),
                UserData::class.java
            )


        recycleView = findViewById(R.id.recycleView)

        var layoutManager = LinearLayoutManager(this)
        val layoutManagerPost = LinearLayoutManager(this)
        recycleView?.layoutManager = layoutManager
        recycleView?.itemAnimator = DefaultItemAnimator()

        mAdapter = AdapterClass(arrayList)
        recycleView?.adapter = mAdapter
        mAdapter.notifyDataSetChanged()
        recycleView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(
                recyclerView: RecyclerView,
                dx: Int,
                dy: Int
            ) {

                var visibleItemCount = layoutManager.getChildCount();
                var totalItemCount = layoutManager.getItemCount();
                var firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (totalPages > pageNumber) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                    ) {
                        pageNumber++
                        callWSList()
                    }
                }

            }
        })


        if (offerType.equals("Received")) {
            btnGiven.setBackgroundDrawable(resources.getDrawable(R.drawable.corner_greybtn))
            btnGiven.setTextColor(resources.getColor(R.color.black))
            btnReceive.setBackgroundDrawable(resources.getDrawable(R.drawable.corner_orange))
            btnReceive.setTextColor(resources.getColor(R.color.white))

        } else {
            btnReceive.setBackgroundDrawable(resources.getDrawable(R.drawable.corner_greybtn))
            btnReceive.setTextColor(resources.getColor(R.color.black))
            btnGiven.setBackgroundDrawable(resources.getDrawable(R.drawable.corner_orange))
            btnGiven.setTextColor(resources.getColor(R.color.white))
        }

        if (postType.equals("Travel Details")) {
            tvTitles.setText("Travel Offers")
            postType = "Travel Details"
        } else {
            tvTitles.setText("Package Offers");
            postType = "Package Details"
        }

        btnReceive.setOnClickListener(View.OnClickListener {
            btnGiven.setBackgroundDrawable(resources.getDrawable(R.drawable.corner_greybtn))
            btnGiven.setTextColor(resources.getColor(R.color.black))
            btnReceive.setBackgroundDrawable(resources.getDrawable(R.drawable.corner_orange))
            btnReceive.setTextColor(resources.getColor(R.color.white))
            offerType = "Received"
            pageNumber = 1
            arrayList.clear()
            callWSList();

        })

        btnGiven.setOnClickListener(View.OnClickListener {
            btnReceive.setBackgroundDrawable(resources.getDrawable(R.drawable.corner_greybtn))
            btnReceive.setTextColor(resources.getColor(R.color.black))
            btnGiven.setBackgroundDrawable(resources.getDrawable(R.drawable.corner_orange))
            btnGiven.setTextColor(resources.getColor(R.color.white))
            offerType = "Given"
            pageNumber = 1
            arrayList.clear()
            callWSList();
        })
    }


    fun showPopupMenu(view: View) {

        var popup = PopupMenu(this, view);
        popup.getMenuInflater().inflate(R.menu.type_menu, popup.getMenu())
        popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { it ->
            val item = it!!.itemId
            when (item) {
                R.id.menuTravel -> {
                    tvTitles.setText("Travel Offers")
                    postType = "Travel Details"
                    pageNumber = 1
                    arrayList.clear()
                    callWSList()

                }
                R.id.menuPkg -> {
                    tvTitles.setText("Package Offers");
                    postType = "Package Details"
                    pageNumber = 1
                    arrayList.clear()
                    callWSList()

                }

            }
            true

        })
        popup.show();
    }


    override fun onResume() {
        super.onResume()


        if (isPaymentProcess) {
            arrayList.clear()
            callWSList();
            return;
        }
        if (pageNumber == 1) {
            arrayList.clear()
            callWSList();
        }
    }
    //

    private fun callWSList() {
        initCallbackDrop()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.OFFER_LIST,
            getParamsDrop(),
            "", true
        )
    }


    private fun getParamsDrop(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("post_type", postType)
            jsonStr.put("offer_type", offerType)
            jsonStr.put("page_number", pageNumber)
            jsonStr.put("user_id", userdata?.user_id)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackDrop() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                if (status.equals("success", ignoreCase = true)) {

                    val data = jsonObject.getJSONObject("response_data")
                    val responseData = data.getJSONArray("offers_list")

                    recordsPerPage = jsonObject.getInt("records_per_page")
                    totalRecords = jsonObject.getInt("total_records")
                    totalPages = jsonObject.getInt("total_pages")

                    if (!data.getString("received_pending").equals("0")) {

                        btnReceive.setText("Offers received (" + data.getString("received_pending") + ")")
                    } else if (!data.getString("given_pending").equals("0")) {

                        btnGiven.setText("Offers made (" + data.getString("given_pending") + ")")
                    }

                    // arrayList.clear()
                    if (responseData.length() == 0) {
                        tvNoDatas.visibility = View.VISIBLE
                        recycleView?.visibility = View.GONE
                    } else {
                        tvNoDatas.visibility = View.GONE
                        recycleView?.visibility = View.VISIBLE


                        for (i in 0 until responseData.length()) {
                            val data: TravelPkgData
                            data =
                                gson.fromJson(
                                    responseData.get(i).toString(),
                                    TravelPkgData::class.java
                                )
                            arrayList.add(data)
                        }

                    }


                    mAdapter.notifyDataSetChanged()

                } else {
                    arrayList.clear()

                    mAdapter.notifyDataSetChanged()
                    toast(msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                toast(error.toString())
            }
        }
    }


    //


    internal inner class AdapterClass(val arrayList1: ArrayList<TravelPkgData>) :
        RecyclerView.Adapter<AdapterClass.MyViewHolder>() {
        override fun getItemCount(): Int {

            return arrayList1.size
        }


        internal inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tvName: TextView
            var tvAmount: TextView
            var tvTravelling: TextView
            var tvDate: TextView
            var tvAccept: TextView
            var tvPay: TextView

            var tvReject: TextView
            var tvStatusText: TextView
            var tvDetails: TextView
            var iv: ImageView
            var ll: LinearLayout

            init {
                tvStatusText = view.findViewById(R.id.tvStatusText) as TextView
                tvName = view.findViewById(R.id.tvName) as TextView
                tvAmount = view.findViewById(R.id.tvAmount) as TextView
                tvTravelling = view.findViewById(R.id.tvTravelling) as TextView
                tvDate = view.findViewById(R.id.tvDate) as TextView
                tvAccept = view.findViewById(R.id.tvAccept) as TextView
                tvPay = view.findViewById(R.id.tvPay) as TextView
                tvReject = view.findViewById(R.id.tvReject) as TextView
                tvDetails = view.findViewById(R.id.tvDetails) as TextView
                iv = view.findViewById(R.id.iv) as ImageView
                ll = view.findViewById(R.id.ll) as LinearLayout
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_offer_list, parent, false)
            return MyViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.tvName.text = arrayList1[position].user_name
            holder.tvAmount.text =
                arrayList1[position].currency_type + arrayList1[position].offer_amount
            holder.tvTravelling.text =
                arrayList1[position].depart_from + "-" + arrayList1[position].travel_destination
            holder.tvDate.text = ChangeDateFormat.getDateMonthDate(arrayList1[position].created)
            holder.tvStatusText.setText(arrayList1[position].status_text)

            /*  holder.ll.setOnClickListener {

                  val mainIntent = Intent(
                      this@OfferListActivity,
                      PgkDetailActivity::class.java
                  ).putExtra("ID", arrayList1[position].id)


                  startActivity(mainIntent)
              }
  */


            if (arrayList1[position].status.equals("0")) {

                holder.tvAccept.visibility = View.VISIBLE
                holder.tvReject.visibility = View.VISIBLE
                //  holder.tvStatusText.visibility = View.GONE
            } else {
                //   holder.tvStatusText.visibility = View.VISIBLE
                holder.tvAccept.visibility = View.GONE
                holder.tvReject.visibility = View.GONE
            }

            if (offerType.equals("Given")) {
                holder.tvAccept.visibility = View.GONE
                holder.tvReject.visibility = View.GONE

            }

            if (arrayList1[position].post_type?.equals("Travel Details")!! && offerType.equals("Received") && arrayList1[position].status_text != null && arrayList1[position].status_text.equals(
                    "Accepted"
                )
            ) {
                // holder.tvStatusText.visibility = View.GONE
                holder.tvPay.visibility = View.GONE
            } else if (arrayList1[position].post_type?.equals("Travel Details")!! && offerType.equals(
                    "Given"
                ) && arrayList1[position].status_text != null && arrayList1[position].status_text.equals(
                    "Accepted"
                )
            ) {
                // holder.tvStatusText.visibility = View.GONE
                holder.tvPay.visibility = View.VISIBLE
            }
            holder.tvPay.setOnClickListener {
                try {
                   // amoutCalculate(position)
                    val mainIntent = Intent(
                        this@OfferListActivity,
                        PaymentWVActivity::class.java
                    ).putExtra("OFFER_ID", arrayList1[position].id.toString())
                        .putExtra("USERID", userdata?.user_id)

                    startActivity(mainIntent)
                   // onBuyPressed(holder.tvPay)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            holder.tvDetails.setOnClickListener {


                if (arrayList1[position].post_type.equals("Travel Details")) {
                    val mainIntent = Intent(
                        this@OfferListActivity,
                        OfferDetailActivity::class.java
                    ).putExtra("ID", arrayList1[position].post_id)
                        .putExtra("USERID", arrayList1[position].offer_posted_user_id)
                    startActivity(mainIntent)

                } else {
                    val mainIntent = Intent(
                        this@OfferListActivity,
                        OfferDetailActivity::class.java
                    ).putExtra("ID", arrayList1[position].post_id)
                        .putExtra("USERID", arrayList1[position].offer_posted_user_id)
                    startActivity(mainIntent)
                }


            }
            holder.tvTravelling.setOnClickListener {


                if (arrayList1[position].post_type.equals("Travel Details")) {
                    val mainIntent = Intent(
                        this@OfferListActivity,
                        TravelDetailActivity::class.java
                    ).putExtra("ID", arrayList1[position].post_id)
                    startActivity(mainIntent)

                } else {
                    val mainIntent = Intent(
                        this@OfferListActivity,
                        PgkDetailActivity::class.java
                    ).putExtra("ID", arrayList1[position].post_id)
                    startActivity(mainIntent)
                }


            }
            Glide.with(this@OfferListActivity)
                .load(arrayList1[position].profile_image)
                .apply(RequestOptions.circleCropTransform())
                .into(holder.iv);

            holder.iv.setOnClickListener {
                if (offerType.equals("Given")) {
                    val mainIntent = Intent(
                        this@OfferListActivity,
                        UserProfileActivity::class.java
                    ).putExtra("ID", arrayList1[position].user_id)
                    startActivity(mainIntent)

                } else {
                    val mainIntent = Intent(
                        this@OfferListActivity,
                        UserProfileActivity::class.java
                    ).putExtra("ID", arrayList1[position].offer_posted_user_id)
                    startActivity(mainIntent)

                }
            }
            holder.tvName.setOnClickListener {
                if (offerType.equals("Given")) {
                    val mainIntent = Intent(
                        this@OfferListActivity,
                        UserProfileActivity::class.java
                    ).putExtra("ID", arrayList1[position].user_id)
                    startActivity(mainIntent)

                } else {
                    val mainIntent = Intent(
                        this@OfferListActivity,
                        UserProfileActivity::class.java
                    ).putExtra("ID", arrayList1[position].offer_posted_user_id)
                    startActivity(mainIntent)

                }

            }

            holder.tvAccept.setOnClickListener {
                if (postType.equals("Travel Details")) {
                    arrayList1[position].id?.let { it1 ->
                        offerID = arrayList1[position].id.toString()
                        optionType = "accept"
                        confirmDialog()
                    }
                } else {
                   /* try {
                        amoutCalculate(position)
                        onBuyPressed(holder.tvPay)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }*/
                    try {
                        // amoutCalculate(position)
                        val mainIntent = Intent(
                            this@OfferListActivity,
                            PaymentWVActivity::class.java
                        ).putExtra("OFFER_ID", arrayList1[position].id.toString())
                            .putExtra("USERID", userdata?.user_id)
                        startActivity(mainIntent)
                        // onBuyPressed(holder.tvPay)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }


            }
            holder.tvReject.setOnClickListener {
                arrayList1[position].id?.let { it1 ->
                    offerID = arrayList1[position].id.toString()
                    optionType = "reject"
                    confirmDialog()
                }
            }

        }

        private fun amoutCalculate(position: Int) {

            try {
                amount = arrayList1[position].offer_amount?.toDouble()!!
                finalPrice =
                    (amount)?.times(HomeActivity.taxtPercent.toDouble())!!
                perAmount = finalPrice?.div(100)!!
                finalPrice = perAmount.plus(amount)
                offerID = arrayList1[position].id.toString()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    private fun confirmDialog() {
        val alertDialog =
            android.app.AlertDialog.Builder(this).create()
        alertDialog.setTitle("Offers")

        if (optionType.equals("accept")) {
            alertDialog.setMessage("Are you sure want to accept this offer?")
        } else {
            alertDialog.setMessage("Are you sure want to reject this offer?")
        }
        alertDialog.setButton(
            android.app.AlertDialog.BUTTON_POSITIVE, "Confirm"
        ) { dialog, which ->
            dialog.dismiss()
            callWS_REJECT()
        }
        alertDialog.setButton(
            android.app.AlertDialog.BUTTON_NEUTRAL, "Cancel"
        ) { dialog, which ->
            dialog.dismiss()

        }
        alertDialog.show()
    }


    private fun callWSDelete() {
        initCallbackDelete()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.OFFER_TRAVEL_ACCEPT,
            getParamsDelete(),
            "", true
        )
    }

    private fun getParamsDelete(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("post_id", arrayList.get(selectedPosition).id)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackDelete() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                totalRecords = jsonObject.getInt("total_records")
                totalPages = jsonObject.getInt("total_pages")
                if (status.equals("success", ignoreCase = true)) {
                    toast(msg)
                    pageNumber = 1
                    arrayList.clear()
                    callWSList()
                } else {
                    toast(msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                toast(error.toString())
            }
        }
    }


    //Reject OFFER

    private fun callWS_REJECT() {

        var url: String

        if (optionType.equals("accept") && postType.equals("Travel Details")) {
            url = URLS.OFFER_TRAVEL_ACCEPT
        } else if (optionType.equals("accept") && postType.equals("Package Details")) {
            url = URLS.OFFER_PKG_ACCEPT
        } else {
            url = URLS.OFFER_REJECT
        }
        initCallback()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            url,
            getParamsAR(),
            "", true
        )
    }


    private fun getParamsAR(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("offer_id", offerID)
            jsonStr.put("user_id", userdata?.user_id)
            jsonStr.put("device_type", "Android")
            jsonStr.put(
                "device_id", HomeActivity.token
            )
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }

    private fun initCallback() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) {
                try {
                    val jsonObject = JSONObject(response)
                    val status = jsonObject.getString("response_status")
                    val msg = jsonObject.getString("response_msg")
                    if (status.equals("success", ignoreCase = true)) {
                        toast(msg)
                        callWSList()
                    } else {
                        toast(msg)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) {
                toast(error.toString())
            }
        }
    }

    //

    //paypal

    //

    override fun onDestroy() {
        stopService(Intent(this, PayPalService::class.java))
        super.onDestroy()
    }

    private fun paypalService() {
        val intent = Intent(this, PayPalService::class.java)
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)
        startService(intent)
    }


    fun onBuyPressed(pressed: View?) { // PAYMENT_INTENT_SALE will cause the payment to complete immediately.
// Change PAYMENT_INTENT_SALE to
//   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
//   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
//     later via calls from your server.
        val payment = PayPalPayment(
            finalPrice.toBigDecimal(), "USD", "iCarri Payment",
            PayPalPayment.PAYMENT_INTENT_SALE
        )

        isPaymentProcess = true
        val intent = Intent(this, PaymentActivity::class.java)
        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment)
        startActivityForResult(intent, 0)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode==PAYMENT_STATUS_CODE){
                callWSPaymentSuccess()
            }
           /* val confirm: PaymentConfirmation =
                data!!.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION)
            if (confirm != null) {
                try {
                    Log.i("paymentExample", confirm.toJSONObject().toString(4))
                    // TODO: send 'confirm' to your server for verification.
                    var json = confirm.toJSONObject()
                    var resJson = json.getJSONObject("response")
                    transactionID = resJson.getString("id")
                    paymentJson = confirm.toJSONObject().toString()
                    callWSPaymentSuccess()
// see https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
// for more details.
                } catch (e: JSONException) {
                    Log.e(
                        "paymentExample",
                        "an extremely unlikely failure occurred: ",
                        e
                    )
                }
            }*/
        } else if (resultCode == Activity.RESULT_CANCELED) {
            Log.i("paymentExample", "The user canceled.")
        } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
            Log.i(
                "paymentExample",
                "An invalid Payment or PayPalConfiguration was submitted. Please see the docs."
            )
        }
    }

    private fun callWSPaymentSuccess() {
        initCallbackPayment()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.PAYMENT_SUCCESS,
            getPaymentSuccess(),
            "", true
        )
    }


    private fun getPaymentSuccess(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("user_id", userdata?.user_id)
            jsonStr.put("payment_json", paymentJson)
            jsonStr.put("payment_json", paymentJson)
            jsonStr.put("offer_id", offerID)
            jsonStr.put("offer_amount", amount)
            jsonStr.put("tax_percent", HomeActivity.taxtPercent)
            jsonStr.put("tax_amount", perAmount)
            jsonStr.put("total_paid_amount", finalPrice)
            jsonStr.put("transaction_id", transactionID)

            jsonStr.put("device_type", "Android")
            jsonStr.put(
                "device_id",
                HomeActivity.token
            ); //FirebaseInstanceId.getInstance().getToken());
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }

    private fun initCallbackPayment() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) {
                try {
                    val jsonObject = JSONObject(response)
                    val status = jsonObject.getString("response_status")
                    val msg = jsonObject.getString("response_msg")
                    if (status.equals("success", ignoreCase = true)) {

                        toast(msg)
                        pageNumber = 1
                        arrayList.clear()
                        callWSList()
                        isPaymentProcess = false
                        /*         val mainIntent =
                                     Intent(this@OfferListActivity, GeneratePassActivity::class.java)
                                 mainIntent.putExtra("EMAIL", email)
                                 startActivity(mainIntent)*/
                    } else {
                        toast(msg)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                toast(error.toString())
            }
        }
    }


    companion object {
        var isPaymentProcess: Boolean = false
        var offerType: String = "Received"
         var postType: String = "Travel Details"
        private val config =
            PayPalConfiguration() // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
// or live (ENVIRONMENT_PRODUCTION)
                .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
                .clientId(PAYPAL_CLIENT_ID)
    }
//
}
