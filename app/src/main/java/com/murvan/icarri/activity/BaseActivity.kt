package com.murvan.icarri.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.murvan.icarri.utils.hideKeyboard
import kotlinx.android.synthetic.main.activity_login.*

open class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
}