package com.murvan.icarri.activity

import android.content.Intent
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.murvan.icarri.R
import com.murvan.icarri.models.InboxData
import com.murvan.icarri.models.UserData
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.ChangeDateFormat
import com.murvan.icarri.utils.Constant
import com.murvan.icarri.utils.Preference
import com.murvan.icarri.utils.toast
import kotlinx.android.synthetic.main.activity_my_travel_list.*
import org.apache.commons.lang3.StringEscapeUtils
import org.json.JSONException
import org.json.JSONObject
import java.lang.Exception
import java.util.ArrayList
import java.util.HashMap

class InboxActivity : AppCompatActivity() {

    var recycleView: RecyclerView? = null
    var arrayList = ArrayList<InboxData>()
    internal lateinit var mAdapter: AdapterClass
    val gson = Gson()
    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null
    var userdata: UserData? = null
    var status: String = "All"
    var selectedPosition: Int = -1

    //
    var pastVisiblesItems = 0
    var visibleItemCount = 0
    var totalItemCount = 0
    private var loading = true
    var pageNumber: Int = 1
    var totalRecords: Int = 0
    var totalPages: Int = 0
    //

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_travel_list)

        tvTitles.setText("Inbox")
        tvNoDatas.setText(resources.getString(R.string.norecord))
        ivAdd.visibility = View.INVISIBLE
        tvNoDatas.setOnClickListener {
            callWSList()
        }

        ivBacks.setOnClickListener {

            finish()
        }

        ivAdd.setOnClickListener {
            startActivity(Intent(this@InboxActivity, AddPkgActivity::class.java))


        }

        userdata =
            gson.fromJson(
                Preference.getInstance(this).getString(Constant.USER_DETAIL),
                UserData::class.java
            )


        recycleView = findViewById(R.id.recycleView)

        var layoutManager = LinearLayoutManager(this)
        val layoutManagerPost = LinearLayoutManager(this)
        recycleView?.layoutManager = layoutManager
        recycleView?.itemAnimator = DefaultItemAnimator()

        mAdapter = AdapterClass(arrayList)
        recycleView?.adapter = mAdapter
        mAdapter.notifyDataSetChanged()
        recycleView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(
                recyclerView: RecyclerView,
                dx: Int,
                dy: Int
            ) {

                var visibleItemCount = layoutManager.getChildCount();
                var totalItemCount = layoutManager.getItemCount();
                var firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (totalPages > pageNumber) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                    ) {
                        pageNumber++
                        callWSList()
                    }
                }

            }
        })
    }


    override fun onResume() {
        super.onResume()
        if (pageNumber == 1) {
            arrayList.clear()
            callWSList();
        }
    }
    //

    private fun callWSList() {
        initCallbackDrop()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.INBOX_MSG,
            getParamsDrop(),
            "", true
        )
    }


    private fun getParamsDrop(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("user_id", userdata?.user_id)
            jsonStr.put("page_number", pageNumber)

            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackDrop() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                totalRecords = jsonObject.getInt("total_records")
                totalPages = jsonObject.getInt("total_pages")
                if (status.equals("success", ignoreCase = true)) {

                    val responseData = jsonObject.getJSONObject("response_data")
                    var msgList = responseData.getJSONArray("messages_list")
                    if (msgList.length() == 0) {
                        tvNoDatas.visibility = View.VISIBLE
                        recycleView?.visibility = View.GONE
                    } else {
                        tvNoDatas.visibility = View.GONE
                        recycleView?.visibility = View.VISIBLE

                        //  arrayList.clear()
                        for (i in 0 until msgList.length()) {
                            val data: InboxData
                            data =
                                gson.fromJson(
                                    msgList.get(i).toString(),
                                    InboxData::class.java
                                )
                            arrayList.add(data)
                        }

                        //  mAdapter = AdapterClass(arrayList)
                        // recycleView?.adapter = mAdapter
                        mAdapter.notifyDataSetChanged()

                    }


                } else {
                    toast(msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                toast(error.toString())
            }
        }
    }


    //


    internal inner class AdapterClass(val arrayList1: ArrayList<InboxData>) :
        RecyclerView.Adapter<AdapterClass.MyViewHolder>() {
        override fun getItemCount(): Int {

            return arrayList1.size
        }


        internal inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tvName: TextView
            var tvMsg: TextView
            var tvLocation: TextView
            var tvDate: TextView
            var iv: ImageView
            var ll: LinearLayout
            var llRoot: LinearLayout

            init {
                tvName = view.findViewById(R.id.tvName) as TextView
                tvMsg = view.findViewById(R.id.tvMsg) as TextView
                tvLocation = view.findViewById(R.id.tvLocation) as TextView
                tvDate = view.findViewById(R.id.tvDate) as TextView
                iv = view.findViewById(R.id.iv) as ImageView
                ll = view.findViewById(R.id.ll) as LinearLayout
                llRoot = view.findViewById(R.id.llRoot) as LinearLayout
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_inbox_list, parent, false)

            return MyViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


            if (arrayList1[position].read_unread.equals("0")) {
                //   holder.llRoot.setBackgroundColor(resources.getColor(R.color.light_pink)
                holder.tvName.setTypeface(Typeface.DEFAULT_BOLD)
                holder.tvMsg.setTypeface(Typeface.DEFAULT_BOLD)
                holder.tvLocation.setTypeface(Typeface.DEFAULT_BOLD)
                holder.tvDate.setTypeface(Typeface.DEFAULT_BOLD)

            } else {
                holder.tvName.setTypeface(Typeface.DEFAULT)
                holder.tvMsg.setTypeface(Typeface.DEFAULT)
                holder.tvLocation.setTypeface(Typeface.DEFAULT)
                holder.tvDate.setTypeface(Typeface.DEFAULT)
                // holder.llRoot.setBackgroundColor(resources.getColor(R.color.white))
            }

            holder.tvName.text = arrayList1[position].user_name
            holder.tvMsg.text = StringEscapeUtils.unescapeJava(arrayList1[position].message_text)
            holder.tvLocation.text =
                arrayList1[position].depart_from + "-" + arrayList1[position].travel_destination
            holder.tvDate.text = arrayList1[position].time_ago



            holder.ll.setOnClickListener {
                val mainIntent = Intent(
                    this@InboxActivity,
                    MsgListActivity::class.java
                ).putExtra("DATA", arrayList1[position])
                startActivity(mainIntent)
            }
            Glide.with(this@InboxActivity)
                .load(arrayList1[position].profile_image)
                .apply(RequestOptions.circleCropTransform())
                .into(holder.iv);

        }
    }


    fun showPopupMenu(view: View, position: Int) {

        var popup = PopupMenu(this, view);
        popup.getMenuInflater().inflate(R.menu.list_menu, popup.getMenu())
        popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { it ->
            val item = it!!.itemId
            when (item) {
                R.id.menuEdit -> {
                    startActivity(
                        Intent(
                            this@InboxActivity,
                            AddPkgActivity::class.java
                        ).putExtra("DATA", arrayList.get(position))
                    )
                }
                R.id.menuDelete -> {
                    deleteDialog(position)
                }

                R.id.menuAdditional -> {
                    val mainIntent = Intent(
                        this@InboxActivity,
                        AdditionalImgActivity::class.java
                    ).putExtra("ID", arrayList[position].id).putExtra(
                        "TITLES",
                        arrayList[position].depart_from + " To " + arrayList[position].travel_destination
                    )
                    startActivity(mainIntent)
                }
            }
            true

        })


        popup.show();


    }


    fun deleteDialog(position: Int) {
        val builder = AlertDialog.Builder(this)
        //set message for alert dialog
        builder.setMessage(R.string.confirm_delete)
        builder.setIcon(android.R.drawable.ic_dialog_alert)

        //performing positive action
        builder.setPositiveButton("Yes") { dialogInterface, which ->
            selectedPosition = position
            callWSDelete()

        }
        builder.setNegativeButton("No") { dialogInterface, which ->

        }
        //performing cancel action
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }


    private fun callWSDelete() {
        initCallbackDelete()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.DELETE_TRAVELER,
            getParamsDelete(),
            "", true
        )
    }

    private fun getParamsDelete(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("post_id", arrayList.get(selectedPosition).id)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackDelete() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                if (status.equals("success", ignoreCase = true)) {
                    toast(msg)
                    callWSList()
                } else {
                    toast(msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                toast(error.toString())
            }
        }
    }


    //


}
