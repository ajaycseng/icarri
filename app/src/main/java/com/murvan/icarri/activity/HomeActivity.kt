package com.murvan.icarri.activity

import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.murvan.icarri.R
import com.murvan.icarri.activity.AddTravelActivity.Companion.isFromHome
import com.murvan.icarri.models.UserData
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.Constant
import com.murvan.icarri.utils.Preference
import com.murvan.icarri.utils.toast
import kotlinx.android.synthetic.main.home_content.*
import kotlinx.android.synthetic.main.side_menu.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class HomeActivity : BaseActivity() {

    var rvSideMenu: RecyclerView? = null
    var navItems = ArrayList<String>()
    internal lateinit var mAdapter: AdapterClass
    var toggle: ActionBarDrawerToggle? = null
    var main_layout: FrameLayout? = null
    var fragContainer: FrameLayout? = null
    var slideMenu: LinearLayout? = null
    var dlMain: DrawerLayout? = null
    var ivMenu: ImageView? = null
    var userID: String? = null
    var userData: UserData? = null
    private val gson = Gson()
    var unreadCount: String = ""
    var pendingOfferCount: String = ""
    lateinit var mRegistrationBroadcastReceiver: BroadcastReceiver


    companion object {
        var token: String = ""
        var taxtPercent: String = ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)


        drawerInit()
        //  firebaseInit()


        llInbox.setOnClickListener {

            startActivity(Intent(this, InboxActivity::class.java))
        }
        llSearch.setOnClickListener {
            startActivity(Intent(this, SearchActivity::class.java))
        }
        llTravel.setOnClickListener {
            startActivity(Intent(this, MyTravelListActivity::class.java))
        }
        llpkg.setOnClickListener {
            startActivity(Intent(this, MyPkgListActivity::class.java))
        }
        llOffer.setOnClickListener {

            startActivity(Intent(this, OfferListActivity::class.java))
        }
        llPay.setOnClickListener {
            startActivity(Intent(this, PaymentListActivity::class.java))
        }
        ivEdit.setOnClickListener {
            startActivity(Intent(this, EditProfileActivity::class.java))
        }
        llReview.setOnClickListener {
            startActivity(Intent(this, ReviewsListActivity::class.java))
        }
        llPassword.setOnClickListener {
            startActivity(Intent(this, ChangePassActivity::class.java))
        }
        llLogout.setOnClickListener {
            logout()

        }

        /*  btnPostPkg.setOnClickListener {
              isFromHome = true
              startActivity(Intent(this, AddPkgActivity::class.java))


          }
          btnPostTravel.setOnClickListener {
              isFromHome = true
              startActivity(Intent(this, AddTravelActivity::class.java))

          }
          btnLogout.setOnClickListener {
              logout()
          }
  */
        /*  FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener { task ->
              if (!task.isSuccessful) {
                  token = task.exception!!.message.toString()
                  Log.w("FCM TOKEN Failed", task.exception)
              } else {
                  token = task.result!!.token
                  Log.i("FCM TOKEN", token)
              }
          }*/
    }


    private fun drawerInit() {
        dlMain = findViewById(R.id.dlMain) as DrawerLayout
        ivMenu = findViewById(R.id.ivMenu)

        main_layout = findViewById(R.id.main_layout) as FrameLayout
        fragContainer = findViewById(R.id.fragContainer) as FrameLayout
        toggle = object :
            ActionBarDrawerToggle(this, dlMain, R.string.close_drawer, R.string.open_drawer) {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                super.onDrawerSlide(drawerView, slideOffset)

                // setUserDATA();
                main_layout!!.translationX = slideOffset * drawerView.width
                dlMain!!.bringChildToFront(drawerView)
                dlMain!!.requestLayout()
            }
        }
        dlMain?.addDrawerListener(toggle as ActionBarDrawerToggle)

        ivMenu?.setOnClickListener {
            slideMenu?.let { it1 -> dlMain?.openDrawer(it1) }

        }

        ivUser.setOnClickListener { }


        slideMenu = findViewById(R.id.slideMenu) as LinearLayout




        userID = userData?.user_id
        if (userID.isNullOrEmpty()) {
            navItems.add("Inbox")
            navItems.add("Search")
            navItems.add("Travel Details")
            navItems.add("Package Details")
            navItems.add("Offers")
            navItems.add("Payment Transactions")
            navItems.add("Reviews")
            navItems.add("Edit Profile")
            navItems.add("Bank Details")
            navItems.add("Change Password")
            navItems.add("Logout")
        } else {
            navItems.add("Inbox")
            navItems.add("Search")
            navItems.add("Travel Details")
            navItems.add("Package Details")
            navItems.add("Offers")
            navItems.add("Payment Transactions")
            navItems.add("Reviews")
            navItems.add("Edit Profile")
            navItems.add("Bank Details")
            navItems.add("Change Password")
            navItems.add("Logout")
            //setUserDATA()
        }
        rvSideMenu = findViewById(R.id.rvSideMenu)
        mAdapter = AdapterClass(navItems)
        var layoutManager = LinearLayoutManager(this)
        val layoutManagerPost = LinearLayoutManager(this)
        rvSideMenu?.layoutManager = layoutManager
        rvSideMenu?.itemAnimator = DefaultItemAnimator()
        rvSideMenu?.adapter = mAdapter
        mAdapter.notifyDataSetChanged()

    }

    override fun onResume() {
        super.onResume()

        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("HomeActivity", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                token = task.result?.token.toString()
                Log.d("deviceId", token)

            })
        setData()
        callWSList()

    }


    override fun onPause() {
        //  LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause()
    }

    private fun setData() {
        userData =
            gson.fromJson(
                Preference.getInstance(this).getString(Constant.USER_DETAIL),
                UserData::class.java
            )
        tvUserName.setText(userData?.first_name)
        tvName.setText("Welcome " + userData?.first_name)

        if (!userData?.profile_image.isNullOrEmpty()) {
            Glide.with(this)
                .load(userData?.profile_image)
                .apply(RequestOptions.circleCropTransform())
                .into(ivlogo);
        } else {
            ivlogo.setImageResource(R.drawable.avtar)
        }
    }

    internal inner class AdapterClass(val navItems: ArrayList<String>) :
        RecyclerView.Adapter<AdapterClass.MyViewHolder>() {
        override fun getItemCount(): Int {

            return navItems.size
        }


        internal inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tvName: TextView
            var iv: ImageView
            var ivUnRead: ImageView
            var ll: LinearLayout

            init {
                tvName = view.findViewById(R.id.tvName) as TextView
                iv = view.findViewById(R.id.iv) as ImageView
                ivUnRead = view.findViewById(R.id.ivUnRead) as ImageView
                ll = view.findViewById(R.id.ll) as LinearLayout
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_nav_menu, parent, false)

            return MyViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

            holder.tvName.text = navItems[position]
            holder.ivUnRead.visibility = View.GONE
            if (userID.isNullOrEmpty()) {
                if (position == 0) {
                    holder.iv.setImageResource(R.drawable.ic_inbox)
                    if (!unreadCount.equals("0")) {

                        holder.ivUnRead.visibility = View.VISIBLE
                    }
                } else if (position == 1) {
                    holder.iv.setImageResource(R.drawable.ic_search_type)
                } else if (position == 2) {
                    holder.iv.setImageResource(R.drawable.ic_travel)
                } else if (position == 3) {
                    holder.iv.setImageResource(R.drawable.ic_pkg)
                } else if (position == 4) {
                    holder.iv.setImageResource(R.drawable.ic_offer)
                    if (!pendingOfferCount.equals("0")) {

                        holder.ivUnRead.visibility = View.VISIBLE
                    }
                } else if (position == 5) {
                    holder.iv.setImageResource(R.drawable.ic_payment)
                } else if (position == 6) {
                    holder.iv.setImageResource(R.drawable.ic_review)
                } else if (position == 7) {
                    holder.iv.setImageResource(R.drawable.ic_pen_orange)
                } else if (position == 8) {
                    holder.iv.setImageResource(R.drawable.ic_money)
                } else if (position == 9) {
                    holder.iv.setImageResource(R.drawable.ic_password)
                } else if (position == 10) {
                    holder.iv.setImageResource(R.drawable.ic_logout)
                }

                holder.ll.setOnClickListener {
                    if (slideMenu?.let { it1 -> dlMain?.isDrawerOpen(it1) }!!)
                        dlMain?.closeDrawer(slideMenu!!)
                    if (position == 0) {
                        startActivity(Intent(this@HomeActivity, InboxActivity::class.java))
                    } else if (position == 1) {
                        startActivity(Intent(this@HomeActivity, SearchActivity::class.java))
                    } else if (position == 2) {
                        startActivity(Intent(this@HomeActivity, MyTravelListActivity::class.java))
                    } else if (position == 3) {
                        startActivity(Intent(this@HomeActivity, MyPkgListActivity::class.java))
                    } else if (position == 4) {
                        startActivity(Intent(this@HomeActivity, OfferListActivity::class.java))
                    } else if (position == 5) {
                        startActivity(Intent(this@HomeActivity, PaymentListActivity::class.java))
                    } else if (position == 6) {
                        startActivity(Intent(this@HomeActivity, ReviewsListActivity::class.java))
                    } else if (position == 7) {
                        startActivity(Intent(this@HomeActivity, EditProfileActivity::class.java))
                    } else if (position == 8) {
                        startActivity(Intent(this@HomeActivity, BankDetailActivity::class.java))
                    } else if (position == 9) {
                        startActivity(Intent(this@HomeActivity, ChangePassActivity::class.java))
                    } else if (position == 10) {
                        logout()
                    }
                }
            } else {


                if (position == 0) {
                    holder.iv.setImageResource(R.drawable.ic_inbox)
                    if (!unreadCount.equals("0")) {

                        holder.ivUnRead.visibility = View.VISIBLE
                    }
                } else if (position == 1) {
                    holder.iv.setImageResource(R.drawable.ic_search_type)
                } else if (position == 2) {
                    holder.iv.setImageResource(R.drawable.ic_travel)
                } else if (position == 3) {
                    holder.iv.setImageResource(R.drawable.ic_pkg)
                } else if (position == 4) {
                    holder.iv.setImageResource(R.drawable.ic_offer)
                    if (!pendingOfferCount.equals("0")) {

                        holder.ivUnRead.visibility = View.VISIBLE
                    }
                } else if (position == 5) {
                    holder.iv.setImageResource(R.drawable.ic_payment)
                } else if (position == 6) {
                    holder.iv.setImageResource(R.drawable.ic_review)
                } else if (position == 7) {
                    holder.iv.setImageResource(R.drawable.ic_pen_orange)
                } else if (position == 8) {
                    holder.iv.setImageResource(R.drawable.ic_money)
                } else if (position == 9) {
                    holder.iv.setImageResource(R.drawable.ic_password)
                } else if (position == 10) {
                    holder.iv.setImageResource(R.drawable.ic_logout)
                }


                holder.ll.setOnClickListener {
                    if (slideMenu?.let { it1 -> dlMain?.isDrawerOpen(it1) }!!)
                        dlMain?.closeDrawer(slideMenu!!)
                    if (position == 0) {
                        startActivity(Intent(this@HomeActivity, InboxActivity::class.java))
                    } else if (position == 1) {
                        startActivity(Intent(this@HomeActivity, SearchActivity::class.java))
                    } else if (position == 2) {
                        startActivity(Intent(this@HomeActivity, MyTravelListActivity::class.java))
                    } else if (position == 3) {
                        startActivity(Intent(this@HomeActivity, MyPkgListActivity::class.java))
                    } else if (position == 4) {
                        startActivity(Intent(this@HomeActivity, OfferListActivity::class.java))
                    } else if (position == 5) {
                        startActivity(Intent(this@HomeActivity, PaymentListActivity::class.java))
                    } else if (position == 6) {
                        startActivity(Intent(this@HomeActivity, ReviewsListActivity::class.java))
                    } else if (position == 7) {
                        startActivity(Intent(this@HomeActivity, EditProfileActivity::class.java))
                    } else if (position == 8) {
                        startActivity(Intent(this@HomeActivity, BankDetailActivity::class.java))
                    } else if (position == 9) {
                        startActivity(Intent(this@HomeActivity, ChangePassActivity::class.java))
                    } else if (position == 10) {
                        logout()
                    }
                }

            }
        }
    }


    private fun logout() {
        val alertDialog =
            AlertDialog.Builder(this).create()
        alertDialog.setTitle("Alert")
        alertDialog.setMessage("Do you want to Logout")
        alertDialog.setButton(
            AlertDialog.BUTTON_POSITIVE, "YES"
        ) { dialog, which ->
            dialog.dismiss()

            callWSListLogout();

        }
        alertDialog.setButton(
            AlertDialog.BUTTON_NEGATIVE, "Cancel"
        ) { dialog, which ->
            dialog.dismiss()

        }
        alertDialog.show()
    }


    //dot


    private fun callWSList() {
        initCallbackDrop()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.UNREAD_MSG,
            getParamsDrop(),
            "", true
        )
    }


    private fun getParamsDrop(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("user_id", userData?.user_id)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
            Log.d("deviceid", "device_id " + HomeActivity.token)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }

    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null
    private fun initCallbackDrop() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                if (status.equals("success", ignoreCase = true)) {

                    val responseData = jsonObject.getJSONObject("response_data")

                    unreadCount = responseData.getString("total_unread_messages")
                    taxtPercent = responseData.getString("tax_percent")
                    pendingOfferCount = responseData.getString("total_pending_offers")

                    if (!unreadCount.equals("0")) {
                        ivUnReadInbox.visibility = View.VISIBLE
                    } else {
                        ivUnReadInbox.visibility = View.INVISIBLE
                    }
                    if (!pendingOfferCount.equals("0")) {
                        ivUnReadOffer.visibility = View.VISIBLE
                    } else {
                        ivUnReadOffer.visibility = View.INVISIBLE
                    }
                    mAdapter.notifyDataSetChanged()

                } else {
                    toast(msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                toast(error.toString())
            }
        }
    }

    //


    // logout
    private fun callWSListLogout() {
        initCallbackLogout()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.LOGOUT,
            getParamsLogout(),
            "", true
        )
    }


    private fun getParamsLogout(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("user_id", userData?.user_id)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
            Log.d("deviceid", "device_id " + HomeActivity.token)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }

    private fun initCallbackLogout() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                if (status.equals("success", ignoreCase = true)) {
                    toast(msg)
                    Preference.getInstance(this@HomeActivity).putString(Constant.USER_ID, "")
                    Preference.getInstance(this@HomeActivity).putString(Constant.USER_DETAIL, "")
                    startActivity(Intent(this@HomeActivity, LoginActivity::class.java))
                    finishAffinity()
                } else {
                    toast(msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                toast(error.toString())
            }
        }
    }

    //

}
