package com.murvan.icarri.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.murvan.icarri.R
import com.murvan.icarri.models.TravelPkgData
import com.murvan.icarri.models.UserData
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.ChangeDateFormat
import com.murvan.icarri.utils.Constant
import com.murvan.icarri.utils.Preference
import com.murvan.icarri.utils.toast
import kotlinx.android.synthetic.main.activity_my_travel_list.*
import kotlinx.android.synthetic.main.row_travel_list.*
import org.json.JSONException
import org.json.JSONObject
import java.lang.Exception
import java.util.ArrayList
import java.util.HashMap

class SearchTravelListActivity : AppCompatActivity() {

    var recycleView: RecyclerView? = null
    var arrayList = ArrayList<TravelPkgData>()
    internal lateinit var mAdapter: AdapterClass
    val gson = Gson()
    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null
    var userdata: UserData? = null
    var postType: String = "Travel Details"
    var status: String = "All"
    var selectedPosition: Int = -1
    var travelPkgData = TravelPkgData()

    //
    var pastVisiblesItems = 0
    var visibleItemCount = 0
    var totalItemCount = 0
    private var loading = true
    var pageNumber: Int = 1
    var totalRecords: Int = 0
    var totalPages: Int = 0
    //

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_travel_list)
        tvTitles.setText("Search Travel")

        travelPkgData = intent.getSerializableExtra("DATA") as TravelPkgData

        tvNoDatas.setOnClickListener {
            callWSList()
        }

        ivBacks.setOnClickListener {

            finish()
        }
        ivAdd.visibility = View.INVISIBLE
        /* ivAdd.setOnClickListener {
             startActivity(Intent(this@SearchTravelListActivity, AddTravelActivity::class.java))


         }
 */
        userdata =
            gson.fromJson(
                Preference.getInstance(this).getString(Constant.USER_DETAIL),
                UserData::class.java
            )


        recycleView = findViewById(R.id.recycleView)

        var layoutManager = LinearLayoutManager(this)
        val layoutManagerPost = LinearLayoutManager(this)
        recycleView?.layoutManager = layoutManager
        recycleView?.itemAnimator = DefaultItemAnimator()
        mAdapter = AdapterClass(arrayList)

        recycleView?.adapter = mAdapter
        mAdapter.notifyDataSetChanged()
        recycleView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(
                recyclerView: RecyclerView,
                dx: Int,
                dy: Int
            ) {

                var visibleItemCount = layoutManager.getChildCount();
                var totalItemCount = layoutManager.getItemCount();
                var firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (totalPages > pageNumber) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                    ) {
                        pageNumber++
                        callWSList()
                    }
                }

            }
        })
    }


    override fun onResume() {
        super.onResume()
        if (pageNumber == 1) {
            arrayList.clear()
            callWSList();
        }
    }
    //

    private fun callWSList() {
        initCallbackDrop()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.SEARCH,
            getParamsDrop(),
            "", true
        )
    }


    private fun getParamsDrop(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {

            jsonStr.put("user_id", userdata?.user_id)
            jsonStr.put("page_number", pageNumber)

            jsonStr.put("depart_from", travelPkgData.depart_from)
            jsonStr.put("travel_destination", travelPkgData.travel_destination)
            jsonStr.put("travel_mode", travelPkgData.travel_mode)
            jsonStr.put("travel_date", travelPkgData.travel_date)
            jsonStr.put("post_type", postType)
            // jsonStr.put("status", status)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackDrop() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                if (status.equals("success", ignoreCase = true)) {
                    totalRecords = jsonObject.getInt("total_records")
                    totalPages = jsonObject.getInt("total_pages")

                    val responseData = jsonObject.getJSONArray("response_data")
                    if (responseData.length() == 0) {
                        tvNoDatas.visibility = View.VISIBLE
                        recycleView?.visibility = View.GONE
                    } else {
                        tvNoDatas.visibility = View.GONE
                        recycleView?.visibility = View.VISIBLE

                        //   arrayList.clear()
                        for (i in 0 until responseData.length()) {
                            val data: TravelPkgData
                            data =
                                gson.fromJson(
                                    responseData.get(i).toString(),
                                    TravelPkgData::class.java
                                )
                            arrayList.add(data)
                        }

                        //  mAdapter = AdapterClass(arrayList)
                        // recycleView?.adapter = mAdapter
                        mAdapter.notifyDataSetChanged()

                    }


                } else {
                    toast(msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                toast(error.toString())
            }
        }
    }


    //


    internal inner class AdapterClass(val arrayList1: ArrayList<TravelPkgData>) :
        RecyclerView.Adapter<AdapterClass.MyViewHolder>() {
        override fun getItemCount(): Int {

            return arrayList1.size
        }


        internal inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tvName: TextView
            var tvTo: TextView
            var tvFrom: TextView
            var tvDate: TextView
            var tvTime: TextView
            var ivOption: ImageView

            var tvDescription: TextView

            var iv: ImageView
            var ivUser: ImageView
            var ivDepart: ImageView
            var ivArrive: ImageView
            var ivVia: ImageView

            var ll: LinearLayout

            init {
                tvName = view.findViewById(R.id.tvName) as TextView
                tvTo = view.findViewById(R.id.tvTo) as TextView
                tvFrom = view.findViewById(R.id.tvFrom) as TextView
                tvDate = view.findViewById(R.id.tvDate) as TextView
                tvTime = view.findViewById(R.id.tvTime) as TextView
                tvDescription = view.findViewById(R.id.tvDescription) as TextView
                ivOption = view.findViewById(R.id.ivOption) as ImageView
                iv = view.findViewById(R.id.iv) as ImageView
                ivDepart = view.findViewById(R.id.ivDepart) as ImageView
                ivUser = view.findViewById(R.id.ivUser) as ImageView
                ivVia = view.findViewById(R.id.ivVia) as ImageView
                ivArrive = view.findViewById(R.id.ivArrive) as ImageView

                ll = view.findViewById(R.id.ll) as LinearLayout
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_travel_list, parent, false)

            return MyViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.tvName.text = arrayList1[position].user_name
            holder.tvTo.text = arrayList1[position].travel_destination
            holder.tvFrom.text = arrayList1[position].depart_from
            holder.tvDate.text =
                ChangeDateFormat.getDateMonthDate(arrayList1[position].travel_date)
            holder.tvTime.text =
                ChangeDateFormat.getTimeFromDateTime(arrayList1[position].travel_time)
            holder.tvDescription.text = arrayList1[position].post_description
            // holder.tvReceiverComment.text = arrayList1[position].post_description

            if (arrayList1[position].travel_mode.equals("Train")) {
                holder.ivDepart.setImageResource(R.drawable.ic_train)
                holder.ivArrive.setImageResource(R.drawable.ic_train)
                holder.ivVia.setImageResource(R.drawable.ic_train_front)

            } else if (arrayList1[position].travel_mode.equals("Bus")) {
                holder.ivDepart.setImageResource(R.drawable.ic_bus)
                holder.ivArrive.setImageResource(R.drawable.ic_bus)
                holder.ivVia.setImageResource(R.drawable.ic_bus_front)


            } else if (arrayList1[position].travel_mode.equals("Flight")) {
                holder.ivDepart.setImageResource(R.drawable.ic_departures)
                holder.ivArrive.setImageResource(R.drawable.ic_landing)
                holder.ivVia.setImageResource(R.drawable.ic_front_flight)


            } else if (arrayList1[position].travel_mode.equals("Ship")) {
                holder.ivDepart.setImageResource(R.drawable.ic_ship)
                holder.ivArrive.setImageResource(R.drawable.ic_ship)
                holder.ivVia.setImageResource(R.drawable.ic_ship_front)

            } else if (arrayList1[position].travel_mode.equals("Car")) {
                holder.ivDepart.setImageResource(R.drawable.ic_car)
                holder.ivArrive.setImageResource(R.drawable.ic_car)
                holder.ivVia.setImageResource(R.drawable.ic_car_front)

            } else if (arrayList1[position].travel_mode.equals("Taxi")) {
                holder.ivDepart.setImageResource(R.drawable.ic_taxi)
                holder.ivArrive.setImageResource(R.drawable.ic_taxi)
                holder.ivVia.setImageResource(R.drawable.ic_taxi_front)

            } else if (arrayList1[position].travel_mode.equals("Others")) {
                holder.ivDepart.setImageResource(R.drawable.ic_other)
                holder.ivArrive.setImageResource(R.drawable.ic_other)
                holder.ivVia.setImageResource(R.drawable.ic_other)


            } else if (arrayList1[position].travel_mode.equals("Mini Truck( 1/2 ton)")) {
                holder.ivDepart.setImageResource(R.drawable.ic_mini_truck)
                holder.ivArrive.setImageResource(R.drawable.ic_mini_truck)
                holder.ivVia.setImageResource(R.drawable.ic_mini_truck)

            } else if (arrayList1[position].travel_mode.equals("Light Truck (1 ton)")) {
                holder.ivDepart.setImageResource(R.drawable.ic_light_truck)
                holder.ivArrive.setImageResource(R.drawable.ic_light_truck)
                holder.ivVia.setImageResource(R.drawable.ic_light_truck)

            } else if (arrayList1[position].travel_mode.equals("Heavy Commercial Vehicle")) {
                holder.ivDepart.setImageResource(R.drawable.ic_heavy_commercial_vehicle)
                holder.ivArrive.setImageResource(R.drawable.ic_heavy_commercial_vehicle)
                holder.ivVia.setImageResource(R.drawable.ic_heavy_commercial_vehicle)

            }


            holder.ivOption.visibility = View.INVISIBLE

            holder.ivOption.setOnClickListener {

                showPopupMenu(holder.ivOption, position)
            }

            holder.ll.setOnClickListener {

                val mainIntent = Intent(
                    this@SearchTravelListActivity,
                    TravelDetailActivity::class.java
                ).putExtra("ID", arrayList1[position].id)


                startActivity(mainIntent)
            }
            holder.ivUser.setOnClickListener {

                val mainIntent = Intent(
                    this@SearchTravelListActivity,
                    UserProfileActivity::class.java
                ).putExtra("ID", arrayList1[position].user_id)


                startActivity(mainIntent)
            }
            holder.tvName.setOnClickListener {

                val mainIntent = Intent(
                    this@SearchTravelListActivity,
                    UserProfileActivity::class.java
                ).putExtra("ID", arrayList1[position].user_id)


                startActivity(mainIntent)
            }
            Glide.with(this@SearchTravelListActivity)
                .load(arrayList1[position].main_photo)
                .apply(RequestOptions.fitCenterTransform())
                .into(holder.iv);
            Glide.with(this@SearchTravelListActivity)
                .load(arrayList1[position].profile_image)
                .apply(RequestOptions.circleCropTransform())
                .into(holder.ivUser);

        }
    }


    fun showPopupMenu(view: View, position: Int) {

        var popup = PopupMenu(this, view);

        popup.getMenuInflater().inflate(R.menu.search_list_menu, popup.getMenu())
        popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { it ->
            val item = it!!.itemId
            when (item) {
                R.id.menuEdit -> {
                    startActivity(
                        Intent(
                            this@SearchTravelListActivity,
                            AddTravelActivity::class.java
                        ).putExtra("DATA", arrayList.get(position))
                    )
                }
                R.id.menuDelete -> {
                    deleteDialog(position)
                }

                R.id.menuAdditional -> {
                    val mainIntent = Intent(
                        this@SearchTravelListActivity,
                        AdditionalImgActivity::class.java
                    ).putExtra("ID", arrayList[position].id).putExtra(
                        "TITLES",
                        arrayList[position].depart_from + " To " + arrayList[position].travel_destination
                    )
                    startActivity(mainIntent)
                }
            }
            true

        })


        popup.show();


    }


    fun deleteDialog(position: Int) {
        val builder = AlertDialog.Builder(this)
        //set message for alert dialog
        builder.setMessage(R.string.confirm_delete)
        builder.setIcon(android.R.drawable.ic_dialog_alert)

        //performing positive action
        builder.setPositiveButton("Yes") { dialogInterface, which ->
            selectedPosition = position
            callWSDelete()

        }
        builder.setNegativeButton("No") { dialogInterface, which ->

        }
        //performing cancel action
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }


    private fun callWSDelete() {
        initCallbackDelete()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.DELETE_TRAVELER,
            getParamsDelete(),
            "", true
        )
    }

    private fun getParamsDelete(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("post_id", arrayList.get(selectedPosition).id)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackDelete() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                if (status.equals("success", ignoreCase = true)) {
                    toast(msg)
                    callWSList()
                } else {
                    toast(msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                toast(error.toString())
            }
        }
    }


    //


}
