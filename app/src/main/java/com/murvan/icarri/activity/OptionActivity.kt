package com.murvan.icarri.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.murvan.icarri.R
import kotlinx.android.synthetic.main.activity_option.*

class OptionActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_option)



        ivSkip.setOnClickListener {
            val mainIntent = Intent(this, LoginActivity::class.java)
            startActivity(mainIntent)
        }
        btnCreateProfile.setOnClickListener {
            val mainIntent = Intent(this, SignUpActivity::class.java)
            startActivity(mainIntent)

        }
        btnSearchTraveller.setOnClickListener {
            val mainIntent = Intent(this, LoginActivity::class.java)
            startActivity(mainIntent)
        }
        btnPost.setOnClickListener {
            val mainIntent = Intent(this, LoginActivity::class.java)
            startActivity(mainIntent)
        }

    }
}
