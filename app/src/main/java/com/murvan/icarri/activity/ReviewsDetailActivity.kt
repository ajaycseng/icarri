package com.murvan.icarri.activity

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.WindowManager
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.PopupWindow
import androidx.core.app.ActivityCompat
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.murvan.icarri.R
import com.murvan.icarri.models.DropListData
import com.murvan.icarri.models.ReviewsData
import com.murvan.icarri.models.UserData
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.*
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.activity_add_reviews.*
import kotlinx.android.synthetic.main.activity_add_reviews.iv
import kotlinx.android.synthetic.main.activity_add_reviews.ratinBar
import kotlinx.android.synthetic.main.activity_add_reviews.tvUserName
import kotlinx.android.synthetic.main.activity_add_travel.btnSubmit
import kotlinx.android.synthetic.main.activity_add_travel.etDescription
import kotlinx.android.synthetic.main.activity_add_travel.etTravelMode
import kotlinx.android.synthetic.main.activity_reviews_detail.*
import kotlinx.android.synthetic.main.activity_sign_up.ivBack
import kotlinx.android.synthetic.main.activity_sign_up.ivImage
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.net.MalformedURLException
import java.util.*
import kotlin.collections.ArrayList


class ReviewsDetailActivity : BaseActivity() {

    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null

    var description: String? = null
    var bitmap: Bitmap? = null
    var resultUri: Uri? = null
    var isImgAttached: Boolean = false
    val gson = Gson()
    val postType: String = "Travel Details"
    var paymentID: String = ""

    var userdata: UserData? = null
    lateinit var reviewsData: ReviewsData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reviews_detail)

        reviewsData = intent.extras?.get("DATA") as ReviewsData


        setData()
        ivBack.setOnClickListener {
            finish()
        }


    }

    private fun setData() {

        if (reviewsData != null) {
            Glide.with(this@ReviewsDetailActivity)
                .load(reviewsData.profile_image)
                .apply(RequestOptions.circleCropTransform())
                .into(iv);
        }
        tvUserName.text = reviewsData.user_name
        if (reviewsData.review_score != null) {
            ratinBar.rating = reviewsData.review_score?.toFloat()!!
        }
        tvDescription.text = reviewsData.review_description
        tvTravelling.text = reviewsData.depart_from + "-" + reviewsData.travel_destination
        tvDate.text = ChangeDateFormat.getDateMonthDate(reviewsData.created)
    }


}

