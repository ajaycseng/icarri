package com.murvan.icarri.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.murvan.icarri.R
import com.murvan.icarri.models.TravelDetailData
import com.murvan.icarri.models.UserData
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.ChangeDateFormat
import com.murvan.icarri.utils.Constant
import com.murvan.icarri.utils.Preference
import com.murvan.icarri.utils.toast
import kotlinx.android.synthetic.main.activity_pkg_detail.*
import kotlinx.android.synthetic.main.activity_pkg_detail.tvMsg
import kotlinx.android.synthetic.main.activity_travel_detail.ivArrowLeft
import kotlinx.android.synthetic.main.activity_travel_detail.ivArrowRight
import kotlinx.android.synthetic.main.activity_travel_detail.ivBack
import kotlinx.android.synthetic.main.activity_travel_detail.ivUser
import kotlinx.android.synthetic.main.activity_travel_detail.tvCommentCount
import kotlinx.android.synthetic.main.activity_travel_detail.tvDate
import kotlinx.android.synthetic.main.activity_travel_detail.tvDescription
import kotlinx.android.synthetic.main.activity_travel_detail.tvFrom
import kotlinx.android.synthetic.main.activity_travel_detail.tvName
import kotlinx.android.synthetic.main.activity_travel_detail.tvOffer
import kotlinx.android.synthetic.main.activity_travel_detail.tvTime
import kotlinx.android.synthetic.main.activity_travel_detail.tvTitle
import kotlinx.android.synthetic.main.activity_travel_detail.tvTo
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.Map
import kotlin.collections.MutableMap
import kotlin.collections.set

class PgkDetailActivity : AppCompatActivity() {

    private lateinit var viewPager: ViewPager
    internal lateinit var pagerAdapter: MyCustomPagerAdapter
    var imageData = ArrayList<TravelDetailData>()
    val gson = Gson()
    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null
    var postID: String? = null;
    lateinit var image: ArrayList<String>
    var userdata: UserData? = null
    var userID: String = ""
    var offerId: String = ""
    var offerStatus: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pkg_detail)
        postID = intent.getStringExtra("ID")
        // tvOffer.visibility=View.GONE
        userdata =
            gson.fromJson(
                Preference.getInstance(this).getString(Constant.USER_DETAIL),
                UserData::class.java
            )
        llComment.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this, CommentListActivity::class.java).putExtra("ID", postID))
        })

        tvOffer.setOnClickListener {
            if (offerStatus.equals("-1")) {
                val mainIntent = Intent(
                    this@PgkDetailActivity,
                    AddTravelOfferActivity::class.java
                ).putExtra("ID", postID)
                startActivity(mainIntent)
            } else {
                val mainIntent = Intent(
                    this@PgkDetailActivity,
                    OfferDetailActivity::class.java
                ).putExtra("ID", postID)
                    .putExtra("USERID", userdata?.user_id)
                startActivity(mainIntent)

            }
        }


        viewPager = findViewById(R.id.viewPager)
        tvMsg.setOnClickListener {
            val mainIntent = Intent(
                this@PgkDetailActivity,
                SendMsgActivity::class.java
            ).putExtra("ID", postID).putExtra("LOCATION", tvTitle.text.toString())

            startActivity(mainIntent)
        }
        ivArrowLeft.setOnClickListener {
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);

        }
        ivArrowRight.setOnClickListener {
            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);

        }
        ivBack.setOnClickListener {
            finish()
        }

        ivUser.setOnClickListener {

            val mainIntent = Intent(
                this@PgkDetailActivity,
                UserProfileActivity::class.java
            ).putExtra("ID", userID)

            startActivity(mainIntent)
        }
        tvName.setOnClickListener {

            val mainIntent = Intent(
                this@PgkDetailActivity,
                UserProfileActivity::class.java
            ).putExtra("ID", userID)

            startActivity(mainIntent)
        }
    }

    override fun onResume() {
        super.onResume()
        callWSList()
    }

    private fun callWSList() {
        initCallbackDrop()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.TRAVELER_DETAIL,
            getParamsDrop(),
            "", true
        )
    }


    private fun getParamsDrop(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("user_id", userdata?.user_id)
            jsonStr.put("post_id", postID)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackDrop() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                if (status.equals("success", ignoreCase = true)) {
                    val responseData = jsonObject.getJSONObject("response_data")
                    val data: TravelDetailData
                    data =
                        gson.fromJson(
                            responseData.toString(),
                            TravelDetailData::class.java
                        )
                    tvTitle.text = data.depart_from + " to " + data.travel_destination
                    tvName.text = data.user_name
                    offerId = data.offer_id.toString()

                    tvTo.text = data.travel_destination
                    tvFrom.text = data.depart_from
                    /*  tvTime.text =
                          (data.dimension_packet + "/" + data.dimension_packet_unit)
  */
                    tvTime.text =
                        data.dimension_packet_length + "x" + data.dimension_packet_width + "x" + data.dimension_packet_height + " " + data.dimension_packet_unit
                    tvPkt.text = data.max_packets + " Packet(s)"

                    tvDate.text =
                        (data.max_weight + "/" + data.max_weight_unit)
                    tvDescription.text = data.post_description
                    tvExpectedTime.text =
                        "Expire On: " + ChangeDateFormat.getDateMonthDate(data.expiry_date)
                    Glide.with(this@PgkDetailActivity)
                        .load(data.user_profile_image)
                        .apply(RequestOptions.circleCropTransform())
                        .into(ivUser);
                    userID = data.user_id.toString()

                    tvCommentCount.text = data.total_comments

                    offerStatus = data.offer_status.toString()

                    if (offerStatus.equals("-1")) {
                        tvOffer.setText("Make Offer")
                    } else {
                        tvOffer.setText(data.status_text)
                    }

                    if (data.user_id.equals(userdata?.user_id)) {
                        tvMsg.visibility = View.GONE
                        tvOffer.visibility = View.GONE
                    }

                    /*     if (data.travel_mode.equals("Train")) {
                             ivSender.setImageResource(R.drawable.ic_train)
                             ivReceiver.setImageResource(R.drawable.ic_train)
                             ivVia.setImageResource(R.drawable.ic_train_front)

                         } else if (data.travel_mode.equals("Bus")) {
                             ivSender.setImageResource(R.drawable.ic_bus)
                             ivReceiver.setImageResource(R.drawable.ic_bus)
                             ivVia.setImageResource(R.drawable.ic_bus_front)


                         } else if (data.travel_mode.equals("Flight")) {
                             ivSender.setImageResource(R.drawable.ic_departures)
                             ivReceiver.setImageResource(R.drawable.ic_landing)
                             ivVia.setImageResource(R.drawable.ic_front_flight)


                         } else if (data.travel_mode.equals("Ship")) {
                             ivSender.setImageResource(R.drawable.ic_ship)
                             ivReceiver.setImageResource(R.drawable.ic_ship)
                             ivVia.setImageResource(R.drawable.ic_ship_front)

                         } else if (data.travel_mode.equals("Car")) {
                             ivSender.setImageResource(R.drawable.ic_car)
                             ivReceiver.setImageResource(R.drawable.ic_car)
                             ivVia.setImageResource(R.drawable.ic_car_front)

                         } else if (data.travel_mode.equals("Taxi")) {
                             ivSender.setImageResource(R.drawable.ic_taxi)
                             ivReceiver.setImageResource(R.drawable.ic_taxi)
                             ivVia.setImageResource(R.drawable.ic_taxi_front)

                         } else if (data.travel_mode.equals("Others")) {
                             ivSender.setImageResource(R.drawable.ic_other)
                             ivReceiver.setImageResource(R.drawable.ic_other)
                             ivVia.setImageResource(R.drawable.ic_other)


                         }
     */



                    image = data.all_post_images

                    pagerAdapter = MyCustomPagerAdapter(this@PgkDetailActivity, image)
                    viewPager?.adapter = pagerAdapter
                    pagerAdapter.notifyDataSetChanged()


                } else {
                    toast(msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                toast(error.toString())
            }
        }
    }


    class MyCustomPagerAdapter(var context: Context, var images: ArrayList<String>) :
        PagerAdapter() {
        var layoutInflater: LayoutInflater
        override fun getCount(): Int {
            return images.size
        }

        override fun isViewFromObject(
            view: View,
            `object`: Any
        ): Boolean {
            return view === `object` as LinearLayout
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val itemView =
                layoutInflater.inflate(R.layout.row_image, container, false)
            val imageView =
                itemView.findViewById<View>(R.id.ivImg) as ImageView

            Glide.with(context)
                .load(images[position])
                .apply(RequestOptions.fitCenterTransform())
                .into(imageView);


            container.addView(itemView)
            //listening to image click
            imageView.setOnClickListener {
            }
            return itemView
        }

        override fun destroyItem(
            container: ViewGroup,
            position: Int,
            `object`: Any
        ) {
            container.removeView(`object` as LinearLayout)
        }

        init {
            layoutInflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }
    }

/*
    //
    internal inner class AdapterClass(val travelDetailData: ArrayList<String>) :
        PagerAdapter() {
        override fun getItemCount(): Int {

            return travelDetailData.size
        }


        internal inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var ivImg: ImageView


            init {
                ivImg = view.findViewById(R.id.ivImg) as ImageView
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_image, parent, false)

            return MyViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            Glide.with(this@TravelDetailActivity)
                .load(image[position])
                .apply(RequestOptions.fitCenterTransform())
                .into(ivImg);

            ivImg.setOnClickListener {

            }

        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun getCount(): Int {
            return travelDetailData.size        }
    }*/


}
