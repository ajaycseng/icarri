package com.murvan.icarri.activity

import android.os.Bundle
import com.android.volley.VolleyError
import com.google.gson.Gson
import com.murvan.icarri.R
import com.murvan.icarri.models.UserData
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.Constant
import com.murvan.icarri.utils.Preference
import com.murvan.icarri.utils.snackBar
import kotlinx.android.synthetic.main.activity_bank_detail.*
import kotlinx.android.synthetic.main.activity_sign_up.btnSignUP
import kotlinx.android.synthetic.main.activity_sign_up.ivBack
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class BankDetailActivity : BaseActivity() {

    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null

    var bankName: String? = null
    var accountHolderName: String? = null
    var accountNum: String? = null
    var otherDetail: String? = null
    var bCode: String? = null

    private val gson = Gson()

    var userdata: UserData? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bank_detail)
        userdata =
            gson.fromJson(
                Preference.getInstance(this).getString(Constant.USER_DETAIL),
                UserData::class.java
            )

        ivBack.setOnClickListener {
            finish()
        }

        btnSignUP.setOnClickListener {

            validation()
        }
        setData()
    }

    private fun setData() {


        userdata?.let {

            etBankName.setText(userdata?.bank_name)
            etAholderName.setText(userdata?.account_holder_name)
            etAccountNo.setText(userdata?.iban)
            etBCode.setText(userdata?.branch_code)
            etOther.setText(userdata?.bank_other_details)


        }

    }


    private fun validation() {

        bankName = etBankName.text.toString().trim()
        accountHolderName = etAholderName.text.toString().trim()
        accountNum = etAccountNo.text.toString().trim()
        bCode = etBCode.text.toString().trim()
        otherDetail = etOther.text.toString().trim()

        if (bankName.isNullOrEmpty()) {
            btnSignUP.snackBar(btnSignUP, "Bank Name!!")
        } else if (accountHolderName.isNullOrEmpty()) {
            btnSignUP.snackBar(btnSignUP, "Account Holder Name!!")
        } else if (accountNum.isNullOrEmpty()) {
            btnSignUP.snackBar(btnSignUP, "Account Number!!")
        }/* else if (bCode.isNullOrEmpty()) {
            btnSignUP.snackBar(btnSignUP, "Branch Code!!")
        } else if (otherDetail.isNullOrEmpty()) {
            btnSignUP.snackBar(btnSignUP, "Other Details!!")
        }*/ else {

            callWS();

        }


    }


    private fun callWS() {
        initCallback()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.BANK_DETAIL,
            getParamsLogin(),
            "", true
        )
    }


    private fun getParamsLogin(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {

            jsonStr.put("email_address", userdata?.email_address)
            jsonStr.put("user_id", userdata?.user_id)
            jsonStr.put("bank_name", bankName)
            jsonStr.put("branch_code", bCode)
            jsonStr.put("account_holder_name", accountHolderName)
            jsonStr.put("iban", accountNum)
            jsonStr.put("bank_other_details", otherDetail)


            jsonStr.put("device_type", "Android")
            jsonStr.put(
                "device_id",
                HomeActivity.token
            ); //FirebaseInstanceId.getInstance().getToken());
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallback() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) {


                try {
                    val jsonObject = JSONObject(response)
                    val status = jsonObject.getString("response_status")
                    val msg = jsonObject.getString("response_msg")
                    if (status.equals("success", ignoreCase = true)) {

                        val responseData = jsonObject.getJSONObject("response_data")


                        Preference.getInstance(this@BankDetailActivity)
                            .putString(Constant.USER_DETAIL, responseData.toString())

                        btnSignUP.snackBar(btnSignUP, msg)
                        finish()

                        /*   val mainIntent =
                               Intent(this@EditProfileActivity, SignUpVerifyActivity::class.java)
                           mainIntent.putExtra("EMAIL", email)
                           startActivity(mainIntent)*/
                        // btnSignUP.snackBar(btnSignUP, msg)
                        // btnSignUP.snackBar(btnSignUP, "Request is sent to your email address.")
                        // finish()
                    } else {
                        btnSignUP.snackBar(btnSignUP, msg)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                btnSignUP.snackBar(btnSignUP, error.toString())
            }
        }
    }

}

