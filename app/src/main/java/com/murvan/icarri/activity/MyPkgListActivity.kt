package com.murvan.icarri.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.murvan.icarri.R
import com.murvan.icarri.models.TravelPkgData
import com.murvan.icarri.models.UserData
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.Constant
import com.murvan.icarri.utils.Preference
import com.murvan.icarri.utils.toast
import kotlinx.android.synthetic.main.activity_my_travel_list.*
import kotlinx.android.synthetic.main.activity_my_travel_list.ivAdd
import kotlinx.android.synthetic.main.activity_travel_detail.*
import org.json.JSONException
import org.json.JSONObject
import java.lang.Exception
import java.util.ArrayList
import java.util.HashMap

class MyPkgListActivity : AppCompatActivity() {

    var recycleView: RecyclerView? = null
    var arrayList = ArrayList<TravelPkgData>()
    internal lateinit var mAdapter: AdapterClass
    val gson = Gson()
    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null
    var userdata: UserData? = null
    var postType: String = "Package Details"
    var status: String = "All"
    var selectedPosition: Int = -1
    //
    var pastVisiblesItems = 0
    var visibleItemCount = 0
    var totalItemCount = 0
    private var loading = true
    var pageNumber: Int = 1
    var totalRecords: Int = 0
    var totalPages: Int = 0
    //
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_travel_list)

        tvTitles.setText("My Package List")
        tvNoDatas.setText(resources.getString(R.string.norecord))

        tvNoDatas.setOnClickListener {
            callWSList()
        }

        ivBacks.setOnClickListener {

            finish()
        }

        ivAdd.setOnClickListener {
            startActivity(Intent(this@MyPkgListActivity, AddPkgActivity::class.java))


        }

        userdata =
            gson.fromJson(
                Preference.getInstance(this).getString(Constant.USER_DETAIL),
                UserData::class.java
            )


        recycleView = findViewById(R.id.recycleView)

        var layoutManager = LinearLayoutManager(this)
        val layoutManagerPost = LinearLayoutManager(this)
        recycleView?.layoutManager = layoutManager
        recycleView?.itemAnimator = DefaultItemAnimator()
        mAdapter = AdapterClass(arrayList)
        recycleView?.adapter = mAdapter
        mAdapter.notifyDataSetChanged()
        recycleView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(
                recyclerView: RecyclerView,
                dx: Int,
                dy: Int
            ) {

                var visibleItemCount = layoutManager.getChildCount();
                var totalItemCount = layoutManager.getItemCount();
                var firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (totalPages > pageNumber) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                    ) {
                        pageNumber++
                        callWSList()
                    }
                }

            }
        })
    }


    override fun onResume() {
        super.onResume()
        if (pageNumber == 1) {
            callWSList();
        }
    }
    //

    private fun callWSList() {
        initCallbackDrop()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.TRAVELER_LIST,
            getParamsDrop(),
            "", true
        )
    }


    private fun getParamsDrop(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("post_type", postType)
            jsonStr.put("user_id", userdata?.user_id)
            jsonStr.put("page_number", pageNumber)

            jsonStr.put("status", status)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackDrop() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                totalRecords = jsonObject.getInt("total_records")
                totalPages = jsonObject.getInt("total_pages")
                if (status.equals("success", ignoreCase = true)) {

                    val responseData = jsonObject.getJSONArray("response_data")
                    if (responseData.length() == 0) {
                        tvNoDatas.visibility = View.VISIBLE
                        recycleView?.visibility = View.GONE
                    } else {
                        tvNoDatas.visibility = View.GONE
                        recycleView?.visibility = View.VISIBLE

                        //   arrayList.clear()
                        for (i in 0 until responseData.length()) {
                            val data: TravelPkgData
                            data =
                                gson.fromJson(
                                    responseData.get(i).toString(),
                                    TravelPkgData::class.java
                                )
                            arrayList.add(data)
                        }

                        //  mAdapter = AdapterClass(arrayList)
                        //  recycleView?.adapter = mAdapter
                        mAdapter.notifyDataSetChanged()

                    }


                } else {
                    toast(msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                toast(error.toString())
            }
        }
    }


    //


    internal inner class AdapterClass(val arrayList1: ArrayList<TravelPkgData>) :
        RecyclerView.Adapter<AdapterClass.MyViewHolder>() {
        override fun getItemCount(): Int {

            return arrayList1.size
        }


        internal inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tvName: TextView
            var tvTo: TextView
            var tvFrom: TextView
            var tvDimension: TextView
            var tvWeight: TextView
            var ivOption: ImageView

            var tvDescription: TextView

            var iv: ImageView
            var ivDepart: ImageView
            var ivArrive: ImageView
            var ivVia: ImageView

            var ll: LinearLayout

            init {
                tvName = view.findViewById(R.id.tvName) as TextView
                tvTo = view.findViewById(R.id.tvTo) as TextView
                tvFrom = view.findViewById(R.id.tvFrom) as TextView
                tvDimension = view.findViewById(R.id.tvDate) as TextView
                tvWeight = view.findViewById(R.id.tvTime) as TextView
                tvDescription = view.findViewById(R.id.tvDescription) as TextView
                ivOption = view.findViewById(R.id.ivOption) as ImageView
                iv = view.findViewById(R.id.iv) as ImageView
                ivDepart = view.findViewById(R.id.ivDepart) as ImageView
                ivVia = view.findViewById(R.id.ivVia) as ImageView
                ivArrive = view.findViewById(R.id.ivArrive) as ImageView

                ll = view.findViewById(R.id.ll) as LinearLayout
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_pkg_list, parent, false)

            return MyViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.tvName.text = arrayList1[position].user_name
            holder.tvTo.text = arrayList1[position].travel_destination
            holder.tvFrom.text = arrayList1[position].depart_from
            holder.tvDescription.text = arrayList1[position].post_description
            holder.tvDimension.text =
                arrayList1[position].max_weight + "/" + arrayList1[position].max_weight_unit
            holder.tvWeight.text =
                arrayList1[position].dimension_packet_length + "x" + arrayList1[position].dimension_packet_width + "x" + arrayList1[position].dimension_packet_height + " " + arrayList1[position].dimension_packet_unit

            /*  holder.tvReceiverName.text =
                  ChangeDateFormat.getDateMonthDate(arrayList1[position].travel_date)
              holder.tvReceiverDate.text =
                  ChangeDateFormat.getTimeFromDateTime(arrayList1[position].travel_time)
              // holder.tvReceiverComment.text = arrayList1[position].post_description

              if (arrayList1[position].travel_mode.equals("Train")) {
                  holder.ivSender.setImageResource(R.drawable.ic_train)
                  holder.ivReceiver.setImageResource(R.drawable.ic_train)
                  holder.ivVia.setImageResource(R.drawable.ic_train_front)

              } else if (arrayList1[position].travel_mode.equals("Bus")) {
                  holder.ivSender.setImageResource(R.drawable.ic_bus)
                  holder.ivReceiver.setImageResource(R.drawable.ic_bus)
                  holder.ivVia.setImageResource(R.drawable.ic_bus_front)


              } else if (arrayList1[position].travel_mode.equals("Flight")) {
                  holder.ivSender.setImageResource(R.drawable.ic_departures)
                  holder.ivReceiver.setImageResource(R.drawable.ic_landing)
                  holder.ivVia.setImageResource(R.drawable.ic_front_flight)


              } else if (arrayList1[position].travel_mode.equals("Ship")) {
                  holder.ivSender.setImageResource(R.drawable.ic_ship)
                  holder.ivReceiver.setImageResource(R.drawable.ic_ship)
                  holder.ivVia.setImageResource(R.drawable.ic_ship_front)

              } else if (arrayList1[position].travel_mode.equals("Car")) {
                  holder.ivSender.setImageResource(R.drawable.ic_car)
                  holder.ivReceiver.setImageResource(R.drawable.ic_car)
                  holder.ivVia.setImageResource(R.drawable.ic_car_front)

              } else if (arrayList1[position].travel_mode.equals("Taxi")) {
                  holder.ivSender.setImageResource(R.drawable.ic_taxi)
                  holder.ivReceiver.setImageResource(R.drawable.ic_taxi)
                  holder.ivVia.setImageResource(R.drawable.ic_taxi_front)

              } else if (arrayList1[position].travel_mode.equals("Others")) {
                  holder.ivSender.setImageResource(R.drawable.ic_other)
                  holder.ivReceiver.setImageResource(R.drawable.ic_other)
                  holder.ivVia.setImageResource(R.drawable.ic_other)


              }*/



            holder.ivOption.setOnClickListener {

                showPopupMenu(holder.ivOption, position)
            }

            holder.ll.setOnClickListener {

                val mainIntent = Intent(
                    this@MyPkgListActivity,
                    PgkDetailActivity::class.java
                ).putExtra("ID", arrayList1[position].id)


                startActivity(mainIntent)
            }
            Glide.with(this@MyPkgListActivity)
                .load(arrayList1[position].main_photo)
                .apply(RequestOptions.fitCenterTransform())
                .into(holder.iv);

        }
    }


    fun showPopupMenu(view: View, position: Int) {

        var popup = PopupMenu(this, view);
        popup.getMenuInflater().inflate(R.menu.list_menu, popup.getMenu())
        popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { it ->
            val item = it!!.itemId
            when (item) {
                R.id.menuEdit -> {
                    startActivity(
                        Intent(
                            this@MyPkgListActivity,
                            AddPkgActivity::class.java
                        ).putExtra("DATA", arrayList.get(position))
                    )
                }
                R.id.menuDelete -> {
                    deleteDialog(position)
                }

                R.id.menuAdditional -> {
                    val mainIntent = Intent(
                        this@MyPkgListActivity,
                        AdditionalImgActivity::class.java
                    ).putExtra("ID", arrayList[position].id).putExtra(
                        "TITLES",
                        arrayList[position].depart_from + " To " + arrayList[position].travel_destination
                    )
                    startActivity(mainIntent)
                }
            }
            true

        })


        popup.show();


    }


    fun deleteDialog(position: Int) {
        val builder = AlertDialog.Builder(this)
        //set message for alert dialog
        builder.setMessage(R.string.confirm_delete)
        builder.setIcon(android.R.drawable.ic_dialog_alert)

        //performing positive action
        builder.setPositiveButton("Yes") { dialogInterface, which ->
            selectedPosition = position
            callWSDelete()

        }
        builder.setNegativeButton("No") { dialogInterface, which ->

        }
        //performing cancel action
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }


    private fun callWSDelete() {
        initCallbackDelete()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.DELETE_TRAVELER,
            getParamsDelete(),
            "", true
        )
    }

    private fun getParamsDelete(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("post_id", arrayList.get(selectedPosition).id)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
            //jsonStr.put("action", action);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackDelete() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                if (status.equals("success", ignoreCase = true)) {
                    toast(msg)
                    callWSList()
                } else {
                    toast(msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                toast(error.toString())
            }
        }
    }


    //


}
