package com.murvan.icarri.activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.VolleyError
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.murvan.icarri.R
import com.murvan.icarri.models.AnnouncementTravelData
import com.murvan.icarri.models.OfferTravelData
import com.murvan.icarri.models.UserData
import com.murvan.icarri.networkcall.IResult
import com.murvan.icarri.networkcall.URLS
import com.murvan.icarri.networkcall.VolleyService
import com.murvan.icarri.utils.ChangeDateFormat
import com.murvan.icarri.utils.Constant
import com.murvan.icarri.utils.Preference
import com.murvan.icarri.utils.toast
import kotlinx.android.synthetic.main.activity_offer_detail.*
import kotlinx.android.synthetic.main.activity_travel_detail.ivBack
import kotlinx.android.synthetic.main.activity_travel_detail.tvDate
import kotlinx.android.synthetic.main.activity_travel_detail.tvDescription
import kotlinx.android.synthetic.main.activity_travel_detail.tvName
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.collections.set

class OfferDetailActivity : AppCompatActivity() {

    val gson = Gson()
    var resultCallback: IResult? = null
    var mVolleyService: VolleyService? = null
    var postID: String? = null;
    var userID: String = ""
    var offerStatus: String = ""
    var postedUserID: String = ""
    var userdata: UserData? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_offer_detail)
        userdata =
            gson.fromJson(
                Preference.getInstance(this).getString(Constant.USER_DETAIL),
                UserData::class.java
            )

        postID = intent.getStringExtra("ID")
        postedUserID = intent.getStringExtra("USERID")

        ivBack.setOnClickListener {
            finish()
        }


    }

    override fun onResume() {
        super.onResume()
        callWSList()
    }

    private fun callWSList() {
        initCallbackDrop()
        mVolleyService = VolleyService(resultCallback, this)
        mVolleyService!!.postStringRequest(
            this,
            "POST",
            URLS.OFFER_DETAIL,
            getParamsDrop(),
            "", true
        )
    }


    private fun getParamsDrop(): Map<String, String>? {
        val jsonStr = JSONObject()
        try {
            jsonStr.put("user_id", postedUserID)
            jsonStr.put("post_id", postID)
            jsonStr.put("device_type", "Android")
            jsonStr.put("device_id", HomeActivity.token);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val map: MutableMap<String, String> =
            HashMap()
        map["jsonData"] = jsonStr.toString()
        return map
    }


    private fun initCallbackDrop() {
        resultCallback = object : IResult {
            override fun notifySuccess(
                requestType: String?,
                response: String?
            ) = try {
                val jsonObject = JSONObject(response)
                val status = jsonObject.getString("response_status")
                val msg = jsonObject.getString("response_msg")
                if (status.equals("success", ignoreCase = true)) {

                    val responseData = jsonObject.getJSONObject("response_data")
                    val offerDatalist = responseData.getJSONObject("offer_data")
                    val announcementPostData = responseData.getJSONObject("announcement_post_data")

                    val offerTravelData: OfferTravelData
                    offerTravelData =
                        gson.fromJson(
                            offerDatalist.toString(),
                            OfferTravelData::class.java
                        )
                    val announcementTravelData: AnnouncementTravelData
                    announcementTravelData =
                        gson.fromJson(
                            announcementPostData.toString(),
                            AnnouncementTravelData::class.java
                        )



                    if (announcementTravelData.post_type.equals("Package Details")) {


                        tvName.setText(announcementTravelData.user_name)

                        tvTravelling.setText(announcementTravelData.depart_from + "-" + announcementTravelData.travel_destination)
                        tvDescription.setText(announcementTravelData.dimension_packet_length + "X" + announcementTravelData.dimension_packet_width + "X" + announcementTravelData.dimension_packet_height + " / " + announcementTravelData.dimension_packet_unit + " | " + announcementTravelData.max_weight + announcementTravelData.max_weight_unit + "    " + announcementTravelData.max_packets + " Packet(s)")
                        //  tvDate.setText("" + ChangeDateFormat.getDateMonthDate(announcementTravelData.travel_date))

                        //  tvTravelling.setText(offerTravelData.offer_amount)
                        //  tvDate.setText(ChangeDateFormat.getDateMonthDate(offerTravelData.travel_date))
                        //  tvDescription.setText(announcementTravelData.offer_description)


                        tvOfferUserName.setText(offerTravelData.offer_posted_user_name)
                        tvOfferAmount.setText(offerTravelData.currency_type + "" + offerTravelData.offer_amount)
                        tvOfferDescription.setText(offerTravelData.offer_description)
                        tvOfferDate.setText(ChangeDateFormat.getDateMonthDate(offerTravelData.created))
                        tvOfferPkt.setText("Via: " + offerTravelData.travel_mode)

                        // tvOfferDate.setText(ChangeDateFormat.getDateMonthDate(announcementTravelData.created))

                        //  tvOfferPkt.setText(announcementTravelData.dimension_packet_length + "X" + announcementTravelData.dimension_packet_width + "X" + announcementTravelData.dimension_packet_height + "/" + announcementTravelData.max_weight + announcementTravelData.max_weight_unit)
                        //  tvOfferDate.setText(ChangeDateFormat.getDateMonthDate(announcementTravelData.created))
                        //   tvOfferDescription.setText(offerTravelData.offer_description)
                        Glide.with(this@OfferDetailActivity)
                            .load(announcementTravelData.main_photo)
                            // .apply(RequestOptions.circleCropTransform())
                            .into(ivPostMain)

                        Glide.with(this@OfferDetailActivity)
                            .load(offerTravelData.offer_posted_user_profile_image)
                            .apply(RequestOptions.circleCropTransform())
                            .into(ivOfferUser)

                        Glide.with(this@OfferDetailActivity)
                            .load(announcementTravelData.profile_image)
                            .apply(RequestOptions.circleCropTransform())
                            .into(ivPostUser)
                        llOfferDetail.visibility = View.VISIBLE

                        tvtDateOffer.setText(
                            "Date: " + ChangeDateFormat.getDateMonthDate(offerTravelData.travel_date)
                        )
                        tvtTimeOffer.setText(
                            "Time: " + (ChangeDateFormat.getTimeFromDateTime(
                                offerTravelData.travel_time
                            ))
                        )

                    } else {
                        tvName.setText(announcementTravelData.user_name)
                        tvVia.setText("Via:" + announcementTravelData.travel_mode)
                        tvTravelling.setText(announcementTravelData.depart_from + "-" + announcementTravelData.travel_destination)
                        //  tvDate.setText(ChangeDateFormat.getDateMonthDate(announcementTravelData.travel_date))
                        tvDescription.setText(announcementTravelData.user_name)




                        tvOfferUserName.setText(offerTravelData.offer_posted_user_name)
                        tvOfferAmount.setText(offerTravelData.currency_type + offerTravelData.offer_amount)
                        // tvOfferPkt.setText(offerTravelData.dimension_packet_length + "X" + offerTravelData.dimension_packet_width + "X" + offerTravelData.dimension_packet_height + "/" + offerTravelData.dimension_packet_unit + "|" + offerTravelData.max_weight + offerTravelData.max_weight_unit)
                        tvOfferPkt.setText(offerTravelData.dimension_packet_length + "X" + offerTravelData.dimension_packet_width + "X" + offerTravelData.dimension_packet_height + " / " + offerTravelData.dimension_packet_unit + " | " + offerTravelData.max_weight + offerTravelData.max_weight_unit + "    " + offerTravelData.max_packets + " Packet(s)")

                        //tvOfferDate.setText(ChangeDateFormat.getDateMonthDate(offerTravelData.created))
                        tvOfferDescription.setText(offerTravelData.offer_description)
                        Glide.with(this@OfferDetailActivity)
                            .load(announcementTravelData.main_photo)
                            // .apply(RequestOptions.circleCropTransform())
                            .into(ivPostMain)

                        Glide.with(this@OfferDetailActivity)
                            .load(announcementTravelData.profile_image)
                            .apply(RequestOptions.circleCropTransform())
                            .into(ivPostUser)

                        Glide.with(this@OfferDetailActivity)
                            .load(offerTravelData.offer_posted_user_profile_image)
                            .apply(RequestOptions.circleCropTransform())
                            .into(ivOfferUser)
                        tvtDate.setText(
                            "Date: " + ChangeDateFormat.getDateMonthDate(
                                announcementTravelData.travel_date
                            )
                        )
                        tvtTime.setText(
                            "Time: " + (ChangeDateFormat.getTimeFromDateTime(
                                announcementTravelData.travel_time
                            ))
                        )
                        llPostDetail.visibility = View.VISIBLE


                    }


                    userID = offerTravelData.user_id.toString()


                } else {
                    toast(msg)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            override fun notifyError(
                requestType: String?,
                error: VolleyError
            ) { // showToast(error.toString());
                toast(error.toString())
            }
        }
    }

}
