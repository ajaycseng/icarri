package com.murvan.icarri.models

import java.io.Serializable

class TravelDetailData : Serializable {
    var id: String? = null
    var user_name: String? = null

    var slug: String? = null
    var user_id: String? = null

    var post_description: String? = null
    var depart_from: String? = null
    var travel_destination: String? = null
    var travel_mode: String? = null
    var travel_date: String? = null
    var total_comments: String? = null

    var travel_time: String? = null
    var main_photo: String? = null
    var created: String? = null
    var expiry_date: String? = null
    var user_profile_image: String? = null
    var max_weight: String? = null
    var max_weight_unit: String? = null
    var max_packets: String? = null
    var dimension_packet_unit: String? = null
    var notification: String? = null
    var dimension_packet_length: String? = null
    var dimension_packet_width: String? = null
    var dimension_packet_height: String? = null
    var offer_status: String? = null
    var status_text: String? = null
    var offer_id: String? = null

    var all_post_images = ArrayList<String>()
}