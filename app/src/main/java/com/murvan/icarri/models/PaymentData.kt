package com.murvan.icarri.models

import java.io.Serializable

class PaymentData : Serializable {
    var id: String? = null
    var user_id: String? = null

    var transaction_id: String? = null
    var created: String? = null
    var delivery_status: String? = null
    var show_payment_release_button: String? = null
    var total_paid_amount: String? = null
    var commission_percent: String? = null
    var commission_amount: String? = null
    var final_earnings: String? = null
    var user_name: String? = null
    var profile_image: String? = null
    var delivery_current_status: String? = null

    var delivery_next_status: String? = null


    var delivery_next_status_code: String? = null
    var post_id: String? = null
    var post_type: String? = null
    var depart_from: String? = null
    var travel_destination: String? = null

    var sender_received_type: String? = null
    var tax_percent: String? = null
    var tax_amount: String? = null
    var offer_amount: String? = null
    var payment_transfer_status: String? = null

    var payment_transfer_transaction_id: String? = null
    var transfer_comments: String? = null
    var payment_released_status: String? = null
    var payment_released_button_pressed: String? = null
    var transfer_date: String? = null
    var review_posted: String? = null


}