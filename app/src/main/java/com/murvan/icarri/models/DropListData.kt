package com.murvan.icarri.models

import java.io.Serializable

class DropListData : Serializable {
    var type: String? = null
    var format: String? = null
}