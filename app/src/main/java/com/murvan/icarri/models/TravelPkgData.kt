package com.murvan.icarri.models

import java.io.Serializable

class TravelPkgData : Serializable {
    var id: String? = null
    var user_name: String? = null

    var slug: String? = null
    var post_description: String? = null
    var depart_from: String? = null
    var travel_destination: String? = null
    var travel_mode: String? = null
    var travel_date: String? = null
    var travel_time: String? = null
    var main_photo: String? = null
    var created: String? = null
    var user_id: String? = null

    var expiry_date: String? = null


    var max_weight: String? = null
    var max_weight_unit: String? = null
    var max_packets: String? = null
    var dimension_packet_length: String? = null
    var dimension_packet_width: String? = null
    var dimension_packet_height: String? = null
    // var dimension_packet: String? = null

    var dimension_packet_unit: String? = null
    var profile_image: String? = null
    var notification: Boolean? = null
    var offer_amount: String? = null
    var currency_type: String? = null
    var date_to_move: String? = null
    var offer_posted_user_id: String? = null
    var status_text: String? = null
    var status: String? = null
    var post_id: String? = null
    var post_type: String? = null


}