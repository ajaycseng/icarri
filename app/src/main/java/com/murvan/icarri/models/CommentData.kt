package com.murvan.icarri.models

import java.io.Serializable

class CommentData : Serializable {
    var id: String? = null
    var user_id: String? = null

    var slug: String? = null
    var comment_text: String? = null
    var created: String? = null
    var user_name: String? = null
    var profile_image: String? = null
    var time_ago: String? = null


}