package com.murvan.icarri.models

import java.io.Serializable

class InboxData : Serializable {
    var id: String? = null
    var user_id: String? = null
    var read_unread: String? = null
    var message_text: String? = null
    var created: String? = null
    var user_name: String? = null
    var profile_image: String? = null
    var post_id: String? = null
    var post_type: String? = null
    var depart_from: String? = null
    var travel_destination: String? = null
    var time_ago: String? = null
}