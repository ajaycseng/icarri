package com.murvan.icarri.models

import java.io.Serializable

class ReviewsData : Serializable {
    var id: String? = null
    var user_id: String? = null

    var receiver_user_id: String? = null
    var payment_id: String? = null
    var review_score: String? = null
    var review_description: String? = null
    var user_name: String? = null
    var profile_image: String? = null
    var post_id: String? = null
    var post_type: String? = null
    var depart_from: String? = null
    var travel_destination: String? = null
    var created: String? = null


}