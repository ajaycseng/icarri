package com.murvan.icarri.models

import java.io.Serializable

class OfferTravelData : Serializable {
    var id: String? = null
    var user_id: String? = null
    var offer_posted_user_id: String? = null
    var offer_posted_user_name: String? = null
    var offer_payment_status: String? = null
    var offer_amount: String? = null
    var status: String? = null
    var offer_description: String? = null
    var created: String? = null
    var max_weight: String? = null
    var max_weight_unit: String? = null
    var max_packets: String? = null
    var dimension_packet_unit: String? = null
    var dimension_packet_length: String? = null
    var dimension_packet_width: String? = null
    var dimension_packet_height: String? = null
    var offer_posted_user_profile_image: String? = null

    var travel_mode: String? = null
    var travel_date: String? = null
    var travel_time: String? = null
    var currency_type: String? = null


}
