package com.murvan.icarri.models

import java.io.Serializable

class UserData : Serializable {
    var user_id: String? = null
    var username: String? = null
    var first_name: String? = null
    var phone: String? = null
    var email_address: String? = null
    var country: String? = null
    var swift_code: String? = null
    var city_town: String? = null
    var dob: String? = null
    var mobile_number: String? = null
    var profile_image: String? = null
    var last_name: String? = null


    var total_posts: String? = null
    var total_comments: String? = null
    var total_reviews: String? = null
    var average_rating: String? = null

    var branch_code: String? = null
    var bank_name: String? = null
    var account_holder_name: String? = null
    var iban: String? = null
    var bank_other_details: String? = null

}