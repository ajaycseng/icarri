package com.murvan.icarri.models

import java.io.Serializable

class AnnouncementTravelData : Serializable {
    var post_id: String? = null
    var post_type: String? = null
    var depart_from: String? = null
    var travel_destination: String? = null
    var travel_mode: String? = null
    var travel_date: String? = null
    var travel_time: String? = null
    var user_name: String? = null
    var main_photo: String? = null
    var profile_image: String? = null

    var max_weight: String? = null
    var max_weight_unit: String? = null
    var max_packets: String? = null
    var dimension_packet_unit: String? = null
    var dimension_packet_length: String? = null
    var dimension_packet_width: String? = null
    var dimension_packet_height: String? = null

}