package com.murvan.icarri.utils;

import android.text.format.DateUtils;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;


public class ChangeDateFormat {

    static String TAG = "ChangeDateFormat ";

    // method to change time from milliseconds into sat, 24 Aug 2013 format.
    public static String getDays(String finalDate) {
        String dayDifference = "";
        try {

            Calendar c = Calendar.getInstance();

            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH) + 1;
            int day = c.get(Calendar.DAY_OF_MONTH);

            //Dates to compare
            String currentDate = "" + year + "-" + month + "-" + day;
            currentDate = getDateYYYYMMDD(currentDate);
            Date date1;
            Date date2;

            SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd");

            //Setting dates
            date1 = dates.parse(currentDate);
            date2 = dates.parse(finalDate);

            //Comparing dates
            assert date1 != null;
            assert date2 != null;
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);


            //Convert long to String
            dayDifference = Long.toString(differenceDates);
            if (differenceDates == 0) {
                dayDifference = "Today";
            } else {
                dayDifference = dayDifference + " Days ago";
            }
            Log.e("HERE", "HERE: " + dayDifference);

        } catch (Exception exception) {
            Log.e("DIDN'T WORK", "exception " + exception);
        }
        return dayDifference;
    }


    public static String getYYYYMMDD_from_DDMMMYYYY(String date) {
        SimpleDateFormat df = new SimpleDateFormat("dd-MMMM-yyyy", Locale.US);
        Date new_date;
        long t = 0L;
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Calendar calendar = Calendar.getInstance();
        try {
            new_date = df.parse(date);
            t = new_date.getTime();
            calendar.setTimeInMillis(t);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String s = f.format(calendar.getTime());
        return s;
    }

    // method to change time from milliseconds into 24 Aug 2013 format.
    public static String getDate2(String time) {
        long t = Long.parseLong(time);
        SimpleDateFormat f = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(t);
        String s = f.format(calendar.getTime());
        return s;
    }

    // method to change time from milliseconds into 01:01 AM format.
    public static String getTime(String time) {
        long t = Long.parseLong(time);
        SimpleDateFormat f = new SimpleDateFormat("hh:mm aa", Locale.US);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(t);
        String s = f.format(calendar.getTime());
        return s;
    }

    // method to change time from milliseconds into 15:01:00 format.
    public static String getTimeIn24(String time) {
        long t = Long.parseLong(time);
        SimpleDateFormat f = new SimpleDateFormat("kk:mm:ss", Locale.US);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(t);
        String s = f.format(calendar.getTime());
        return s;
    }

    // method to get date from 2013-05-13 format to milliseconds.
    public static long getDateInMili(String date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date new_date;
        long time = 0L;
        try {
            new_date = df.parse(date);
            time = new_date.getTime();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return time;
    }

    // change date format from 2014-01-14 to 14-01-2014
    public static String getDateToAnother(String date) {
        SimpleDateFormat df = new SimpleDateFormat("hh:mm:ss", Locale.US);
        Date new_date;
        long t = 0L;
        SimpleDateFormat f = new SimpleDateFormat("hh:mm aa", Locale.US);
        Calendar calendar = Calendar.getInstance();
        try {
            new_date = df.parse(date);
            t = new_date.getTime();
            calendar.setTimeInMillis(t);

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String s = f.format(calendar.getTime());
        return s;
    }

    // change date format from 23-9-2013 to 2013-09-23
    public static String getDateYYYYMMDD(String date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date new_date;
        long t = 0L;
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Calendar calendar = Calendar.getInstance();
        try {
            new_date = df.parse(date);
            t = new_date.getTime();
            calendar.setTimeInMillis(t);

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String s = f.format(calendar.getTime());
        return s;
    }

    // change date format from 23-9-2013 to 2013-09-23
    public static String getDateDDMMYYYY(String date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date new_date;
        long t = 0L;
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        Calendar calendar = Calendar.getInstance();
        try {
            new_date = df.parse(date);
            t = new_date.getTime();
            calendar.setTimeInMillis(t);

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String s = f.format(calendar.getTime());
        return s;
    }

    // get day on date from milliseconds
    public static String getDay(String time) {
        long t = Long.parseLong(time);
        SimpleDateFormat f = new SimpleDateFormat("EEE", Locale.US);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(t);
        String s = f.format(calendar.getTime());
        return s;
    }

    // change date format to 12 Aug 2013 from 2013-08-12 12:23:21
    public static String getDateFromDateTime(String date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",
                Locale.US);
        Date new_date;
        long t = 0L;
        SimpleDateFormat f = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
        Calendar calendar = Calendar.getInstance();
        try {
            new_date = df.parse(date);
            t = new_date.getTime();
            calendar.setTimeInMillis(t);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String s = f.format(calendar.getTime());
        return s;
    }

    // change time format to 08:45 PM from 2013-08-12 12:23:21
    public static String getTimeFromDateTime(String date) {
        SimpleDateFormat df = new SimpleDateFormat("hh:mm:ss",
                Locale.US);
        Date new_date;
        long t = 0L;
        SimpleDateFormat f = new SimpleDateFormat("hh:mm aa", Locale.US);
        Calendar calendar = Calendar.getInstance();
        try {
            new_date = df.parse(date);
            t = new_date.getTime();
            calendar.setTimeInMillis(t);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String s = f.format(calendar.getTime());
        return s;
    }

    // change date format to 12 Aug 2013 from 2013-08-12
    public static String getDateMonthFromDate(String date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date new_date;
        long t = 0L;
        SimpleDateFormat f = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
        Calendar calendar = Calendar.getInstance();
        try {
            new_date = df.parse(date);
            t = new_date.getTime();
            calendar.setTimeInMillis(t);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String s = f.format(calendar.getTime());
        return s;
    }


    // change date format to 12 Aug 2013 from 2013-08-12
    public static String getDateMonthDate(String date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date new_date;
        long t = 0L;
        SimpleDateFormat f = new SimpleDateFormat("MMM dd", Locale.US);
        Calendar calendar = Calendar.getInstance();
        try {
            new_date = df.parse(date);
            t = new_date.getTime();
            calendar.setTimeInMillis(t);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String s = f.format(calendar.getTime());
        return s;
    }

    // change time format to 12 Aug 2013, 08:45 PM from 2013-08-12 12:23:21
    public static String getNewDateTimeFromDateTime(String date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",
                Locale.US);
        Date new_date;
        long t = 0L;
        SimpleDateFormat f = new SimpleDateFormat("dd MMM yyyy, hh:mm aa",
                Locale.US);
        Calendar calendar = Calendar.getInstance();
        try {
            new_date = df.parse(date);
            t = new_date.getTime();
            calendar.setTimeInMillis(t);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String s = f.format(calendar.getTime());
        return s;
    }

    // change time format to 12 Aug, 2013 from 2013-08-12
    public static String getNewDateTimeFromDate(String date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date new_date;
        long t = 0L;
        SimpleDateFormat f = new SimpleDateFormat("dd MMM, yyyy", Locale.US);
        Calendar calendar = Calendar.getInstance();
        try {
            new_date = df.parse(date);
            t = new_date.getTime();
            calendar.setTimeInMillis(t);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String s = f.format(calendar.getTime());
        return s;
    }

    // method to change time from milliseconds into 23/03/2014 format.
    public static String getDateFromMili(String time) {
        long t = Long.parseLong(time) * 1000;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(t);
        String s = f.format(calendar.getTime());
        return s;
    }

    public static String getDateFromMilisecondNew(String time) {
        long t = Long.parseLong(time) * 1000;
        SimpleDateFormat f = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(t);
        String s = f.format(calendar.getTime());
        return s;
    }

    public static String getDateFromSec(String time) {
        long t = Long.parseLong(time) * 1000;
        SimpleDateFormat f = new SimpleDateFormat("dd MMM, yyyy", Locale.US);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(t);
        String s = f.format(calendar.getTime());
        return s;
    }

    public static String getDateTimeFromSec(String time) {
        long t = Long.parseLong(time) * 1000;
        SimpleDateFormat f = new SimpleDateFormat("dd MMM, yyyy, hh:mm aa",
                Locale.US);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(t);
        String s = f.format(calendar.getTime());
        return s;
    }

    // method get milliseconds from Date like 04/30/2014
    public static long getMilliSecFromDate(String date) {

        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
        Date new_date;
        long time = 0L;
        try {
            new_date = df.parse(date);
            time = new_date.getTime();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return time;

    }

    // change date formatin portugues to 12,Aug 13 from 2013-08-12 12:23:21
    public static String getDateFromDateTimePortugues(String date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",
                Locale.US);
        Date new_date;
        long t = 0L;
        final Locale myLocale = new Locale("pt", "PT");
        SimpleDateFormat f = new SimpleDateFormat("dd, MMM yy, hh:mm", myLocale);
        Calendar calendar = Calendar.getInstance();
        try {
            new_date = df.parse(date);
            t = new_date.getTime();
            calendar.setTimeInMillis(t);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String s = f.format(calendar.getTime());
        return s;
    }

    // change time format from 12:33:00 to 12:33 PM
    public static String getTimeForamteHHMMA(String time) {
        SimpleDateFormat df = new SimpleDateFormat("hh:mm", Locale.US);
        Date new_date;
        long t = 0L;
        SimpleDateFormat f = new SimpleDateFormat("hh:mm aa", Locale.US);
        Calendar calendar = Calendar.getInstance();
        try {
            new_date = df.parse(time);
            t = new_date.getTime();
            calendar.setTimeInMillis(t);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String s = f.format(calendar.getTime());
        return s;
    }

    // change date time format in to 12,Aug 13 from 2013-08-12 12:23:21
    public static String getDateTimeMonthAM(String date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd",
                Locale.US);
        Date new_date;
        long t = 0L;
        SimpleDateFormat f = new SimpleDateFormat("MMM dd, yyyy",
                Locale.US);
        Calendar calendar = Calendar.getInstance();
        try {
            new_date = df.parse(date);
            t = new_date.getTime();
            calendar.setTimeInMillis(t);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String s = f.format(calendar.getTime());
        return s;
    }

    // get difference in year between two date
    public static String getYearBetweenTwoDate(String date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date new_date = null;
        SimpleDateFormat simpleDateformat = new SimpleDateFormat("yyyy");
        Calendar calendar = Calendar.getInstance();
        int year = 0;
        try {
            new_date = df.parse(date);
            year = calendar.get(Calendar.YEAR);
            System.out.println("year current:- " + year);
            System.out.println("year:- "
                    + Integer.parseInt(simpleDateformat.format(new_date)));

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return ""
                + (year - Integer.parseInt(simpleDateformat.format(new_date)));
    }


    // 24 hr to 12 hr like 13:02:03 to 1.02pm
    public static String getNewTimeTwlveHrFormat(String date) {
        SimpleDateFormat df = new SimpleDateFormat("hh:mm",
                Locale.US);
        Date new_date;
        long t = 0L;
        SimpleDateFormat f = new SimpleDateFormat("HH:mm:ss",
                Locale.US);
        Calendar calendar = Calendar.getInstance();
        try {
            new_date = df.parse(date);
            t = new_date.getTime();
            calendar.setTimeInMillis(t);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String s = f.format(calendar.getTime());
        return s;
    }

    public static String getFutureDate(int dayslater) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.DATE, dayslater);
        String day = sdf.format(calendar.getTime());
        Log.i(TAG, day);

        return day;
    }


}
