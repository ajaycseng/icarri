package com.murvan.icarri.view

import android.content.Context
import android.view.View
import com.murvan.icarri.utils.hideKeyboard

class MyOnClickListner(val context: Context) {


    fun onClick(arg0: View?) {

        arg0?.let { context.hideKeyboard(it) }
        //  OR
    }
}