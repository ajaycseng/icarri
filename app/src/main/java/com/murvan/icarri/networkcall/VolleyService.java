package com.murvan.icarri.networkcall;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.murvan.icarri.R;
import com.murvan.icarri.utils.Utilities;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;


public class VolleyService {

    IResult mResultCallback = null;
    Context mContext;

    Map<String, String> paramsdata = new HashMap<String, String>();


    public VolleyService(IResult resultCallback, Context context) {
        mResultCallback = resultCallback;
        mContext = context;
    }

    public void postStringRequest(final Context context, final String requestType, String url, final Map<String, String> params, final String token, final boolean progessBar) {
        final ProgressDialog pDialog = Utilities.progressDialog(context);
        if (progessBar) {

            pDialog.show();
        }
        paramsdata = params;
        mContext = context;
        try {
            if (!Utilities.isInternetConnected(context)) {
                if (pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Utilities.showDialog(context, context.getResources().getString(R.string.errorInternet));
                return;
            } else {
                RequestQueue queue = Volley.newRequestQueue(mContext);
                StringRequest strReq = new StringRequest(Request.Method.POST,
                        url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (mResultCallback != null)
                            mResultCallback.notifySuccess(requestType, response);
                        if (pDialog.isShowing()) {
                            pDialog.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mResultCallback != null)
                            mResultCallback.notifyError(requestType, error);
                        if (pDialog.isShowing()) {
                            pDialog.dismiss();
                        }
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {

                        final Map<String, String> headers = new HashMap<>();
                        headers.put("key", "iCarrI20191210");
                        // headers.put("Content-Type", "application/x-www-form-urlencoded");

                      /*  if (!token.equals("")) {
                            headers.put("token", token);
                        }*/
                        return headers;
                    }

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        return paramsdata;
                    }

                };

                strReq.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(strReq);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void postStringRequestContentType(final Context context, final String requestType, String url, final Map<String, String> params, final String token) {
        final ProgressDialog pDialog = Utilities.progressDialog(context);
        pDialog.show();
        paramsdata = params;
        mContext = context;
        try {
            if (!Utilities.isInternetConnected(context)) {
                pDialog.hide();
                Utilities.showDialog(context, context.getResources().getString(R.string.errorInternet));
                return;
            } else {
                RequestQueue queue = Volley.newRequestQueue(mContext);
                StringRequest strReq = new StringRequest(Request.Method.POST,
                        url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (mResultCallback != null)
                            mResultCallback.notifySuccess(requestType, response);
                        pDialog.hide();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mResultCallback != null)
                            mResultCallback.notifyError(requestType, error);
                        pDialog.hide();
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {

                        final Map<String, String> headers = new HashMap<>();
                        headers.put("key", "iCarrI20191210");
                        headers.put("Content-Type", "application/x-www-form-urlencoded");

                      /*  if (!token.equals("")) {
                            headers.put("token", token);
                        }*/
                        return headers;
                    }

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        return paramsdata;
                    }

                };

                strReq.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(strReq);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getStringRequest(final Context context, final String requestType, String url, final Map<String, String> params, final String token) {
        final ProgressDialog pDialog = Utilities.progressDialog(context);
        pDialog.show();
        paramsdata = params;
        mContext = context;
        try {
            if (!Utilities.isInternetConnected(context)) {
                pDialog.hide();
                Utilities.showDialog(context, context.getResources().getString(R.string.errorInternet));
                return;
            } else {
                RequestQueue queue = Volley.newRequestQueue(mContext);
                StringRequest strReq = new StringRequest(Request.Method.GET,
                        url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (mResultCallback != null)
                            mResultCallback.notifySuccess(requestType, response);
                        pDialog.hide();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mResultCallback != null)
                            mResultCallback.notifyError(requestType, error);
                        pDialog.hide();
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {

                        final Map<String, String> headers = new HashMap<>();
                        headers.put("key", "iCarrI20191210");

                       /* if (!token.equals("")) {
                            headers.put("token", token);
                        }*/
                        return headers;
                    }

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        return paramsdata;
                    }

                };
                strReq.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(strReq);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void sendImgRequest(final Context context, final String requestType, final String imgkey, final Bitmap bitmap, String url, final Map<String, String> params, final String token) {
        final ProgressDialog pDialog = Utilities.progressDialog(context);
        pDialog.show();
        paramsdata = params;
        mContext = context;

        try {
            if (!Utilities.isInternetConnected(context)) {
                pDialog.hide();
                Utilities.showDialog(context, context.getResources().getString(R.string.errorInternet));
                return;
            } else {
                RequestQueue queue = Volley.newRequestQueue(mContext);


                VolleyMultipartRequest strReq = new VolleyMultipartRequest(Request.Method.POST,
                        url, new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        String resultResponse = new String(response.data);

                        if (mResultCallback != null)
                            mResultCallback.notifySuccess(requestType, resultResponse);
                        pDialog.hide();
                    }


                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mResultCallback != null)
                            mResultCallback.notifyError(requestType, error);
                        pDialog.hide();
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {

                        final Map<String, String> headers = new HashMap<>();
                        headers.put("key", "iCarrI20191210");

                        if (!token.equals("")) {
                            headers.put("token", token);
                        }
                        return headers;
                    }


                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        return paramsdata;
                    }


                    @Override
                    protected Map<String, DataPart> getByteData() {
                        Map<String, DataPart> params = new HashMap<>();

                        if (bitmap != null) {
                            // Bitmap bit = Utilities.to4BytesPerPixelBitmap(bitmap);
                            params.put(imgkey, new DataPart("icarri_user" + ".jpeg", getFileDataFromDrawable(bitmap)));
                        }
                        // file name could found file base or direct access from real path
                        // for now just get bitmap data from ImageView
                        return params;
                    }
                };

                strReq.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(strReq);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }


    public void postStringRequestinbg(final Context context, final String requestType, String url, Map<String, String> params) {
        //  final ProgressDialog pDialog = Utils.progressDialog(context);
        // pDialog.show();
        paramsdata = params;
        mContext = context;
        try {
            RequestQueue queue = Volley.newRequestQueue(mContext);
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (mResultCallback != null)
                        mResultCallback.notifySuccess(requestType, response);
                    // pDialog.hide();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (mResultCallback != null)
                        mResultCallback.notifyError(requestType, error);
                    // pDialog.hide();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return paramsdata;
                }
            };
            queue.add(strReq);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}