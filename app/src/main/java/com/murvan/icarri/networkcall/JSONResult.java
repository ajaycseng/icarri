package com.murvan.icarri.networkcall;

import com.android.volley.VolleyError;

import org.json.JSONObject;


public interface JSONResult {

    public void notifySuccess(String requestType, JSONObject response);


    public void notifyError(String requestType, VolleyError error);
}
