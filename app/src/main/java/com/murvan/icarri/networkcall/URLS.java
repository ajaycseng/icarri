package com.murvan.icarri.networkcall;


public interface URLS {

/*    // Local
    String BASE_URL = "http://demo.bestwebsiteapps.com/icarri/api/";
    String PAYMENT_BASE_URL = "http://demo.bestwebsiteapps.com/icarri/paymenttransactions";*/
    // Live
    String BASE_URL = "https://www.icarri.net/api/";
    String PAYMENT_BASE_URL = "https://www.icarri.net/paymenttransactions";


    // Profile Image
    String IMG_BASE_URL = "";
    String PAYPAL_CLIENT_ID = "AcuoFnBr5kRvUKBg2mzX4701v4BHJynZk_FJZWIIJl-YZyuqsQpWQguHcQ-s_aISr3lqfzHDe1kVpAHY";

    String LOGIN = BASE_URL + "users/login";
    String FORGOT_PASS = BASE_URL + "users/forgotpassword";
    String GENERATE_PASS = BASE_URL + "users/resetpassword";
    String CHANGE_PASS = BASE_URL + "users/changepassword";
    String EDIT_PROFILE = BASE_URL + "users/editprofile";
    String BANK_DETAIL = BASE_URL + "users/savebankdetails";
    String GET_PROFILE = BASE_URL + "users/getprofile";

    String REMOVE_IMG = BASE_URL + "users/removeprofilepicture";
    String ADD_TRAVELER = BASE_URL + "announcements/add";
    String DROP_LIST_OPTION = BASE_URL + "announcements/getddvalues";
    String TRAVELER_LIST = BASE_URL + "announcements/index";
    String TRAVELER_DETAIL = BASE_URL + "announcements/details";
    String SEARCH = BASE_URL + "announcements/search";

    String COMMENT_LIST = BASE_URL + "comments/index";
    String ADD_COMMENT = BASE_URL + "comments/add";


    String EDIT_TRAVELER = BASE_URL + "announcements/edit";
    String DELETE_TRAVELER = BASE_URL + "announcements/delete";

    String IMAGE_LIST = BASE_URL + "mediafiles/index";
    String IMAGE_DELETE = BASE_URL + "mediafiles/delete";
    String IMAGE_ADD = BASE_URL + "mediafiles/add";

    String SIGN_UP_VERIFY = BASE_URL + "users/registerverify";
    String SIGN_UP = BASE_URL + "users/register";
    String ADD_MSG = BASE_URL + "messages/add";
    String UNREAD_MSG = BASE_URL + "messages/countunread";
    String INBOX_MSG = BASE_URL + "messages/inbox";
    String TOWN_LIST = BASE_URL + "towns/index";
    String MSG_LIST = BASE_URL + "messages/thread";
    String MSG_REPLY = BASE_URL + "messages/reply";
    String ADD_OFFER = BASE_URL + "offers/add";
    String OFFER_DETAIL = BASE_URL + "offers/details";
    String OFFER_LIST = BASE_URL + "offers/index";
    String OFFER_TRAVEL_ACCEPT = BASE_URL + "offers/travelaccept";
    String OFFER_PKG_ACCEPT = BASE_URL + "offers/accept";
    String OFFER_REJECT = BASE_URL + "offers/reject";
    String PAYMENT_SUCCESS = BASE_URL + "paymenttransactions/success";
    String PAYMENT_LIST = BASE_URL + "paymenttransactions/index";
    String PAYMENT_DETAIL = BASE_URL + "paymenttransactions/details";
    String PAYMENT_STATUS = BASE_URL + "paymenttransactions/updatedeliverystatus";
    String PAYMENT_RELEASE = BASE_URL + "paymenttransactions";
    String REVIEWS_LIST = BASE_URL + "reviews/index";
    String REVIEWS_DETAIL = BASE_URL + "reviews/details";
    String ADD_REVIEWS = BASE_URL + "reviews/add";
    String LOGOUT = BASE_URL + "users/logout";
    String TERM_CONDITION = BASE_URL + "pages/details";
    String PAYMENT_WV_DETAIL = PAYMENT_BASE_URL+"/details/";
    String PAYMENT_WV_SUCCESS = PAYMENT_BASE_URL+"/paygatesuccess";
    String PAYMENT_WV_DECLINE = PAYMENT_BASE_URL+"/declined";
    String PAYMENT_WV_CANCEL = PAYMENT_BASE_URL+"/cancelled";

}
