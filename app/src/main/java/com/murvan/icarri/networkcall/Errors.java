package com.murvan.icarri.networkcall;

public interface Errors {
    String TIME_OUT = "com.android.volley.TimeoutError";
    //String NO_INTERNET = "com.android.volley.NoConnectionError: java.net.UnknownHostException: Unable to resolve host \"sweepstakes.imagetowebpage.com\": No address associated with hostname";
    String NO_INTERNET = "Your internet connection is slow";
    String SERVER_ERROR = "com.android.volley.ServerError";
}